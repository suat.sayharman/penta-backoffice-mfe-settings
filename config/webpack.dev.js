const { merge } = require('webpack-merge')

const common = require('./webpack.common.js')
const paths = require('./paths')
const packageJson = require('../package.json')

module.exports = merge(common, {
  mode: 'development',

  // Control how source maps are generated
  devtool: 'eval-cheap-module-source-map',

  // Spin up a server for quick development
  devServer: {
    static: {
      directory: paths.public
    },
    proxy: {
      '/api': {
        target: packageJson.proxy,
        secure: false,
        changeOrigin: true,
        setupExitSignals: true
      }
    },
    hot: true,
    open: true,
    compress: true,
    port: 8081,
    historyApiFallback: true
  },

  module: {
    rules: [
      {
        test: /\.s?css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'resolve-url-loader' },
          { loader: 'sass-loader', options: { sourceMap: true } }
        ]
      }
    ]
  }
})
