const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const InterpolateHtmlPlugin = require('interpolate-html-plugin')
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin')

const webpack = require('webpack')
const paths = require('./paths')
const packageJson = require('../package.json')

const currentCommitHash = require('child_process')
  .execSync('git rev-parse --short HEAD')
  .toString()
  .trim()

module.exports = {
  entry: {
    main: paths.src + '/index.js'
  },

  output: {
    path: paths.build,
    filename: '[name].bundle.js'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
            plugins: [
              '@babel/plugin-transform-runtime',
              '@babel/plugin-proposal-class-properties'
            ]
          }
        }
      },
      {
        test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
        type: 'asset/resource'
      },
      {
        test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
        type: 'asset/inline'
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: paths.public + '/index.html',
      cache: false
    }),

    // Removes/cleans build folders and unused assets when rebuilding
    new CleanWebpackPlugin(),

    new InterpolateHtmlPlugin({
      PUBLIC_URL: 'http://localhost:8081' // TODO SS - make dynamic if possible
    }),

    // Shorthand of "'process.env.FOO_BAR': JSON.stringify(process.env.FOO_BAR)" with DefinePlugin
    new webpack.EnvironmentPlugin({
      REACT_APP_GIT_SHA: JSON.stringify(currentCommitHash),
      REACT_APP_HOST_ENV: 'LOCAL',
      REACT_APP_PENTA_BUSINESS_ID: 'default-business-id',
      REACT_APP_LD_CLIENT_ID: 'default-client-id',
      REACT_APP_REFERRAL_API: 'default-referral-api',
      REACT_APP_SENTRY_DSN: 'default-sentry-dsn'
    }),

    new ModuleFederationPlugin({
      name: 'remote__settings',
      filename: 'remoteEntry.js',
      exposes: {
        './RemoteSettingsApp': './src/index'
      },
      shared: {
        react: {
          requiredVersion: packageJson.dependencies.react,
          import: 'react', // the "react" package will be used a provided and fallback module
          shareKey: 'react', // under this name the shared module will be placed in the share scope
          shareScope: 'default', // share scope with this name will be used
          singleton: true, // only a single version of the shared module is allowed
          eager: true
        },
        'react-dom': {
          requiredVersion: packageJson.dependencies['react-dom'],
          singleton: true, // only a single version of the shared module is allowed
          eager: true
        }
      }
    })
  ],

  resolve: {
    modules: [paths.src, 'node_modules'],
    extensions: ['.js', '.jsx', '.json'],
    alias: {
      '@': paths.src
    }
  }
}
