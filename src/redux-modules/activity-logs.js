import {createActions, createReducer} from 'reduxsauce'

const {Types, Creators} = createActions({
  getActivityLogs: ['filters', 'page', 'perPage'],
  onActivityLogsReceived: ['data'],
  onFiltersChanged: ['data'],
  onResetFilters: null
})

export const ActivityLogTypes = Types
export default Creators

export const request = state => {
  return {
    ...state,
    fetching: true,
  }
}

export const INITIAL_STATE = {
  activityLogs: [],
  activityLogsPagination: {
    nextPage: 1,
    perPage: 20,
    count: 0,
    hasMore: true
  },
  filters: {},
}

export const onFiltersChanged = (state, action) => {
  return {
    ...state,
    activityLogs: [],
    activityLogsPagination: {
      nextPage: 1,
      perPage: 20,
      count: 0,
      hasMore: true
    },
    filters: action.data
  }
}

export const onActivityLogsReceived = (state, action) => {
  return {
    ...state,
    activityLogs: [...state.activityLogs, ...action.data.activities],
    activityLogsPagination: {
      nextPage: action.data.page + 1,
      perPage: action.data.perPage,
      count: Number(action.data.count),
      hasMore: (action.data.page * action.data.perPage) < action.data.count
    },
    loading: false
  }
}

export const onResetFilters = state => {
  return {
    ...state,
    filters: {},
    activityLogs: [],
    activityLogsPagination: {
      nextPage: 1,
      perPage: 20,
      count: 0,
      hasMore: true
    },
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [ActivityLogTypes.GET_ACTIVITY_LOGS]: request,
  [ActivityLogTypes.ON_FILTERS_CHANGED]: onFiltersChanged,
  [ActivityLogTypes.ON_ACTIVITY_LOGS_RECEIVED]: onActivityLogsReceived,
  [ActivityLogTypes.ON_RESET_FILTERS]: onResetFilters,
})
