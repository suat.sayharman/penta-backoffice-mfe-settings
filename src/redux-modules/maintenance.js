import { createActions, createReducer } from 'reduxsauce'

const { Types, Creators } = createActions({
  toggleMaintenanceStatus: ['maintenanceStatus'],
  getMaintenanceStatus: null,
  onMaintenanceStatusChanged: ['status'],
  onError: null
})

export const MaintenanceTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  maintenanceStatus: false,
  fetching: false,
  isOpen: false
}

/* ------------- Reducers ------------- */

export const onMaintenanceStatusChanged = (state, action) => {
    return {
        ...state,
        maintenanceStatus: action.status,
        fetching: false
    }
}

export const request = state => {
  return {
    ...state,
    fetching: true
  }
}

export const onError = state => {
  return {
    ...state,
    fetching: false
  }
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ON_MAINTENANCE_STATUS_CHANGED]: onMaintenanceStatusChanged,
  [Types.GET_MAINTENANCE_STATUS]: request,
  [Types.TOGGLE_MAINTENANCE_STATUS]: request,
  [Types.ON_ERROR]: onError
})
