import { createActions, createReducer } from 'reduxsauce'
import { AuthTypes } from './auth'
import { createSelector } from 'reselect'
import { formValueSelector } from 'redux-form'
import uniqBy from 'lodash/uniqBy'

const { Types, Creators } = createActions({
  toggleCreateProfileModal: ['payload'],

  // direct debit profiles
  getDirectDebitProfile: ['businessId'],
  getCollectionLimit: ['businessId'],
  onGetCollectionLimitSuccess: ['data'],
  onGetCollectionLimitError: [],
  onGetDirectDebitProfileSuccess: ['data'],
  onGetDirectDebitProfileError: [],
  clearDirectDebitProfileData: [],
  requestCollectionList: ['businessId', 'filter', 'pageNumber', 'pageSize'],
  onCollectionsListDataSuccess: [
    'payload',
    'totalCollection',
    'hasMore',
    'pageNumber',
    'loadedCollection'
  ],
  onCollectionsListDataFailure: [],

  createDirectDebitProfile: ['report', 'file', 'tcDate'],
  onCreateDirectDebitProfileComplete: [],
  updateCreditorIdentifierRequest: ['businessId', 'creditorIdentifier'],
  updateCreditorIdentifierSuccess: [],
  updateCreditorIdentifierFailure: [],
  resetCreditorData: [],

  // onboarding requests
  getDirectDebitOnboardingRequest: ['businessId'],
  onGetDirectDebitOnboardingRequestSuccess: ['data'],
  onGetDirectDebitOnboardingRequestError: [],
  rejectDirectDebitOnboardingRequest: ['businessId', 'requestId'],
  onRejectDirectDebitOnboardingRequestComplete: []
})

export const DirectDebitTypes = Types
export default Creators

export const INITIAL_STATE = {
  fetchingProfile: false,
  fetchingOnboardingRequest: false,
  fetchingCollections: false,

  createProfileModalVisible: false,
  directDebitState: null,
  onboarding: null,
  isCreditorIdentifierSubmitting: false,
  creditorUpdated: false,

  collectionData: {
    loadedCollection: 0,
    hasMore: true,
    totalCollection: 0,
    pageNumber: 1,
    collectionList: []
  }
}

export const toggleCreateProfileModal = (state, action) => ({
  ...state,
  createProfileModalVisible: action.payload
})

export const requestCollectionList = state => {
  return { ...state, fetching: true }
}

export const getDirectDebitProfile = state => {
  return {
    ...state,
    fetchingProfile: true
  }
}

export const updateCreditorIdentifierRequest = state => {
  return {
    ...state,
    isCreditorIdentifierSubmitting: true
  }
}

export const updateCreditorIdentifierSuccess = state => {
  return {
    ...state,
    isCreditorIdentifierSubmitting: false,
    creditorUpdated: true
  }
}

export const updateCreditorIdentifierFailure = state => {
  return {
    ...state,
    isCreditorIdentifierSubmitting: false,
    creditorUpdated: false
  }
}

export const onGetDirectDebitProfileError = state => {
  return {
    ...state,
    fetchingProfile: false
  }
}

export const onGetCollectionLimitError = state => {
  return {
    ...state,
    fetchingProfile: false
  }
}

export const onGetDirectDebitProfileSuccess = (state, action) => {
  return {
    ...state,
    directDebitState: action.data,
    fetchingProfile: false
  }
}

export const clearDirectDebitProfileData = state => {
  return {
    ...state,
    directDebitState: null
  }
}

export const onGetCollectionLimitSuccess = (state, action) => {
  return {
    ...state,
    collectionLimitState: action.data,
    fetchingProfile: false
  }
}

export const getDirectDebitOnboardingRequest = state => {
  return {
    ...state,
    fetchingOnboardingRequest: true
  }
}

export const onCollectionsListDataSuccess = (state, action) => {
  const combinedCollections = uniqBy(
    [...state.collectionData.collectionList, ...action.payload],
    'id'
  )
  return {
    ...state,
    fetching: false,
    collectionData: {
      collectionList: combinedCollections,
      hasMore: action.hasMore,
      pageNumber: parseInt(action.pageNumber, 10),
      totalCollection: action.totalCollection,
      loadedCollection:
        state.collectionData.loadedCollection +
        parseInt(action.loadedCollection, 10)
    }
  }
}

export const resetCreditorData = state => {
  return {
    ...state,
    collectionData: {
      ...INITIAL_STATE.collectionData
    }
  }
}

export const onCollectionsListDataFailure = (state, action) => {
  return { ...state, fetching: false }
}

export const onGetDirectDebitOnboardingError = state => {
  return {
    ...state,
    fetchingOnboardingRequest: false
  }
}

export const onGetDirectDebitOnboardingSuccess = (state, action) => ({
  ...state,
  fetchingOnboardingRequest: false,
  onboarding:
    action.data !== null
      ? {
          id: action.data.id,
          termsAndConditionsSignedAt: action.data.terms_conditions_signed_at,
          status: action.data.status,
          updatedAt: action.data.updated_at
        }
      : null
})

const onCreateDirectDebitProfileStart = state => ({
  ...state,
  creatingProfile: true
})
const onCreateDirectDebitProfileComplete = state => ({
  ...state,
  creatingProfile: false
})

const rejectDirectDebitOnboardingRequest = state => ({
  ...state,
  fetchingOnboardingRequest: true
})

const onRejectDirectDebitOnboardingRequestComplete = state => ({
  ...state,
  fetchingOnboardingRequest: false
})

export const reducer = createReducer(INITIAL_STATE, {
  [Types.TOGGLE_CREATE_PROFILE_MODAL]: toggleCreateProfileModal,
  [Types.GET_DIRECT_DEBIT_PROFILE]: getDirectDebitProfile,
  [Types.GET_COLLECTION_LIMIT]: getDirectDebitProfile,
  [Types.ON_GET_COLLECTION_LIMIT_SUCCESS]: onGetCollectionLimitSuccess,
  [Types.ON_GET_COLLECTION_LIMIT_ERROR]: onGetCollectionLimitError,
  [Types.REQUEST_COLLECTION_LIST]: requestCollectionList,
  [Types.ON_COLLECTIONS_LIST_DATA_SUCCESS]: onCollectionsListDataSuccess,
  [Types.ON_COLLECTIONS_LIST_DATA_FAILURE]: onCollectionsListDataFailure,
  [Types.ON_GET_DIRECT_DEBIT_PROFILE_SUCCESS]: onGetDirectDebitProfileSuccess,
  [Types.ON_GET_DIRECT_DEBIT_PROFILE_ERROR]: onGetDirectDebitProfileError,
  [Types.CREATE_DIRECT_DEBIT_PROFILE]: onCreateDirectDebitProfileStart,
  [Types.ON_CREATE_DIRECT_DEBIT_PROFILE_COMPLETE]: onCreateDirectDebitProfileComplete,
  [Types.GET_DIRECT_DEBIT_ONBOARDING_REQUEST]: getDirectDebitOnboardingRequest,
  [Types.ON_GET_DIRECT_DEBIT_ONBOARDING_REQUEST_SUCCESS]: onGetDirectDebitOnboardingSuccess,
  [Types.CLEAR_DIRECT_DEBIT_PROFILE_DATA]: clearDirectDebitProfileData,
  [Types.ON_GET_DIRECT_DEBIT_ONBOARDING_REQUEST_ERROR]: onGetDirectDebitOnboardingError,
  [Types.UPDATE_CREDITOR_IDENTIFIER_REQUEST]: updateCreditorIdentifierRequest,
  [Types.UPDATE_CREDITOR_IDENTIFIER_SUCCESS]: updateCreditorIdentifierSuccess,
  [Types.UPDATE_CREDITOR_IDENTIFIER_FAILURE]: updateCreditorIdentifierFailure,
  [Types.RESET_CREDITOR_DATA]: resetCreditorData,

  [Types.REJECT_DIRECT_DEBIT_ONBOARDING_REQUEST]: rejectDirectDebitOnboardingRequest,
  [Types.ON_REJECT_DIRECT_DEBIT_ONBOARDING_REQUEST_COMPLETE]: onRejectDirectDebitOnboardingRequestComplete,
  [AuthTypes.LOGOUT_SUCCESS]: () => INITIAL_STATE
})

const getState = state => state.directDebit
export const getDirectDebitProfileFormValues = formValueSelector(
  'directDebitProfileForm'
)

export const isProfileModalVisible = createSelector(
  getState,
  state => state.createProfileModalVisible
)
export const isFetching = createSelector(
  getState,
  state => state.fetchingProfile || state.fetchingOnboardingRequest
)
export const getDirectDebitState = createSelector(
  getState,
  state => state.directDebitState
)
export const getCollectionLimitState = createSelector(
  getState,
  state => state.collectionLimitState
)
export const getDirectDebitOnboardingRequestState = createSelector(
  getState,
  state => state.onboarding
)
export const isCreditorCheckRejected = createSelector(
  getDirectDebitOnboardingRequestState,
  state => state && state.status === 'REJECTED'
)
export const getDirectDebitOnboardingRequestTermsAndConditionsSignedAt = createSelector(
  getDirectDebitOnboardingRequestState,
  state => (state ? state.termsAndConditionsSignedAt : null)
)
export const getDirectDebitsCollectionListData = createSelector(
  getState,
  state => state.collectionData
)
export const isFetchingCollections = createSelector(
  getState,
  state => state.fetchingCollections
)
export const getIsCreditorIdentifierSubmitting = createSelector(
  getState,
  state => state.isCreditorIdentifierSubmitting
)
export const getIsCreditorUpdated = createSelector(
  getState,
  state => state.creditorUpdated
)
