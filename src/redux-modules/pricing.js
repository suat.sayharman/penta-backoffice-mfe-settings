import { createActions, createReducer } from 'reduxsauce'
import { AuthTypes } from './auth'

const { Types, Creators } = createActions({
  onInvoicesRecieved: ['data'],
  onUsageRecieved: ['data'],
  onCostsRecieved: ['data'],
  onCurrencyCloudCostsReceived: ['data'],
  onCreditNotesReceived: ['data'],
  onPlanRecieved: ['data'],
  onPentaPaymentsRecieved: [
  'data',
  'total',
  'hasMore',
  'pagenumber',
  'loaded'],
  clearPentaTransactions:null,
  setLoading:null,
  onPentaMatchPaymentsRecieved: ['data'],
  cancelDowngrade: ['businessId'],
  showChangePlanModal: null,
  showCancelBillModal: ['bill', 'billName'],
  showMatchPaymentModal:null,
  hideMatchPaymentModal:null,
  hideModal: null,
  hideCancelBillModal: null,
  changePlan: ['businessId', 'data'],
  topUpCredit: ['businessId', 'data'],
  getUsageAndCostsByDate: ['businessId', 'month', 'year'],
  getPentaPayments: ['filter','direction', 'page', 'size'],
  updateFreeTrial: ['endDate'],
  cancelBill: ['bill', 'reason'],
  getRemainingCredits: ['businessId'],
  onRemainingCreditsReceieved: ['data'],
  onError: null,
  updateInvoice: ['businessId', 'payload'],
  onInvoiceUpdated: null,
  onUpdateCreditNoteForm: ['creditNoteForm'],
  onDiscardCreditNoteForm: null,
  onSelectCreditNoteDraftForEditing: ['draft'],
  createCreditNote: ['businessId', 'payload'],
  updateCreditNote: ['businessId', 'creditNoteId', 'payload', 'pauseFetchingCreditNotes'],
  onCreditNoteSaved: ['data'],
  onUpdateSubscriptionPageSelectedTab: ['tab'],
  deleteCreditNote: ['businessId', 'id'],
  onCreditNoteDeleted: null,
  downloadBillDocument: ['bill'],
  previewBillDocument: ['bill'],
  downloadMandate: ['id'],
  downloadConversionsStatement: null
})

export const PricingTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  fetching: false,
  plan: {},
  usage: {},
  invoices: [],
  pentaPayments:[],
  pentaPaymentsTotal:0,
  pentaPaymentsHasMore: true,
  pentaPaymentsPageNumber: 0,
  pentaPaymentsLoaded: 0,
  costs: null,
  currencyCloudCosts: null,
  credits: null,
  isModalOpen: false,
  showCancelBillModal: false,
  showMatchPaymentModal:false,
  cancelBillName: null,
  cancelBill: null,
  creditNoteForm: {
    id: null,
    servicePeriodRequired: false,
    startDate: "",
    endDate: "",
    servicePeriodErrors: {},
    additionalInformation:  "Der Gutschriftsbetrag in Höhe von 0.00 EUR wird in Kürze auf dein Konto unter " +
      "der (Customer IBAN) überwiesen.",
    vatChecked: false,
    items: [
      {
        id: null,
        deleted: false,
        itemNumber: 1,
        name: "",
        description: "",
        quantity: 0,
        unitPrice: 0,
        vatPercent: null,
        total: 0,
        errors: {}
      }
    ]
  },
  creditNotes: [],
  creditNoteDraftForEditing: null,
  subscriptionPageSelectedTab: 'SUBSCRIPTION_PAGE'
}

/* ------------- Reducers ------------- */

export const onInvoicesRecieved = (state, action) => {
  return {
    ...state,
    invoices: action.data
  }
}

export const onUsageRecieved = (state, action) => {
  return {
    ...state,
    usage: action.data,
    fetching: false
  }
}

export const onCostsRecieved = (state, action) => {
  return {
    ...state,
    costs: action.data,
    fetching: false
  }
}

export const onCurrencyCloudCostsReceived = (state, action) => {
  return {
    ...state,
    currencyCloudCosts: action.data,
    fetching: false
  }
}

export const onCreditNotesReceived = (state, action) => {
    return {
      ...state,
      creditNotes: action.data
  }
}

export const onPlanRecieved = (state, action) => {
  return {
    ...state,
    plan: action.data
  }
}

export const onRemainingCreditsReceived = (state, { data }) => {
  return {
    ...state,
    credits: data
  }
}

export const onPentaPaymentsRecieved = (state, {
  data,
  total,
  hasMore,
  pagenumber,
  loaded }) => {
  return{
    ...state,
    pentaPayments: [...state.pentaPayments, ...data],
    pentaPaymentsTotal: total,
    pentaPaymentsHasMore: hasMore,
    pentaPaymentsPageNumber: parseInt(pagenumber, 10),
    pentaPaymentsLoaded: state.pentaPaymentsLoaded + parseInt(loaded, 10),
    loading:false
  }
}

export const clearPentaTransactions = state => {
  return {
    ...state,
    pentaPaymentsLoaded: 0,
    pentaPaymentsTotal: 0,
    pentaPaymentsPageNumber: 0,
    pentaPaymentsHasMore:true,
    pentaPayments:[]
  }
}

export const setLoading = state => {
  return {
    ...state,
    loading:true
  }
}

export const onPentaMatchPaymentsRecieved = (state, { data }) => {
  return{
    ...state,
  }
}

export const showChangePlanModal = state => {
  return {
    ...state,
    isModalOpen: true
  }
}

export const showCancelBillModal = (state, { bill, billName }) => {
  return {
    ...state,
    showCancelBillModal: true,
    cancelBillName: billName,
    cancelBill: bill
  }
}

export const showMatchPaymentModal = (state) => {
  return {
    ...state,
    showMatchPaymentModal: true
  }
}

export const hideModal = state => {
  return {
    ...state,
    isModalOpen: false
  }
}

export const hideCancelBillModal = state => {
  return {
    ...state,
    showCancelBillModal: false,
    cancelBillName: null,
    cancelBill: null
  }
}


export const hideMatchPaymentModal = state => {
  return {
    ...state,
    showMatchPaymentModal: false,
  }
}

export const startFetching = state => {
  return {
    ...state,
    fetching: true
  }
}

export const onError = state => {
  return {
    ...state,
    fetching: false
  }
}


export const onInvoiceUpdated = state => {
  return {
    ...state,
    fetching: false
  }
}

export const onUpdateCreditNoteForm = (state, { creditNoteForm }) => {
  return {
    ...state,
    creditNoteForm
  }
}

export const onCreditNoteSaved = (state, action) => {
  return {
    ...state,
    subscriptionPageSelectedTab: 'SUBSCRIPTION_PAGE',
    creditNoteForm: {
      id: null,
      servicePeriodRequired: false,
      startDate: "",
      endDate: "",
      servicePeriodErrors: {},
      additionalInformation:  "Der Gutschriftsbetrag in Höhe von 0.00 EUR wird in Kürze auf dein Konto unter " +
        "der (Customer IBAN) überwiesen.",
      vatChecked: false,
      items: [
        {
          id: null,
          deleted: false,
          itemNumber: 1,
          name: "",
          description: "",
          quantity: 0,
          unitPrice: 0,
          vatPercent: null,
          total: 0,
          errors: {}
        }
      ]
    }
  }
}

export const onDiscardCreditNoteForm = (state) => {
  return {
    ...state,
    creditNoteForm: {
      id: null,
      servicePeriodRequired: false,
      startDate: "",
      endDate: "",
      servicePeriodErrors: {},
      additionalInformation: "Der Gutschriftsbetrag in Höhe von 0.00 EUR wird in Kürze auf dein Konto unter " +
        "der (Customer IBAN) überwiesen.",
      vatChecked: false,
      items: [
        {
          id: null,
          deleted: false,
          itemNumber: 1,
          name: "",
          description: "",
          quantity: 0,
          unitPrice: 0,
          vatPercent: null,
          total: 0,
          errors: {}
        }
      ]
    }
  }
}

export const onSelectCreditNoteDraftForEditing = (state, { draft }) => {
  return {
    ...state,
    creditNoteDraftForEditing: draft
  }
}

export const onUpdateSubscriptionPageSelectedTab = (state, { tab }) => {
  return {
    ...state,
    subscriptionPageSelectedTab: tab
  }
}

export const onCreditNoteDeleted = state => {
  return {
    ...state
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [PricingTypes.ON_INVOICES_RECIEVED]: onInvoicesRecieved,
  [PricingTypes.ON_USAGE_RECIEVED]: onUsageRecieved,
  [PricingTypes.ON_COSTS_RECIEVED]: onCostsRecieved,
  [PricingTypes.ON_CURRENCY_CLOUD_COSTS_RECEIVED]: onCurrencyCloudCostsReceived,
  [PricingTypes.ON_CREDIT_NOTES_RECEIVED]: onCreditNotesReceived,
  [PricingTypes.ON_PLAN_RECIEVED]: onPlanRecieved,
  [PricingTypes.ON_PENTA_PAYMENTS_RECIEVED]: onPentaPaymentsRecieved,
  [PricingTypes.ON_PENTA_MATCH_PAYMENTS_RECIEVED]: onPentaMatchPaymentsRecieved,
  [PricingTypes.SHOW_CHANGE_PLAN_MODAL]: showChangePlanModal,
  [PricingTypes.SHOW_CANCEL_BILL_MODAL]: showCancelBillModal,
  [PricingTypes.SHOW_MATCH_PAYMENT_MODAL]: showMatchPaymentModal,
  [PricingTypes.HIDE_MODAL]: hideModal,
  [PricingTypes.HIDE_CANCEL_BILL_MODAL]: hideCancelBillModal,
  [PricingTypes.HIDE_MATCH_PAYMENT_MODAL]: hideMatchPaymentModal,
  [PricingTypes.UPDATE_FREE_TRIAL]: startFetching,
  [PricingTypes.CLEAR_PENTA_TRANSACTIONS]: clearPentaTransactions,
  [PricingTypes.SET_LOADING]:setLoading,
  [PricingTypes.ON_REMAINING_CREDITS_RECEIEVED]: onRemainingCreditsReceived,
  [PricingTypes.ON_ERROR]: onError,
  [AuthTypes.LOGOUT_SUCCESS]: () => INITIAL_STATE,
  [PricingTypes.UPDATE_INVOICE]: startFetching,
  [PricingTypes.ON_INVOICE_UPDATED]: onInvoiceUpdated,
  [PricingTypes.ON_UPDATE_CREDIT_NOTE_FORM]: onUpdateCreditNoteForm,
  [PricingTypes.ON_DISCARD_CREDIT_NOTE_FORM]: onDiscardCreditNoteForm,
  [PricingTypes.ON_SELECT_CREDIT_NOTE_DRAFT_FOR_EDITING]: onSelectCreditNoteDraftForEditing,
  [PricingTypes.ON_CREDIT_NOTE_SAVED]: onCreditNoteSaved,
  [PricingTypes.CREATE_CREDIT_NOTE]: startFetching,
  [PricingTypes.UPDATE_CREDIT_NOTE]: startFetching,
  [PricingTypes.DELETE_CREDIT_NOTE]: startFetching,
  [PricingTypes.ON_CREDIT_NOTE_DELETED]: onCreditNoteDeleted,
  [PricingTypes.ON_UPDATE_SUBSCRIPTION_PAGE_SELECTED_TAB]: onUpdateSubscriptionPageSelectedTab
})
