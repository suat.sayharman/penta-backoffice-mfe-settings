import { createActions, createReducer } from 'reduxsauce'
import { AuthTypes } from './auth'

const { Types, Creators } = createActions({
  getBusinessUsers: ['businessId'],
  onUsersReceived: ['data'],
  onRequestFinished: null,
  deleteUser: ['id'],
  authorizeUser: ['businessId', 'accountId', 'userId'],
  authorizeUserFailure: null,
  authorizeUserSuccess: null
})

export const UsersTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  fetching: false,
  users: null,
  isRequestingAuthorization: false
}

/* ------------- Reducers ------------- */

export const request = state => {
  return {
    ...state,
    fetching: true
  }
}

export const onUsersReceived = (state, action) => {
  return {
    ...state,
    fetching: false,
    users: action.data
  }
}

function onAuthorizeUser(state) {
  return { ...state, isRequestingAuthorization: true }
}

function onAuthorizeUserResponse(state) {
  return { ...state, isRequestingAuthorization: false }
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_BUSINESS_USERS]: request,
  [Types.ON_USERS_RECEIVED]: onUsersReceived,
  [AuthTypes.LOGOUT_SUCCESS]: () => INITIAL_STATE,
  [Types.AUTHORIZE_USER]: onAuthorizeUser,
  [Types.AUTHORIZE_USER_FAILURE]: onAuthorizeUserResponse,
  [Types.AUTHORIZE_USER_SUCCESS]: onAuthorizeUserResponse
})
