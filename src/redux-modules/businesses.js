import { createActions, createReducer } from 'reduxsauce'
import { AuthTypes } from './auth'
import _ from 'lodash'
import { createSelector } from 'reselect'

const { Types, Creators } = createActions(
  {
    fetchBusinesses: ['term', 'category', 'page'],
    onBusinessesReceived: ['data'],
    clearBusinessList: null,
    getBusinessData: ['id'],
    onBusinessDataReceived: ['data'],
    sortBy: ['column', 'order'],
    showAddressModal: null,
    hideAddressModal: null,
    updateBusiness: ['businessId', 'data'],
    onChangeRequestReceived: ['idChange'],
    confirmBusinessUpdate: ['idChange', 'businessId', 'tan', 'personId'],
    onBusinessUpdated: null,
    onUpdateBusinessDetails: ['data'],
    onError: ['error'],
    onInvalidTan: null,
    onApprovalRequested: ['approver'],
    onApproversReceived: ['data'],
    showLoader: null,
    businessReadyForSolaris: ['businessId'],
    businessReadyForSolarisSuccess: null,
    getFiles: ['businessId'],
    onFilesReceived: ['data'],
    resetBusinessData: null,
    updateTaxInfo: ['businessId', 'data'],
    onTaxInfoUpdated: ['data'],
    uploadDocument: ['businessId', 'docType', 'file'],
    setActiveTab: ['tab'],
    onRequestFinished: null,
    deleteBusinessData: ['businessId'],
    restartBusinessIdentification: ['businessId'],
    setAccountClosureDate: ['businessId', 'accountId', 'closureDate'],
    onAccountClosureDateSet: ['closureDate'],
    getComplianceQuestionsRequest: ['businessId'],
    getComplianceQuestionsSuccess: ['complianceQuestions'],
    setComplianceQuestions: ['complianceQuestions'],
    getMissingDocumentsRequest: ['businessId'],
    getMissingDocumentsSuccess: ['missingDocuments'],
    downloadDocument: ['businessId', 'documentId'],
    getBusinessClosureReason: ['businessId'],
    onBusinessClosureReasonReceived: ['churnReason', 'churnComment'],
    saveBusinessClosureReason: ['businessId', 'churnReason', 'churnComment'],
    updateBusinessClosureReason: ['businessId', 'churnReason', 'churnComment'],
    deleteBusinessClosureReason: ['businessId'],
    selectAccount: ['payload'],
    syncBusinessRequest: ['businessId'],
    syncBusinessSuccess: [],
    syncBusinessFailure: [],
    onUpdateInvoice: ['invoice'],
    onSelectBillForEditing: ['bill', 'match'],
    showWrittenOffInvoiceModal: ['isOpen', 'status'],
    initialBusinessState: null
  },
  {
    prefix: 'BUSINESS_'
  }
)

export const BusinessTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  fetching: false,
  businessList: [],
  businessListPagination: {
    nextPage: 1,
    perPage: 20,
    count: 0,
    hasMore: true
  },
  businessDetails: {
    details: {}
  },
  account: {},
  accounts: [],
  seizures: [],
  sortColumn: '',
  sortOrder: 'asc',
  isModalOpen: false,
  isSubmitting: false,
  error: null,
  updateStep: 1,
  approvers: [],
  files: null,
  taxInfo: {},
  activeTab: 'tab-business',
  complianceQuestions: undefined,
  missingDocuments: undefined,
  termsAndConditions: null,
  priceAndService: null,
  churnReason: '',
  churnComment: '',
  identification: undefined,
  updatedInvoice: null,
  selectedBillForEditing: null,
  showWrittenOffInvoiceModal: false,
  writtenOffInvoiceStatus: null,
  patchBusinessData: undefined
}

/* ------------- Reducers ------------- */

export const request = state => {
  return {
    ...state,
    fetching: true
  }
}

export const onApproversReceived = (state, action) => {
  return {
    ...state,
    approvers: action.data
  }
}

export const clearBusinessList = state => {
  return {
    ...state,
    businessList: [],
    businessListPagination: INITIAL_STATE.businessListPagination
  }
}

export const resetBusinessData = state => {
  return {
    ...state,
    files: null,
    account: {},
    accounts: [],
    businessDetails: {
      details: {}
    },
    taxInfo: {},
    activeTab: 'tab-business'
  }
}

export const setActiveTab = (state, action) => {
  return {
    ...state,
    activeTab: action.tab
  }
}

export const onBusinessesReceived = (state, action) => {
  const { results, totalCount, isInfiniteScrollSearch } = action.data || {}

  const baseBusinessList = isInfiniteScrollSearch ? state.businessList : []
  const businessList = [...baseBusinessList, ...(results || [])]

  const currentPage = isInfiniteScrollSearch
    ? state.businessListPagination.nextPage
    : 1

  return {
    ...state,
    fetching: false,
    businessList,
    businessListPagination: {
      nextPage: currentPage + 1,
      perPage: 20,
      count: Number(totalCount),
      hasMore: currentPage * 20 < totalCount
    }
  }
}

export const onTaxInfoUpdated = (state, action) => {
  return {
    ...state,
    taxInfo: action.data,
    isSubmitting: false
  }
}

export const onBusinessDataReceived = (state, action) => {
  return {
    ...state,
    ...action.data,
    fetching: false
  }
}

export const sortList = (state, action) => {
  const sortedList = _.orderBy(state.businessList, action.column, action.order)
  return {
    ...state,
    sortColumn: action.column,
    sortOrder: action.order,
    businessList: sortedList
  }
}

export const showModal = state => {
  return {
    ...state,
    isModalOpen: true,
    modalHeaderText: 'Update Address'
  }
}

export const hideModal = state => {
  return {
    ...state,
    isModalOpen: false,
    idChange: null,
    updateStep: 1,
    approver: null
  }
}

export const submitting = state => {
  return {
    ...state,
    isSubmitting: true
  }
}

export const onChangeRequestReceived = (state, action) => {
  return {
    ...state,
    idChange: action.idChange,
    isSubmitting: false,
    updateStep: 2,
    modalHeaderText: 'Select Approver',
    isModalOpen: true,
    fetching: false
  }
}

export const onError = (state, action) => {
  return {
    ...state,
    isSubmitting: false,
    fetching: false,
    error: action.error
  }
}

export const onUpdateBusinessDetails = (state, action) => {
  return {
    ...state,
    businessDetails: action.data,
    fetching: false,
    isSubmitting: false
  }
}

export const onInvalidTan = state => {
  return {
    ...state,
    tanInvalid: true,
    isSubmitting: false
  }
}

export const onBusinessUpdated = state => {
  return {
    ...state,
    isSubmitting: false,
    fetching: false,
    error: null,
    isModalOpen: false,
    idChange: null,
    updateStep: 1,
    approver: null,
    patchBusinessData: undefined
  }
}

export const onApprovalRequested = (state, action) => {
  return {
    ...state,
    isSubmitting: false,
    updateStep: 3,
    modalHeaderText: 'Confirm Change',
    approver: action.approver
  }
}

export const onFilesReceived = (state, action) => {
  return {
    ...state,
    files: action.data,
    fetching: false,
    isSubmitting: false
  }
}

export const onRequestFinished = state => {
  return {
    ...state,
    fetching: false
  }
}

export const onAccountClosureDateSet = (state, { closureDate }) => {
  return {
    ...state,
    fetching: false,
    account: {
      ...state.account,
      closure_date: closureDate
    }
  }
}

const getComplianceQuestionsRequest = state => ({
  ...state,
  fetching: true,
  complianceQuestions: undefined
})

const getComplianceQuestionsSuccess = (state, { complianceQuestions }) => ({
  ...state,
  fetching: false,
  complianceQuestions
})

export const setComplianceQuestions = (state, { complianceQuestions }) => ({
  ...state,
  complianceQuestions
})

const getMissingDocumentsRequest = state => ({
  ...state,
  fetching: true,
  missingDocuments: undefined
})

const getBusinessClosureReason = state => ({
  ...state,
  churnReason: '',
  churnComment: ''
})

const getMissingDocumentsSuccess = (state, { missingDocuments }) => ({
  ...state,
  fetching: false,
  missingDocuments
})

const setBusinessClosureReason = (state, { churnReason, churnComment }) => ({
  ...state,
  churnReason,
  churnComment
})

const onSelectAccount = (state, { payload }) => ({
  ...state,
  account: state.accounts[payload]
})

const onUpdateInvoice = (state, { invoice }) => ({
  ...state,
  updatedInvoice: invoice
})

const onSelectBillForEditing = (state, { bill, match }) => ({
  ...state,
  selectedBillForEditing: { bill, match }
})

export const showWrittenOffInvoiceModal = (
  state,
  { isOpen, status = null }
) => {
  return {
    ...state,
    showWrittenOffInvoiceModal: isOpen,
    writtenOffInvoiceStatus: status
  }
}

function onUpdateBusinessRequest(state, action) {
  return { ...state, patchBusinessData: action.data }
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.FETCH_BUSINESSES]: request,
  [Types.ON_BUSINESSES_RECEIVED]: onBusinessesReceived,
  [Types.GET_BUSINESS_DATA]: request,
  [Types.CLEAR_BUSINESS_LIST]: clearBusinessList,
  [Types.SHOW_LOADER]: request,
  [Types.ON_BUSINESS_DATA_RECEIVED]: onBusinessDataReceived,
  [Types.SORT_BY]: sortList,
  [Types.HIDE_ADDRESS_MODAL]: hideModal,
  [Types.SHOW_ADDRESS_MODAL]: showModal,
  [Types.UPDATE_BUSINESS]: submitting,
  [Types.ON_CHANGE_REQUEST_RECEIVED]: onChangeRequestReceived,
  [Types.CONFIRM_BUSINESS_UPDATE]: submitting,
  [Types.ON_BUSINESS_UPDATED]: onBusinessUpdated,
  [Types.ON_UPDATE_BUSINESS_DETAILS]: onUpdateBusinessDetails,
  [Types.ON_INVALID_TAN]: onInvalidTan,
  [Types.ON_ERROR]: onError,
  [Types.ON_APPROVAL_REQUESTED]: onApprovalRequested,
  [Types.ON_APPROVERS_RECEIVED]: onApproversReceived,
  [Types.GET_FILES]: request,
  [Types.ON_FILES_RECEIVED]: onFilesReceived,
  [Types.RESET_BUSINESS_DATA]: resetBusinessData,
  [Types.UPDATE_TAX_INFO]: submitting,
  [Types.UPLOAD_DOCUMENT]: submitting,
  [Types.ON_TAX_INFO_UPDATED]: onTaxInfoUpdated,
  [Types.BUSINESS_READY_FOR_SOLARIS]: request,
  [Types.BUSINESS_READY_FOR_SOLARIS_SUCCESS]: onRequestFinished,
  [Types.ON_REQUEST_FINISHED]: onRequestFinished,
  [Types.SET_ACTIVE_TAB]: setActiveTab,
  [Types.DELETE_BUSINESS_DATA]: request,
  [Types.RESTART_BUSINESS_IDENTIFICATION]: request,
  [Types.SET_ACCOUNT_CLOSURE_DATE]: request,
  [Types.ON_ACCOUNT_CLOSURE_DATE_SET]: onAccountClosureDateSet,
  [Types.GET_COMPLIANCE_QUESTIONS_REQUEST]: getComplianceQuestionsRequest,
  [Types.GET_COMPLIANCE_QUESTIONS_SUCCESS]: getComplianceQuestionsSuccess,
  [Types.SET_COMPLIANCE_QUESTIONS]: setComplianceQuestions,
  [Types.GET_MISSING_DOCUMENTS_REQUEST]: getMissingDocumentsRequest,
  [Types.GET_MISSING_DOCUMENTS_SUCCESS]: getMissingDocumentsSuccess,
  [Types.GET_BUSINESS_CLOSURE_REASON]: getBusinessClosureReason,
  [Types.ON_BUSINESS_CLOSURE_REASON_RECEIVED]: setBusinessClosureReason,
  [Types.SELECT_ACCOUNT]: onSelectAccount,
  [AuthTypes.LOGOUT_SUCCESS]: () => INITIAL_STATE,
  [Types.INITIAL_BUSINESS_STATE]: () => INITIAL_STATE,
  [Types.SYNC_BUSINESS_REQUEST]: request,
  [Types.SYNC_BUSINESS_SUCCESS]: onRequestFinished,
  [Types.SYNC_BUSINESS_FAILURE]: onRequestFinished,
  [Types.ON_UPDATE_INVOICE]: onUpdateInvoice,
  [Types.ON_SELECT_BILL_FOR_EDITING]: onSelectBillForEditing,
  [Types.SHOW_WRITTEN_OFF_INVOICE_MODAL]: showWrittenOffInvoiceModal,
  [Types.UPDATE_BUSINESS]: onUpdateBusinessRequest
})

export const AccountStatus = {
  ACTIVE: 'ACTIVE',
  INACTIVE: 'INACTIVE'
}

const getState = state => state.businesses

export const getBusinessDetails = createSelector(
  getState,
  state => state.businessDetails
)

export const getFoundationDate = createSelector(getBusinessDetails, state =>
  state.details ? state.details.foundation_date : null
)

export const getAccounts = createSelector(getState, state => state.accounts)

export const getActiveAccounts = createSelector(getAccounts, accounts =>
  accounts.filter(account => account.status !== AccountStatus.INACTIVE)
)

export const getActiveAccountsCount = createSelector(
  getActiveAccounts,
  accounts => accounts.length
)

export const getBusinessId = createSelector(
  getState,
  state => state.businessDetails.id
)

export const getAccount = createSelector(
  getState,
  state => state.account
)

export const getAccountById = (accountId) => createSelector(
  getState,
  state => state.accounts.find(account => account.id === accountId)
)
