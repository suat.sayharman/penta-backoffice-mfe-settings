import { createActions, createReducer } from 'reduxsauce'
import { AuthTypes } from './auth'

const { Types, Creators } = createActions({
  getPromoCodes: ['query'],
  getPromoCodeBusinessList: ['query'],
  createPromoCode: ['promoCode'],
  updatePromoCode: ['id', 'promoCode'],
  onPromoCodesReceived: ['promoCodeList'],
})

export const PromoCodesTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  promoCodesList: [],
  fetching: false,
}

export const onPromoCodesReceived = (state, action) => {
  return {
    ...state,
    promoCodesList: action.promoCodeList,
    fetching: false
  }
}

export const request = state => {
  return {
    ...state,
    fetching: true
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_PROMO_CODES]: request,
  [Types.CREATE_PROMO_CODE]: request,
  [Types.UPDATE_PROMO_CODE]: request,
  [Types.ON_PROMO_CODES_RECEIVED]: onPromoCodesReceived,
  [AuthTypes.LOGOUT_SUCCESS]: () => INITIAL_STATE
})
