import { createActions, createReducer } from 'reduxsauce'

const { Types, Creators } = createActions({
  getRolePermissions: ['role'],
  getRolePermissionsSuccess: ['permissions'],
  updateRolePermissions: ['role', 'addToPermissions', 'removeFromPermissions'],
  updateRolePermissionsSuccess: ['role', 'addedPermissions', 'removedPermissions'],
  getMyPermissions: null,
  getMyPermissionsSuccess: ['permissions'],
})

export const PermissionsTypes = Types
export default Creators

export const INITIAL_STATE = {
  fetching: false,
  myPermissionsFetched: false,
  rolePermissions: [],
  myPermissions: []
}

export const request = state => {
  return {
    ...state,
    fetching: true,
  }
}

export const onPermissionsReceived = (state, { permissions }) => {
  return {
    ...state,
    fetching: false,
    rolePermissions: permissions
  }
}

export const onPermissionsUpdated = (state, { addedPermissions, removedPermissions }) => {
  const rolePermissions = [...state.rolePermissions, ...addedPermissions]
    .filter((permission) => !removedPermissions.includes(permission))
  return {
    ...state,
    fetching: false,
    rolePermissions
  }
}

export const onMyPermissionsReceived = (state, { permissions }) => {
  return {
    ...state,
    fetching: false,
    myPermissionsFetched: true,
    myPermissions: permissions
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_ROLE_PERMISSIONS]: request,
  [Types.GET_ROLE_PERMISSIONS_SUCCESS]: onPermissionsReceived,
  [Types.UPDATE_ROLE_PERMISSIONS]: request,
  [Types.UPDATE_ROLE_PERMISSIONS_SUCCESS]: onPermissionsUpdated,
  [Types.GET_MY_PERMISSIONS]: request,
  [Types.GET_MY_PERMISSIONS_SUCCESS]: onMyPermissionsReceived
})
