import { createReducer, createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  loginRequest: ['email', 'password'],
  loginSuccess: ['data'],
  logoutRequest: null,
  logoutSuccess: null,
  onAuthError: ['error'],
  checkAuthState: null,
  checkAuthStateComplete: ['role']
})

export const AuthTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  fetching: false,
  error: null,
  isLoggedIn: false
}

/* ------------- Reducers ------------- */

export const request = state => {
  return {
    ...state,
    fetching: true,
    error: null
  }
}

export const checkAuthStateComplete = (state, { role }) => {
  const userId = sessionStorage.getItem('user_id')
  return {
    ...state,
    isLoggedIn: userId ? true : false,
    role
  }
}

export const loginSuccess = (state, action) => {
  sessionStorage.setItem('auth_token', action.data.token)
  sessionStorage.setItem('user_id', action.data.id_user)
  return {
    ...state,
    fetching: false,
    isLoggedIn: true,
    role: action.data.role
  }
}

export const logoutSuccess = state => {
  sessionStorage.removeItem('auth_token')
  sessionStorage.removeItem('user_id')
  sessionStorage.removeItem('email')
  return INITIAL_STATE
}

export const onError = (state, action) => {
  return {
    ...state,
    fetching: false,
    error: action.error
  }
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN_REQUEST]: request,
  [Types.LOGIN_SUCCESS]: loginSuccess,
  [Types.LOGOUT_REQUEST]: request,
  [Types.LOGOUT_SUCCESS]: logoutSuccess,
  [Types.ON_AUTH_ERROR]: onError,
  [Types.CHECK_AUTH_STATE_COMPLETE]: checkAuthStateComplete
})
