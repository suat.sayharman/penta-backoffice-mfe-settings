import { createActions, createReducer } from 'reduxsauce'
import { AuthTypes } from './auth'

const { Types, Creators } = createActions({
  onBusinessIdentificationsReceived: ['data'],
  onBusinessIdentificationReceived: ['data'],
  getBusinessIdentificationsByBusinessId: ['businessId'],
  getBusinessIdentificationById: ['businessId', 'identificationId'],
  onError: [],
  clearBusinessIdentifications: [],
  clearBusinessIdentificationDetails: []
})

export const BusinessIdentificationsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  fetching: false,
  businessIdentifications: undefined,
  areThereMoreBusinessIdentificatons: false,
  businessIdentificationDetails: undefined,
  fetchingIdentificationDetails: false
}

/* ------------- Reducers ------------- */

export const request = state => {
  return {
    ...state,
    fetching: true,
    fetchingIdentificationDetails: true
  }
}

export const onError = state => {
  return {
    ...state,
    fetching: false,
    fetchingIdentificationDetails: false
  }
}

export const clearBusinessIdentifications = state => {
  return {
    ...state,
    businessIdentifications: undefined,
    areThereMoreBusinessIdentificatons: false
  }
}

export const clearBusinessIdentificationDetails = state => {
  return {
    ...state,
    businessIdentificationDetails: undefined,
    fetchingIdentificationDetails: false
  }
}

export const onBusinessIdentificationsReceived = (state, { data }) => {
  const sortedIdentsDesc = data.identifications.sort((a, b) =>
    a.legal_identification_created_at <= b.legal_identification_created_at
      ? 1
      : -1
  )

  return {
    ...state,
    businessIdentifications: sortedIdentsDesc,
    areThereMoreBusinessIdentificatons: data.hasNext,
    fetching: false
  }
}

export const onBusinessIdentificationReceived = (state, { data }) => {
  return {
    ...state,
    businessIdentificationDetails: data,
    fetchingIdentificationDetails: false
  }
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_BUSINESS_IDENTIFICATIONS_BY_BUSINESS_ID]: request,
  [Types.GET_BUSINESS_IDENTIFICATION_BY_ID]: request,
  [Types.ON_ERROR]: onError,
  [Types.ON_BUSINESS_IDENTIFICATIONS_RECEIVED]: onBusinessIdentificationsReceived,
  [Types.ON_BUSINESS_IDENTIFICATION_RECEIVED]: onBusinessIdentificationReceived,
  [Types.CLEAR_BUSINESS_IDENTIFICATIONS]: clearBusinessIdentifications,
  [Types.CLEAR_BUSINESS_IDENTIFICATION_DETAILS]: clearBusinessIdentificationDetails,
  [AuthTypes.LOGOUT_SUCCESS]: () => INITIAL_STATE
})
