import { createActions, createReducer } from 'reduxsauce'
import { AuthTypes } from './auth'

const { Types, Creators } = createActions({
  getAnnouncements: null,
  createAnnouncement: ['data'],
  editAnnouncement: ['id', 'data'],
  removeAnnouncement: ['id'],
  onAnnouncementsReceived: ['data', 'previous'],
  onAnnouncementEdited: null,
  turnEditingOn: ['id'],
  turnEditingOff: null,
  showModal: ['announcement'],
  hideModal: null
})

export const AnnouncementTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  announcementsList: [],
  previousAnnouncementsList: [],
  announcData: {
    metadata: {},
    condition: {}
  },
  editMode: false,
  fetching: false,
  isOpen: false,
  selected: null
}

export const onAnnouncementsReceived = (state, action) => {
  return {
    ...state,
    announcementsList: action.data,
    previousAnnouncementsList: action.previous,
    fetching: false
  }
}
export const request = state => {
  return {
    ...state,
    fetching: true
  }
}

export const turnEditingOn = (state, action) => {
  return {
    ...state,
    announcData: state.announcementsList.find(
      announcement => announcement.id === action.id
    ),
    editMode: true
  }
}

export const turnEditingOff = state => {
  return {
    ...state,
    editMode: false
  }
}

export const showModal = (state, action) => {
  return {
    ...state,
    isOpen: true,
    selected: action.announcement
  }
}

export const hideModal = state => {
  return {
    ...state,
    isOpen: false,
    selected: null
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_ANNOUNCEMENTS]: request,
  [Types.CREATE_ANNOUNCEMENT]: request,
  [Types.EDIT_ANNOUNCEMENT]: request,
  [Types.REMOVE_ANNOUNCEMENT]: request,
  [Types.ON_ANNOUNCEMENT_EDITED]: () => INITIAL_STATE,
  [Types.TURN_EDITING_ON]: turnEditingOn,
  [Types.TURN_EDITING_OFF]: turnEditingOff,
  [Types.ON_ANNOUNCEMENTS_RECEIVED]: onAnnouncementsReceived,
  [Types.SHOW_MODAL]: showModal,
  [Types.HIDE_MODAL]: hideModal,
  [AuthTypes.LOGOUT_SUCCESS]: () => INITIAL_STATE
})
