import { createActions, createReducer } from 'reduxsauce'

const { Types, Creators } = createActions({
  setLaunchdarklyClient: ['payload'],
  identifyLaunchdarklyUser: ['payload']
})

export const mkUser = userId => ({
  key: userId
})

const initialState = { type: 'NotReady' }

export const fold = (state, whenNotReady, whenReady) =>
  state.type === 'NotReady' ? whenNotReady : whenReady(state.client)

export const isReady = (state, whenReady) => fold(state, undefined, whenReady)


export const LDTypes = Types
export default Creators

export const setLaunchdarklyClient = (state, action) => {
  return {
    ...state,
    type: 'Ready',
    client: action.payload
  }
}

export const launchDarklyReducer = createReducer(initialState, {
  [Types.SET_LAUNCHDARKLY_CLIENT]: setLaunchdarklyClient
})

export const getLDClient = state => {
  const typedState = state
  return typedState.launchdarkly && typedState.launchdarkly.type === 'Ready'
    ? typedState.launchdarkly.client
    : undefined
}
