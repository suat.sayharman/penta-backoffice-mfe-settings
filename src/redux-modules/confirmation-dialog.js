import { createActions, createReducer } from 'reduxsauce'

const { Types, Creators } = createActions({
  showConfirmation: [
    'confirmationText',
    'callback',
    'options'
  ],
  closeConfirmation: null
})

export const ConfirmDialogTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  isOpen: false,
  modalOptionalClassName: '',
  confirmButtonText: 'Yes',
  cancelButtonText: 'No',
  headerText: 'Please confirm'
}

/* ------------- Reducers ------------- */

export const showConfirmation = (state, action) => {
  return {
    ...state,
    isOpen: true,
    confirmationText: action.confirmationText,
    callback: action.callback,
    modalOptionalClassName: action.options && action.options.modalOptionalClassName
      ? action.options.modalOptionalClassName: '',
    confirmButtonText: action.options && action.options.confirmButtonText
      ? action.options.confirmButtonText
      : 'Yes',
    cancelButtonText: action.options && action.options.cancelButtonText
      ? action.options.cancelButtonText
      : 'No',
    headerText: action.options && action.options.headerText
      ? action.options.headerText
      : 'Please confirm',
  }
}

export const closeConfirmation = state => {
  return {
    ...state,
    isOpen: false,
    modalOptionalClassName: '',
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
    headerText: 'Please confirm'
  }
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SHOW_CONFIRMATION]: showConfirmation,
  [Types.CLOSE_CONFIRMATION]: closeConfirmation
})
