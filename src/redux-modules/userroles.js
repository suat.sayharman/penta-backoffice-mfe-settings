import { createActions, createReducer } from 'reduxsauce'

const { Types, Creators } = createActions({
  getRoles: null,
  getRolesSuccess: ['roles'],
  getBackofficeUsers: null,
  getBackofficeUsersSuccess: ['users'],
  createRole: ['role'],
  createRoleSuccess: null,
  updateRoleName: ['roleId', 'newName'],
  updateRoleNameSuccess: null,
  assignUserToRole: ['userId', 'role'],
  assignUserToRoleSuccess: ['userId', 'role'],
  deleteRole: ['role'],
})

export const UserRoleTypes = Types
export default Creators

export const INITIAL_STATE = {
  fetching: false,
  roles: [],
  users: []
}

export const request = state => {
  return {
    ...state,
    fetching: true,
  }
}

export const onRolesReceived = (state, { roles }) => {
  return {
    ...state,
    fetching: false,
    roles
  }
}

export const onRoleCreated = (state) => {
  return {
    ...state,
    fetching: false
  }
}

export const onBackofficeUsersReceived = (state, { users }) => {
  return {
    ...state,
    users,
    fetching: false
  }
}

export const roleNameUpdated = (state) => {
  return {
    ...state,
    fetching: false
  }
}

export const onUserRoleUpdated = (state, { userId, role }) => {
  const updatedUsers = [...state.users]
  const index = updatedUsers.findIndex((user) => user.id === userId);
  if (index > -1) {
    updatedUsers[index].role = role
  }

  return {
    ...state,
    fetching: false,
    users: updatedUsers
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_ROLES]: request,
  [Types.GET_ROLES_SUCCESS]: onRolesReceived,
  [Types.CREATE_ROLE]: request,
  [Types.CREATE_ROLE_SUCCESS]: onRoleCreated,
  [Types.GET_BACKOFFICE_USERS]: request,
  [Types.GET_BACKOFFICE_USERS_SUCCESS]: onBackofficeUsersReceived,
  [Types.UPDATE_ROLE_NAME]: request,
  [Types.UPDATE_ROLE_NAME_SUCCESS]: roleNameUpdated,
  [Types.ASSIGN_USER_TO_ROLE]: request,
  [Types.ASSIGN_USER_TO_ROLE_SUCCESS]: onUserRoleUpdated,
})
