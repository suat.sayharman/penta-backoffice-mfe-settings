import { createActions, createReducer } from 'reduxsauce'
import { AuthTypes } from './auth'

const { Types, Creators } = createActions({
  getReferralReports: ['query'],
  getReferralSummary: ['query'],
  onReportsReceived: ['reportList'],
  onSummaryReceived: ['summary'],
})

export const ReferralReportsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  reportList: [],
  summary: {},
  fetching: false,
}

export const onReportsReceived = (state, action) => {
  return {
    ...state,
    reportList: action.reportList,
    fetching: false
  }
}

export const onSummaryReceived = (state, action) => {
  return {
    ...state,
    summary: action.summary,
    fetching: false
  }
}

export const request = state => {
  return {
    ...state,
    fetching: true
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_REFERRAL_REPORTS]: request,
  [Types.GET_REFERRAL_SUMMARY]: request,
  [Types.ON_REPORTS_RECEIVED]: onReportsReceived,
  [Types.ON_SUMMARY_RECEIVED]: onSummaryReceived,
  [AuthTypes.LOGOUT_SUCCESS]: () => INITIAL_STATE
})
