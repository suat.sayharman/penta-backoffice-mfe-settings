import { createActions, createReducer } from 'reduxsauce'
import { AuthTypes } from './auth'
import { BusinessTypes } from './businesses'

const { Types, Creators } = createActions({
  fetchInitialTransactions: ['businessId', 'accountId', 'filter'],
  requestTransactions: ['businessId', 'accountId', 'filter', 'number', 'size'],
  onError: null,
  onInitialTransactionsDataLoaded: ['data'],
  onTransactionsDataLoaded: [
    'payload',
    'totalTransactions',
    'hasMore',
    'pageNumber',
    'loadedTransactions'
  ],
  selectTransaction: ['businessId', 'accountId', 'id'],
  onTransactionLoaded: ['data'],
  setFilter: ['data'],
  clearTransactions: null,
  closeTrxModal: null,
  selectCardReservation: ['item'],
  closeReservationModal: null
})

export const TransactionsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  fetching: false,
  hasMore: true,
  totalTransactions: 0,
  pageNumber: 1,
  loadedTransactions: 0,
  trxList: null,
  scheduledOrders: null,
  cardReservations: null,
  balance: {},
  transactionDetails: null,
  reservationDetails: null,
  showTrxModal: false,
  categories: null,
  filter: {
    inflows: true,
    outflows: true
  }
}

/* ------------- Reducers ------------- */

export const onRequest = state => {
  return {
    ...state,
    fetching: true
  }
}

export const onError = state => {
  return {
    ...state,
    fetching: false
  }
}

export const setFilter = (state, action) => {
  return {
    ...state,
    filter: { ...state.filter, ...action.data }
  }
}

export const openTrxModal = state => {
  return {
    ...state,
    showTrxModal: true
  }
}

export const closeTrxModal = state => {
  return {
    ...state,
    showTrxModal: false,
    transactionDetails: null
  }
}

export const openReservationModal = (state, action) => {
  return {
    ...state,
    reservationDetails: action.item
  }
}

export const closeReservationModal = state => {
  return {
    ...state,
    reservationDetails: null
  }
}

export const onInitialTransactionsDataLoaded = (state, action) => {
  const transactions = action.data.transactions
  return {
    ...state,
    fetching: false,
    totalTransactions: transactions ? transactions.length : 0,
    scheduledOrders: action.data.scheduledOrders,
    trxList: transactions,
    cardReservations: action.data.reservations,
    balance: action.data.balance,
    categories: action.data.categories
  }
}

export const onTransactionsDataLoaded = (state, action) => {
  return {
    ...state,
    trxList: [...state.trxList, ...action.payload],
    hasMore: action.hasMore,
    pageNumber: parseInt(action.pageNumber, 10),
    totalTransactions: action.totalTransactions,
    loadedTransactions:
      state.loadedTransactions + parseInt(action.loadedTransactions, 10)
  }
}

export const onTransactionLoaded = (state, action) => {
  return {
    ...state,
    transactionDetails: action.data
  }
}

export const clearTransactions = state => {
  return {
    ...state,
    loadedTransactions: 0,
    totalTransactions: 0,
    pageNumber: 1,
    trxList: []
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [TransactionsTypes.FETCH_INITIAL_TRANSACTIONS]: onRequest,
  [TransactionsTypes.ON_INITIAL_TRANSACTIONS_DATA_LOADED]: onInitialTransactionsDataLoaded,
  [TransactionsTypes.ON_TRANSACTIONS_DATA_LOADED]: onTransactionsDataLoaded,
  [TransactionsTypes.ON_ERROR]: onError,
  [TransactionsTypes.SELECT_TRANSACTION]: openTrxModal,
  [TransactionsTypes.ON_TRANSACTION_LOADED]: onTransactionLoaded,
  [TransactionsTypes.CLOSE_TRX_MODAL]: closeTrxModal,
  [TransactionsTypes.SELECT_CARD_RESERVATION]: openReservationModal,
  [TransactionsTypes.CLOSE_RESERVATION_MODAL]: closeReservationModal,
  [TransactionsTypes.SET_FILTER]: setFilter,
  [TransactionsTypes.CLEAR_TRANSACTIONS]: clearTransactions,
  [AuthTypes.LOGOUT_SUCCESS]: () => INITIAL_STATE,
  [BusinessTypes.RESET_BUSINESS_DATA]: () => INITIAL_STATE
})
