import { createActions, createReducer } from 'reduxsauce'
import { createSelector } from 'reselect'
import { AuthTypes } from './auth'

const { Types, Creators } = createActions({
  getCards: ['businessId'],
  onCardsReceived: ['cards'],
})

export const CardsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  fetching: false,
  cards: null,
}

/* ------------- Reducers ------------- */

export const request = state => {
  return {
    ...state,
    fetching: true
  }
}

export const onCardsReceived = (state, { cards }) => {
  return {
    ...state,
    cards,
    fetching: false,
  }
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_CARDS]: request,
  [Types.ON_CARDS_RECEIVED]: onCardsReceived,
  [AuthTypes.LOGOUT_SUCCESS]: () => INITIAL_STATE,
})

/* ------------- Selectors ------------- */

export const getCards = state => state.cards.cards
export const getCardById = id =>
  createSelector(getCards, cards => cards.find(card => card.id === id))