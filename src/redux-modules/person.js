import { createActions, createReducer } from 'reduxsauce'
import { AuthTypes } from './auth'

const { Types, Creators } = createActions(
  {
    syncPersons: ['businessId'],
    onSyncComplete: null,
    onSyncError: null,
    getPerson: ['businessId', 'personId'],
    onPersonLoaded: ['data'],
    getPersonIdentifications: ['personId'],
    onPersonIdentificationLoaded: ['data'],
    onPersonIdentificationsFetched: ['data'],
    getPersonIdentificationAttempts: ['identificationId'],
    onPersonIdentificationAttemptsFetched: ['data'],
    createPersonIdentification: ['businessId', 'personId'],
    setAsLr: ['businessId', 'personId'],
    setAsUbo: ['businessId', 'personId', 'voting_share'],
    savePerson: ['businessId', 'data'],
    onPersonSaved: null,
    resetPersonState: null,
    setCurrentUser: ['user'],
    onLegalRepSet: null,
    updateUserActiveState: ['businessId', 'userId', 'isActive'],
    updateUserActiveStateSuccess: ['isActive'],
    syncPersonMobileNumber: ['personId', 'businessId'],
    getPersonMobileNumberSuccess: ['data'],
    updateUserPermissions: ['businessId', 'userId', 'data'],
    updateUserPermissionsSuccessful: ['data'],
    updateUserPermissionsFailed: null,
    setUserPermissions: ['data'],
    getUserPermissions: ['businessId', 'userId'],
    getUserLoginAttempts: ['userId', 'limit', 'offset'],
    setUserLoginAttempts: ['data'],
    resetUserLoginAttemptsState: null,
    resetUserPassword: ['email'],
    clearInfoMsg: null,
    getUserPermissionsFailed: null,
    patchPerson: ['businessId', 'personId', 'data'],
    showEmailModal: ['user', 'personId'],
    hideEmailModal: null,
    onPatchPersonChangeRequestReceived: ['idChange'],
    requestApprovalForPatchPerson: ['idChange', 'person', 'businessId'],
    confirmPatchPersonUpdate: ['idChange', 'businessId', 'tan', 'personId'],
    onApprovalRequested: ['approver'],
    onPatchPersonUpdated: null,
    showAddressModal: null,
    hideAddressModal: null,
    sendActivationEmail: ['businessId', 'userId'],
    onSendActivationEmailSuccess: null,
    generateTrustpilotLink: ['userId'],
    onTrustpilotLinkReceived: ['url']
  },
  {
    prefix: 'PERSON_'
  }
)

export const PersonTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  fetching: false,
  current: {},
  identificationLink: {},
  personIdentifications: [],
  personIdentificationAttempts: [],
  user: null,
  updateStep: 1,
  trustpilotLink: null,
  userLoginAttempts: []
}

/* ------------- Reducers ------------- */

export const request = state => {
  return {
    ...state,
    fetching: true
  }
}

export const resetPersonState = state => {
  return {
    ...state,
    current: {},
    identificationLink: {},
    personIdentifications: [],
    user: null
  }
}

export const setCurrentUser = (state, action) => {
  return {
    ...state,
    user: action.user
  }
}

export const setUserLoginAttempts = (state, action) => {
  return {
    ...state,
    userLoginAttempts: action.data.data
  }
}

export const resetUserLoginAttempts = (state, action) => {
  return {
    ...state,
    userLoginAttempts: []
  }
}

export const onSyncComplete = state => {
  return {
    ...state,
    fetching: false
  }
}

export const onPersonLoaded = (state, action) => {
  return {
    ...state,
    fetching: false,
    current: action.data
  }
}

export const onPersonIdentificationLoaded = (state, { data }) => {
  return {
    ...state,
    fetching: false,
    identificationLink: data
  }
}

export const onPersonIdentificationsFetched = (state, { data }) => {
  return {
    ...state,
    fetching: false,
    personIdentifications: data
  }
}

export const onPersonIdentificationAttemptsFetched = (state, { data }) => {
  return {
    ...state,
    fetching: false,
    personIdentificationAttempts: data
  }
}

export const createPersonIdentificationRequest = (state, { data }) => {
  return {
    ...state,
    identificationLink: {
      url: 'Creating new link...',
      status: 'Loading...'
    }
  }
}

export const onSetAsLRRequest = state => {
  return {
    ...state,
    current: { ...state.current, is_legal_representative: 'Setting...' }
  }
}

export const onSetUboLRRequest = state => {
  return {
    ...state,
    current: { ...state.current, is_beneficial_owner: 'Setting...' }
  }
}

export const onLegalRepSet = state => {
  return {
    ...state,
    infoMsg:
      'Note that personal identification link has not been created. Please, do it manually!'
  }
}

export const updateUserActiveStateSuccess = (state, { isActive }) => {
  return {
    ...state,
    user: {
      ...state.user,
      is_active: isActive
    },
    fetching: false
  }
}

export const getPersonMobileNumberSuccess = (state, { data }) => {
  return {
    ...state,
    current: { ...state.current, verified_phone: data },
    fetching: false
  }
}

export const clearInfoMsg = state => {
  return {
    ...state,
    infoMsg: null
  }
}

export const updateUserPermissions = state => {
  return {
    ...state,
    fetching: true
  }
}

export const updateUserPermissionsSuccessful = (state, { data }) => {
  return {
    ...state,
    user: {
      ...state.user,
      permissions: {
        services: data.services
      },
      is_admin: data.is_admin
    },
    fetching: false
  }
}

export const updateUserPermissionsFailed = state => {
  return {
    ...state,
    fetching: false
  }
}

export const setUserPermissions = (state, { data }) => {
  return {
    ...state,
    user: {
      ...state.user,
      permissions: {
        services: data.services
      },
      is_admin: data.is_admin
    },
    fetching: false
  }
}

export const getUserPermissionsFailed = state => {
  return {
    ...state,
    fetching: false
  }
}

export const showEmailModal = (state, { user, personId }) => ({
  ...state,
  isEmailModalOpen: true,
  modalHeaderText: 'Update Email',
  modalUserObject: user,
  modalPersonId: personId
})

export const hideEmailModal = state => ({
  ...state,
  isEmailModalOpen: false,
  idChange: null,
  updateStep: 1,
  approver: null
})

export const patchPerson = (state, action) => ({
  ...state,
  fetching: true,
  patchPersonData: action.data
})

export const onPatchPersonChangeRequestReceived = (state, action) => ({
  ...state,
  idChange: action.idChange,
  isSubmitting: false,
  updateStep: 2,
  modalHeaderText: 'Select Approver',
  isEmailModalOpen: true,
  fetching: false
})

export const onApprovalRequested = (state, action) => ({
  ...state,
  isSubmitting: false,
  updateStep: 3,
  modalHeaderText: 'Confirm Change',
  approver: action.approver
})

export const onPatchPersonUpdated = state => ({
  ...state,
  isSubmitting: false,
  fetching: false,
  error: null,
  isEmailModalOpen: false,
  isAddressModalOpen: false,
  idChange: null,
  updateStep: 1,
  approver: null
})

export const showAddressModal = state => ({
  ...state,
  isAddressModalOpen: true,
  modalHeaderText: 'Update Address'
})

export const hideAddressModal = state => ({
  ...state,
  isAddressModalOpen: false,
  idChange: null,
  updateStep: 1,
  approver: null
})

export const onTrustpilotLinkReceived = (state, action) => ({
  ...state,
  trustpilotLink: action.url
})

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SYNC_PERSONS]: request,
  [Types.ON_SYNC_COMPLETE]: onSyncComplete,
  [Types.ON_SYNC_ERROR]: onSyncComplete,
  [Types.GET_PERSON]: request,
  [Types.ON_PERSON_LOADED]: onPersonLoaded,
  [Types.GET_PERSON_IDENTIFICATIONS]: request,
  [Types.GET_PERSON_IDENTIFICATION_ATTEMPTS]: request,
  [Types.CREATE_PERSON_IDENTIFICATION]: createPersonIdentificationRequest,
  [Types.ON_PERSON_IDENTIFICATION_LOADED]: onPersonIdentificationLoaded,
  [Types.ON_PERSON_IDENTIFICATIONS_FETCHED]: onPersonIdentificationsFetched,
  [Types.ON_PERSON_IDENTIFICATION_ATTEMPTS_FETCHED]: onPersonIdentificationAttemptsFetched,
  [AuthTypes.LOGOUT_SUCCESS]: () => INITIAL_STATE,
  [Types.SET_AS_LR]: onSetAsLRRequest,
  [Types.SET_AS_UBO]: onSetUboLRRequest,
  [Types.SET_CURRENT_USER]: setCurrentUser,
  [Types.SAVE_PERSON]: request,
  [Types.ON_LEGAL_REP_SET]: onLegalRepSet,
  [Types.UPDATE_USER_ACTIVE_STATE]: request,
  [Types.UPDATE_USER_ACTIVE_STATE_SUCCESS]: updateUserActiveStateSuccess,
  [Types.SYNC_PERSON_MOBILE_NUMBER]: request,
  [Types.GET_PERSON_MOBILE_NUMBER_SUCCESS]: getPersonMobileNumberSuccess,
  [Types.CLEAR_INFO_MSG]: clearInfoMsg,
  [Types.RESET_PERSON_STATE]: resetPersonState,
  [Types.UPDATE_USER_PERMISSIONS]: updateUserPermissions,
  [Types.UPDATE_USER_PERMISSIONS_SUCCESSFUL]: updateUserPermissionsSuccessful,
  [Types.UPDATE_USER_PERMISSIONS_FAILED]: updateUserPermissionsFailed,
  [Types.GET_USER_PERMISSIONS]: request,
  [Types.GET_USER_PERMISSIONS_FAILED]: getUserPermissionsFailed,
  [Types.SET_USER_PERMISSIONS]: setUserPermissions,
  [Types.PATCH_PERSON]: patchPerson,
  [Types.HIDE_EMAIL_MODAL]: hideEmailModal,
  [Types.SHOW_EMAIL_MODAL]: showEmailModal,
  [Types.ON_PATCH_PERSON_CHANGE_REQUEST_RECEIVED]: onPatchPersonChangeRequestReceived,
  [Types.ON_APPROVAL_REQUESTED]: onApprovalRequested,
  [Types.ON_PATCH_PERSON_UPDATED]: onPatchPersonUpdated,
  [Types.HIDE_ADDRESS_MODAL]: hideAddressModal,
  [Types.SHOW_ADDRESS_MODAL]: showAddressModal,
  [Types.SEND_ACTIVATION_EMAIL]: request,
  [Types.ON_SEND_ACTIVATION_EMAIL_SUCCESS]: onSyncComplete,
  [Types.ON_TRUSTPILOT_LINK_RECEIVED]: onTrustpilotLinkReceived,
  [Types.GET_USER_LOGIN_ATTEMPTS]: request,
  [Types.SET_USER_LOGIN_ATTEMPTS]: setUserLoginAttempts,
  [Types.RESET_USER_LOGIN_ATTEMPTS_STATE]: resetUserLoginAttempts
})
