import { createActions, createReducer } from 'reduxsauce'
import { AuthTypes } from './auth'

const { Types, Creators } = createActions({
  getPersons: ['businessId'],
  onPersonsReceived: ['data'],
  invitePersonRequest: ['businessId', 'personId', 'permissions'],
  invitePersonSuccess: [],
  invitePersonFailure: [],
  requestApprovalForPerson: ['idChange', 'person', 'businessId'],
})

export const PersonsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  fetching: false,
  persons: null,
  isInvitingPerson: false
}

/* ------------- Reducers ------------- */

export const request = state => {
  return {
    ...state,
    fetching: true
  }
}

const onInvitePersonRequest = state => ({ ...state, isInvitingPerson: true })

const onInvitePersonSuccess = state => ({ ...state, isInvitingPerson: false })

const onInvitePersonFailure = state => ({ ...state, isInvitingPerson: false })

export const onPersonsReceived = (state, action) => {
  return {
    ...state,
    persons: action.data,
    fetching: false
  }
}

export const submitting = state => {
  return {
    ...state,
    isSubmitting: true
  }
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_PERSONS]: request,
  [Types.ON_PERSONS_RECEIVED]: onPersonsReceived,
  [AuthTypes.LOGOUT_SUCCESS]: () => INITIAL_STATE,
  [Types.INVITE_PERSON_REQUEST]: onInvitePersonRequest,
  [Types.INVITE_PERSON_SUCCESS]: onInvitePersonSuccess,
  [Types.INVITE_PERSON_FAILURE]: onInvitePersonFailure,
  [Types.REQUEST_APPROVAL_FOR_PERSON]: submitting,
})
