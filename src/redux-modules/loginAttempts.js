import { createActions, createReducer } from 'reduxsauce'


const { Types, Creators } = createActions({
  getLoginAttempts: ['limit', 'offset', 'search'],
  setLoginAttempts: ['data'],
  resetLoginAttemptsState: null,
  updateSearchParams: ['search']
})

export const LoginAttemptsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  fetching: false,
  search : {
    userId: '',
    email: '',
    ip: ''
  },
  count: 0,
  pageNum: 1,
  numOfPages: 1,
  recordsPerPage: 50,
  loginAttempts: [],
}


/* ------------- Reducers ------------- */
export const setLoginAttempts = (state, action) => {
  return {
    ...state,
    fetching: false,
    count: action.data.count,
    numOfPages: Math.ceil(action.data.count / state.recordsPerPage) || 1 ,
    pageNum: Math.ceil(action.data.offset / state.recordsPerPage + 1 ),
    loginAttempts: action.data.data
  }
}

export const getLoginAttempts = (state, action) => {
  return {
    ...state,
    fetching: true
  }
}

export const updateSearchParams = (state, action) => {
  return {
    ...state,
    search: action.search
  }
}


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_LOGIN_ATTEMPTS]: getLoginAttempts,
  [Types.SET_LOGIN_ATTEMPTS]: setLoginAttempts,
  [Types.RESET_LOGIN_ATTEMPTS_STATE]: setLoginAttempts,
  [Types.UPDATE_SEARCH_PARAMS]: updateSearchParams
})
