import { createActions, createReducer } from 'reduxsauce'

const { Types, Creators } = createActions({
  getVat: ['businessId'],
  createVat: ['businessId', 'vatData'],
  onVatDataReceived: ['data'],
  deleteVatServicePeriod: ['businessId', 'id']
})

export const VatTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  vat: null,
  id: null,
  vat_default: null,
  service_start_date: null,
  service_end_date: null,
  isSubmitting: false,
  error: null
}

/* ------------- Reducers ------------- */

export const request = state => {
  return {
    ...state,
    isSubmitting: true
  }
}

export const onVatDataReceived = (state, { data }) => {
  return {
    ...state,
    vat: data.vat,
    id: data.id,
    vat_default: data.vat_default,
    service_start_date: data.service_start_date,
    service_end_date: data.service_end_date,
    isSubmitting: false
  }
}

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_VAT]: request,
  [Types.CREATE_VAT]: request,
  [Types.ON_VAT_DATA_RECEIVED]: onVatDataReceived
})
