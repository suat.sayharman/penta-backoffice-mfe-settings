import './polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'

import moment from 'moment'

import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'

import rootReducer from './reducers'
import rootSaga from './sagas'
import { Provider } from 'react-redux'
import { routerMiddleware } from 'react-router-redux'
import { setStore } from './api'
import { initSentry } from './utils/sentryHandler'

const history = createBrowserHistory()

if (process.env.REACT_APP_HOST_ENV !== 'LOCAL') {
  initSentry()
}

// create the saga middleware
const sagaMiddleware = createSagaMiddleware()
const routeMiddleware = routerMiddleware(history)
const middleware = [sagaMiddleware, routeMiddleware]

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

// mount it on the Store
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(...middleware))
)
setStore(store)

// then run the saga
sagaMiddleware.run(rootSaga)

moment.locale('en-AU')

const mount = el => {
  ReactDOM.render(
    <Provider store={store}>
      <Router history={history}>
        <App />
      </Router>
    </Provider>,
    el
  )
}

if (process.env.REACT_APP_HOST_ENV === 'LOCAL') {
  const sliceRoot = document.getElementById('slice-root')

  if (sliceRoot) {
    mount(sliceRoot)
  }
}

export { mount }
