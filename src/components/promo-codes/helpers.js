import _ from 'lodash'
import moment from 'moment'

export function getQuery(data) {
  const cleanData = Object.keys(data).reduce((acc, key) => {
    const value = data[key]
    if(!_.isEmpty(value)){
      acc[key] = value
    }
    if(_.isNumber(value) && !_.isNaN(value)){
      acc[key] = value
    }
    return acc
  }, {})
  const query = new URLSearchParams(cleanData).toString()
  return _.isEmpty(query) ? query : `?${query}`
}

export const formatFullDate = (date) => {
  if(!date){
    return null
  }
  const formattedDate = moment(date).utcOffset(0).format('DD/MM/YYYY')
  const time = moment(date).utcOffset(0).format('HH:mm:ss')
  return `${formattedDate} ${time}`
}

export const formatDate = (date) => {
  if(!date){
    return null
  }
  return moment(date, 'DD/MM/YYYY').format()
}

export const isDateExpired = (date) => {
  if(!date){
    return false
  }
  return moment(date).isBefore(new Date())
}