import React, { useEffect, useState } from 'react'
import { Redirect, Route } from 'react-router'
import { connect } from 'react-redux'
import { withLDConsumer } from 'launchdarkly-react-client-sdk'


const LDRoute = props => {
  const { requiredFlag, when, ...rest } = props
  const [flagsReady, setFlagsReady] = useState(false)

  useEffect(() => {
    if (props.ldClient && !flagsReady) {
      props.ldClient.waitUntilReady().then(() => {
        setFlagsReady(true)
      })
    }
  }, [props.ldClient])

  const shouldRedirect =
    props.redirectToHome &&
    flagsReady &&
    props.flags &&
    props.flags[requiredFlag] === false
  const hasFlag =
    !requiredFlag ||
    (flagsReady && props.flags && props.flags[requiredFlag] === true)
  const whenHasFlags = (!requiredFlag || (flagsReady && props.flags)) && hasFlag
  
  if (when) {
    return <Route {...rest} component={when(Boolean(whenHasFlags))} />
  }

  if (shouldRedirect) {
    return <Redirect to="/" />
  }
  return hasFlag ? <Route {...rest} /> : null
}

const mapStateToProps = (state) => {
  return {
    ldState: state.launchdarkly
  }
}

export const LaunchDarklyRoute = connect(
  mapStateToProps,
  null
)(withLDConsumer()(LDRoute))
