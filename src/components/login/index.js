import { connect } from 'react-redux'
import LoginForm from './views/LoginForm'
import AuthActions from '../../redux-modules/auth'

const mapStateToProps = state => {
  return {
    fetching: state.auth.fetching,
    errorMessage: state.auth.error
  }
}

export default connect(mapStateToProps, AuthActions)(LoginForm)
