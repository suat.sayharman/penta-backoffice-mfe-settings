import React, { Component } from 'react'
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Row
} from 'reactstrap'
import { Field, reduxForm } from 'redux-form'
import { requiredField, validateEmail } from '../../../utils/form-helper'
import Input from '../../common/Input'
import PageTitle from '../../common/PageTitle'

class LoginForm extends Component {
  handleFormSubmit = formProps => {
    return this.props.loginRequest(formProps.email, formProps.password)
  }

  render() {
    const { handleSubmit, fetching } = this.props
    return (
      <div className="app flex-row align-items-center">
        <PageTitle title="Login" />
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <form
                      noValidate
                      onSubmit={handleSubmit(this.handleFormSubmit)}
                    >
                      <Field
                        name="email"
                        type="email"
                        required
                        disabled={fetching}
                        placeholder="Email"
                        className="mb-3"
                        groupText={<i className="icon-user" />}
                        component={Input}
                      />

                      <Field
                        name="password"
                        type="password"
                        required
                        disabled={fetching}
                        placeholder="Password"
                        className="mb-4"
                        groupText={<i className="icon-lock" />}
                        component={Input}
                      />
                      <Row>
                        <Col xs="6">
                          <Button
                            color="primary"
                            type="submit"
                            className="px-4"
                            disabled={fetching}
                          >
                            Login
                          </Button>
                        </Col>
                      </Row>
                    </form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

const validate = values => {
  const errors = {}
  requiredField(values, errors, 'email')
  requiredField(values, errors, 'password')
  validateEmail(values, errors, 'email')
  return errors
}

export default reduxForm({
  validate,
  form: 'loginForm'
})(LoginForm)
