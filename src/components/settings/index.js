import React, { Component } from 'react'
import {
  Row, Col, Nav,
  NavLink, NavItem, TabPane, TabContent
} from 'reactstrap'
import PageTitle from '../common/PageTitle'
import RolePermissions from './role-permissions'
import UserRole from './user-role'
import { connect } from 'react-redux'

const TABS = {
  rolePermissions: 'role-permission',
  userRole: 'user-role'
}

class SettingsComponent extends Component {
  constructor(props) {
    super(props)
    this.state = { activeTab: TABS.rolePermissions }
  }

  toggleTabs(tab) {
    if (this.state.activeTab !== tab) {
      this.setState( { activeTab: tab })
    }
  }

  render() {
    const {
      activeTab
    } = this.state

    return (
      <Row>
        <PageTitle title="Settings" />
        <Col xs="12">
          <Nav tabs>
            <NavItem>
              <NavLink
                className={activeTab === TABS.rolePermissions ? 'active' : null}
                onClick={() => {
                  this.toggleTabs(TABS.rolePermissions)
                }}
              >
                Role Permissions
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={activeTab === TABS.userRole ? 'active' : null}
                onClick={() => {
                  this.toggleTabs(TABS.userRole)
                }}
              >
                User Roles
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={activeTab}>
            <TabPane tabId={TABS.rolePermissions}><RolePermissions /></TabPane>
            <TabPane tabId={TABS.userRole}><UserRole /></TabPane>
          </TabContent>
        </Col>
      </Row>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    myPermissions: state.permissions.myPermissions
  }
}

export default connect(mapStateToProps)(SettingsComponent)
