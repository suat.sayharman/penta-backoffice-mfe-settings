import { connect } from 'react-redux'
import PermissionActions from '../../../redux-modules/permissions'
import UserroleActions from '../../../redux-modules/userroles'
import { UserRole } from './views/UserRole'

const mapStateToProps = (state) => {
  return {
    users: state.userroles.users,
    roles: state.userroles.roles
  }
}

export default connect(mapStateToProps, {
  ...PermissionActions,
  ...UserroleActions
})(UserRole)
