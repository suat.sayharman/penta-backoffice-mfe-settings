import React, { useState, useCallback } from 'react'
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import styled from 'styled-components'
import { withPermission } from '../../../hoc/withPermission'
import { PERMISSIONS } from '../../../../constants/permissions'

const UserRoleDropdown = withPermission(Dropdown, [PERMISSIONS.settings.write], (props) => <span>{props.assignedRole}</span>)

const UserRoleDropdownToggle = styled(DropdownToggle)`
  min-width: 150px;
`

const dropdownMenuModifiers = {
  setMaxHeight: {
    enabled: true,
    order: 890,
    fn: (data) => ({
        ...data,
        styles: {
          ...data.styles,
          overflow: 'auto',
          maxHeight: '295px',
        },
    }),
  },
}

export const UserRoleLine = ({ user, roles, assignUserToRole }) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [selectedRole, setSelectedRole] = useState(user.role);

  const toggleDropdown = useCallback(() => {
    setDropdownOpen((prev) => !prev)
  }, [])

  const handleDropdownSelect = useCallback((event) => {
    setSelectedRole(event.target.value)
    assignUserToRole(user.id, event.target.value)
  }, [])

  return (
    <tr key={user.id}>
      <td>{user.id}</td>
      <td>
        <UserRoleDropdown size="sm" isOpen={dropdownOpen} toggle={toggleDropdown} assignedRole={user.role}>
          <UserRoleDropdownToggle caret>{selectedRole}</UserRoleDropdownToggle>
          <DropdownMenu modifiers={dropdownMenuModifiers}>
            {
              roles.filter(role => role.role_name !== user.role).map((role) => (
                <DropdownItem key={role.id} value={role.role_name} onClick={handleDropdownSelect}>{role.role_name}</DropdownItem>
              ))
            }
          </DropdownMenu>
        </UserRoleDropdown>
      </td>
      <td>{user.mail}</td>
    </tr>
  )
}
