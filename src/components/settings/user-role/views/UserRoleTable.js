import React from 'react'
import { Table } from 'reactstrap'
import styled from 'styled-components'

import { UserRoleLine } from './UserRoleLine'

const StyledTableWrapper = styled.div`
  width: 100%;

  .table-responsive {
    min-height: calc(100vh - 400px);
  }
`

export const UserRoleTable = ({ roles, users, assignUserToRole }) => (
  <StyledTableWrapper>
    <Table responsive striped hover>
      <thead>
        <tr>
          <th>Id</th>
          <th>Role</th>
          <th>Email</th>
        </tr>
      </thead>
      <tbody>
        {users.map(user => (
          <UserRoleLine
            key={user.id}
            user={user}
            roles={roles}
            assignUserToRole={assignUserToRole}
          />
        ))}
      </tbody>
    </Table>
  </StyledTableWrapper>
)
