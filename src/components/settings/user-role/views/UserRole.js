import React, { useEffect, useState, useCallback } from 'react';
import { Row, Col, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import styled from 'styled-components'
import { UserRoleTable } from './UserRoleTable'
import { DropDownInput } from '../../../activityLog/views/ActivityLogFilters'
import { HeaderWrapper, ClearAllFiltersWrapper } from '../../../activityLog/views/ActivityLogPage';

const ALL_USERS = 'All Users'
const UNASSIGNED_USERS = 'Unassigned Users'
const DELETED_USERS = 'Deleted Users'

const PREDEFINED_ROLES = [
  { id: ALL_USERS, role_name: ALL_USERS },
  { id: UNASSIGNED_USERS, role_name: UNASSIGNED_USERS },
  { id: DELETED_USERS, role_name: DELETED_USERS }
]

const INITIAL_FILTER_STATE = {
  group: ALL_USERS,
  email: null,
}

const RoleSelectionDropdown = styled(Dropdown)`
  button {
    height: 56px;
  }
`

export const UserRole = ({ users, roles, getBackofficeUsers, assignUserToRole }) => {
  const [dropdownOpen, setDropdownOpen] = useState(false)
  const [filteredUsers, setFilteredUsers] = useState(users)
  const [filters, setFilters] = useState(INITIAL_FILTER_STATE)

  useEffect(() => {
    getBackofficeUsers()
  }, [])

  useEffect(() => {
    let nextFilteredUsers = users

    Object.keys(filters).forEach(filter => {
      if (filter === 'group' && filters.group) {
        nextFilteredUsers = nextFilteredUsers.filter(user => {
          switch(filters.group) {
            case ALL_USERS:
              return true
            case UNASSIGNED_USERS:
              return !user.role
            case DELETED_USERS:
              return !user.is_active
            default:
              return user.role === filters.group
          }
        })
      } else if (filter === 'email' && filters.email) {
        nextFilteredUsers = nextFilteredUsers.filter(user => user.mail === filters.email.label)
      }
    })

    setFilteredUsers(nextFilteredUsers)
  }, [users, filters])

  const toggleDropdown = useCallback(() => {
    setDropdownOpen((prev) => !prev)
  }, [])

  const handleUserGroupFilterChange = (event) => {
    setFilters(prevFilters => ({
      ...prevFilters,
      group: event.target.value,
    }))
  }

  const handleUserEmailFilterChange = (email) => {
    setFilters(prevFilters => ({
      ...prevFilters,
      email,
    }))
  }

  const onResetFilters = () => {
    setFilters(INITIAL_FILTER_STATE)
  }

  return (
    <div>
      <HeaderWrapper>
        <ClearAllFiltersWrapper>
          <p onClick={onResetFilters}>Clear all filters</p>
        </ClearAllFiltersWrapper>
      </HeaderWrapper>
      <Row>
        <Col sm={6} md="auto" className="mb-3">
          <div className="mb-2">User Groups</div>
          <RoleSelectionDropdown isOpen={dropdownOpen} toggle={toggleDropdown}>
            <DropdownToggle caret>
              {filters.group}
            </DropdownToggle>
            <DropdownMenu>
              {
                [...PREDEFINED_ROLES, ...roles].map(({ id, role_name }) => (
                  <DropdownItem
                    key={id}
                    value={role_name}
                    onClick={handleUserGroupFilterChange}
                  >
                    {role_name}
                  </DropdownItem>
                ))
              }
            </DropdownMenu>
          </RoleSelectionDropdown>
        </Col>
        <Col sm={6} md={4} className="mb-3">
          <DropDownInput
            label="Users"
            options={users
              .sort((a, b) => a.mail.localeCompare(b.mail))
              .map(user => ({ value: user.id, label: user.mail }))
            }
            onItemSelectedHandler={handleUserEmailFilterChange}
            selectedItem={filters.email}
          />
        </Col>
      </Row>
      <Row>
        <UserRoleTable
          roles={roles}
          users={filteredUsers}
          assignUserToRole={assignUserToRole}
        />
      </Row>
    </div>
  )
}
