import React, { useCallback, useState } from 'react'
import styled from 'styled-components'
import { withPermission } from '../../../hoc/withPermission'
import { PERMISSIONS } from '../../../../constants/permissions'
import Notification from '../../../common/Notification'

const selectedUserRoleStyle = ({ selected }) => selected ? `
  color: #4dbd74;
  font-weight: bold;

  &:hover {
    background-color: #c8ced3;
  }

  i {
    color: #23282c;
    cursor: pointer;
    display: block;
  }

  i + i {
    margin-left: 8px;
  }
  ` : `
  &:hover {
    background-color: #c8ced3;
    color: #23282c;
  }

  i {
    display: none;
  }
`

const UserRoleLineContainer = styled.div`
  display: flex;
  align-items: center;
  border-bottom: 1px solid #c8ced3;
  padding: 1rem;
  ${selectedUserRoleStyle}
`;

const UserRole = styled.div`
  cursor: pointer;
  min-width: 150px;
  max-width: 300px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

const EditRole = styled.input`
  cursor: pointer;
  min-width: 150px;
  max-width: 300px;
  width: 100%;
  border: none;
  outline: none;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;

  &:hover {
    color: default;
  }
`

const SettingsWritePencilIcon = withPermission((props) => <i {...props} className="icon-pencil" />, [PERMISSIONS.settings.write])

const SettingsDeleteTrashIcon = withPermission((props) => <i {...props} className="icon-trash" />, [PERMISSIONS.settings.write])

export const UserRoleLine = ({ role, selectedRole, onSelect, onChangeDone, onDelete }) => {
  const [editMode, setEditMode] = useState(false);
  const [roleName, setRoleName] = useState(role.role_name);

  const changeDone = useCallback(() => {
    setEditMode(false);
    if (roleName.length > 3) {
      onChangeDone(role, roleName);
    } else {
      Notification.showFailureMsg('Please select longer than 3 characters as role name', 'Role Name Should be longer')
    }
  }, [roleName]);

  const handleInputKeydown = useCallback((event) => {
    if (event.key === 'Enter') {
      changeDone();
    }
  }, [changeDone]);

  const handleDeleteClick = useCallback(() => {
    onDelete(role)
  }, [role])

  return (
    <UserRoleLineContainer selected={selectedRole && selectedRole.id === role.id}>
      {
        editMode ? (
          <EditRole
            autoFocus
            onBlur={changeDone}
            onKeyDown={handleInputKeydown}
            value={roleName}
            onChange={(e) => setRoleName(e.target.value)}
          />
        ) : (
          <UserRole
            title={roleName}
            key={role.id}
            onClick={() => onSelect(role)}
          >
            {role.role_name}
          </UserRole>
        )
      }
      <SettingsWritePencilIcon onClick={() => {setEditMode(true)}} />
      <SettingsDeleteTrashIcon onClick={handleDeleteClick} />
    </UserRoleLineContainer>
  )
}
