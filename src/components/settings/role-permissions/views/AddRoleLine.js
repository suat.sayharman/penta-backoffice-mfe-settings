import React, { useCallback, useState } from 'react'
import styled from 'styled-components'
import Notification from '../../../common/Notification'

const selectedUserRoleStyle = ({ selected }) =>
  selected
    ? `
    color: #4dbd74;
    font-weight: bold;
  `
    : ''

const UserRole = styled.div`
  border-bottom: 1px solid #c8ced3;
  padding: 1rem;
  cursor: pointer;
  min-width: 150px;
  max-width: 300px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  ${selectedUserRoleStyle}

  &:hover {
    background-color: #c8ced3;
    color: default;
  }
`

const EditRole = styled.input`
  padding: 1rem;
  cursor: pointer;
  min-width: 150px;
  max-width: 300px;
  width: 100%;
  border: none;
  outline: none;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;

  &:hover {
    color: default;
  }
`

export const ADD_KEY = '##NEW_ROLE##'

export const AddRoleLine = ({ selectedRole, onAdd }) => {
  const [editMode, setEditMode] = useState(false)
  const [roleName, setRoleName] = useState('')

  const switchToEditMode = useCallback(() => {
    setEditMode(true)
  }, [])

  const changeDone = useCallback(() => {
    setEditMode(false)
    if (roleName.length > 3) {
      onAdd(roleName)
      setRoleName('')
    } else if (roleName.length > 0) {
      Notification.showFailureMsg(
        'Please select longer than 3 characters as role name',
        'Role Name Should be longer'
      )
    }
  }, [roleName])

  const handleInputKeydown = useCallback(
    event => {
      if (event.key === 'Enter') {
        changeDone()
      }
    },
    [changeDone]
  )

  return editMode ? (
    <EditRole
      autoFocus
      onBlur={changeDone}
      onKeyDown={handleInputKeydown}
      value={roleName}
      onChange={e => setRoleName(e.target.value)}
    />
  ) : (
    <UserRole
      title="+ ADD ROLE"
      selected={selectedRole && selectedRole.id === ADD_KEY}
      onClick={switchToEditMode}
    >
      + ADD ROLE
    </UserRole>
  )
}
