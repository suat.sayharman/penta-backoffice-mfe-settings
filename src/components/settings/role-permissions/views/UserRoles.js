import React, { useCallback } from 'react'
import styled from 'styled-components'
import { withPermission } from '../../../hoc/withPermission'
import { UserRoleLine } from './UserRoleLine'
import { AddRoleLine } from './AddRoleLine'

const UserRoleContainer = styled.div`
  padding-right: 1rem;
  border-right: 1px solid #c8ced3;
`

const SettingsWriteAddRole = withPermission(AddRoleLine, ['settings:write'])

export const UserRoles = ({ roles, onSelect, onAdd, onNameChange, onDelete, selectedRole }) => {
  const handleRoleSelection = useCallback((role) => {
    onSelect(role);
  }, []);

  const handleRoleChange = useCallback((role, newName) => {
    onNameChange(role.id, newName)
  }, []);

  return (
    <UserRoleContainer>
      {
        roles.map((role) => (
          <UserRoleLine
            key={role.id}
            role={role}
            selectedRole={selectedRole}
            onSelect={handleRoleSelection}
            onChangeDone={handleRoleChange}
            onDelete={onDelete}
          />
        ))
      }
      <SettingsWriteAddRole
        selectedRole={selectedRole}
        onAdd={onAdd}
      />
    </UserRoleContainer>
  )
}
