import React, { useCallback, useEffect, useState } from 'react'
import styled from 'styled-components'
import { UserRoles } from './UserRoles'
import { UserRolePermissions } from './UserRolePermissions'

const UserSettingsContainer = styled.div`
  display: flex;
`

export const RolePermissions = (props) => {
  const [selectedRole, setSelectedRole] = useState(null);
  const [addedRoleName, setAddedRoleName] = useState(null);

  useEffect(() => {
    props.getRoles();
  }, [props.getRoles]);

  useEffect(() => {
    if (addedRoleName) {
      const roleObject = props.userroles.roles.find((role) => role.role_name === addedRoleName);

      if (roleObject) {
        setSelectedRole(roleObject);
      } else if (props.userroles.roles.length) {
        setSelectedRole(props.userroles.roles[0]);
      }
    } else if (props.userroles.roles.length) {
      setSelectedRole(props.userroles.roles[0]);
    }
  }, [addedRoleName, props.userroles.roles.length])

  const onSelectionCallback = useCallback((role) => {
    setSelectedRole(role)
  }, [])

  const onAddCallback = useCallback((roleName) => {
    props.createRole(roleName);
    setAddedRoleName(roleName);
  }, []);

  const updateUserRole = useCallback((role, addToPermissions, removeFromPermissions) => {
    props.updateRolePermissions(role, addToPermissions, removeFromPermissions)
  }, [selectedRole]);

  return (
    <UserSettingsContainer>
      <UserRoles
        roles={props.userroles.roles}
        selectedRole={selectedRole}
        onSelect={onSelectionCallback}
        onAdd={onAddCallback}
        onNameChange={props.updateRoleName}
        onDelete={props.deleteRole}
      />
      {
        selectedRole && (
          <UserRolePermissions
            selectedRole={selectedRole}
            getRolePermissions={props.getRolePermissions}
            permissions={props.rolePermissions}
            saveNewPermissions={updateUserRole}
          />
        )
      }
    </UserSettingsContainer>
  )
};
