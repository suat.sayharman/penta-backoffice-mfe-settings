import React from 'react'
import styled from 'styled-components'
import { Input } from 'reactstrap'
import { withPermission } from '../../../hoc/withPermission'

const TableInput = styled(Input)`
  position: relative;
  margin: 0;
`

export const CenteredTableData = styled.td`
  text-align: center;
`

const PermissionWriteTableInput = withPermission(TableInput, ['settings:write'], (props) => <TableInput {...props} disabled={true} />)

export const UserRolePermissionData = ({
  permission,
  userPermissions,
  availablePermissions,
  onChange,
  disabled
}) => {
  const [,accessType] = permission.split(':')

  return (
    <CenteredTableData>
      <PermissionWriteTableInput type="checkbox"
                  id={permission}
                  disabled={disabled || !availablePermissions.includes(accessType)}
                  checked={userPermissions.includes(permission)}
                  onChange={() => {
                    onChange(permission);
                  }}
      />
    </CenteredTableData>
  );
}
