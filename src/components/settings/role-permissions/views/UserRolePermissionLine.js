import React, { useCallback } from 'react';
import { UserRolePermissionData } from './UserRolePermissionData'

export const UserRolePermissionLine = ({
  availablePermissions,
  userPermissions,
  permissionKey,
  disabled,
  onChange
}) => {
  const onPermissionChange = useCallback((permission) => {
    const newPermissions = [...userPermissions]
    if (!newPermissions.includes(permission)) {
      newPermissions.push(permission)
    } else {
      const index = newPermissions.findIndex((perm) => perm === permission)
      if (index > -1) {
        newPermissions.splice(index, 1)
      }
    }

    onChange(newPermissions)
  }, [userPermissions])

  return (
    <tr>
      <td className="td-20">{permissionKey}</td>
      <UserRolePermissionData
        permission={`${permissionKey}:read`}
        onChange={onPermissionChange}
        userPermissions={userPermissions}
        availablePermissions={availablePermissions}
        disabled={disabled}
      />
      <UserRolePermissionData
        permission={`${permissionKey}:write`}
        onChange={onPermissionChange}
        userPermissions={userPermissions}
        availablePermissions={availablePermissions}
        disabled={disabled}
      />
      <UserRolePermissionData
        permission={`${permissionKey}:delete`}
        onChange={onPermissionChange}
        userPermissions={userPermissions}
        availablePermissions={availablePermissions}
        disabled={disabled}
      />
    </tr>
  );
}
