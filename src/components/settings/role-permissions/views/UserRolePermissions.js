import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { Input, Button, Table } from 'reactstrap'
import { UserRolePermissionLine } from './UserRolePermissionLine'
import _ from 'lodash'

const ALL_PERMISSIONS = {
  business: ['read', 'write', 'delete'],
  announcement: ['read', 'write', 'delete'],
  maintenance: ['read', 'write'],
  loginattempts: ['read'],
  settings: ['read', 'write', 'delete'],
  finance: ['read', 'write', 'delete'],
  promocode: ['read', 'write'],
  developer: ['write'],
  notifications: ['read', 'write', 'delete'],
  onboardingfunnel: ['read', 'write', 'delete'],
  activitylogs: ['read']
}

export const UserPermissionsContainer = styled.div`
  padding: 1rem;
  width: 100%;
`

export const TableInput = styled(Input)`
  position: relative;
  margin: 0;
`

export const CenteredTableHeader = styled.th`
  text-align: center;
`

export const ApplyButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`

export const UserRolePermissions = props => {
  const [permissions, setPermissions] = useState([])

  useEffect(() => {
    if (props.selectedRole) {
      props.getRolePermissions(props.selectedRole.role_name)
    }
  }, [props.getRolePermissions, props.selectedRole])

  useEffect(() => {
    setPermissions(props.permissions)
  }, [props.permissions])

  return (
    <UserPermissionsContainer>
      <Table responsive striped hover>
        <thead>
          <tr>
            <th className="td-20">Permission</th>
            <CenteredTableHeader className="td-20">Read</CenteredTableHeader>
            <CenteredTableHeader className="td-20">Write</CenteredTableHeader>
            <CenteredTableHeader className="td-20">Delete</CenteredTableHeader>
          </tr>
        </thead>
        <tbody>
          {Object.keys(ALL_PERMISSIONS).map(key => {
            return (
              <UserRolePermissionLine
                key={key}
                userPermissions={permissions}
                availablePermissions={ALL_PERMISSIONS[key]}
                permissionKey={key}
                onChange={newPermissions => {
                  setPermissions(newPermissions)
                }}
              />
            )
          })}
        </tbody>
      </Table>
      <ApplyButtonContainer>
        <Button
          onClick={() => {
            const addedPermissions = _.difference(
              permissions,
              props.permissions
            )
            const removedPermissions = _.difference(
              props.permissions,
              permissions
            )
            props.saveNewPermissions(
              props.selectedRole,
              addedPermissions,
              removedPermissions
            )
          }}
        >
          Apply
        </Button>
      </ApplyButtonContainer>
    </UserPermissionsContainer>
  )
}
