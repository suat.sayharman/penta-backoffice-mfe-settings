import { connect } from 'react-redux'
import PermissionActions from '../../../redux-modules/permissions'
import UserroleActions from '../../../redux-modules/userroles'
import { RolePermissions } from './views/RolePermissions'

const mapStateToProps = state => {
  return {
    userroles: state.userroles,
    rolePermissions: state.permissions.rolePermissions
  }
}

export default connect(mapStateToProps, {
  ...PermissionActions,
  ...UserroleActions
})(RolePermissions)
