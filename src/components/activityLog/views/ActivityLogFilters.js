import React, {useEffect, useState, useRef} from 'react';
import {Row, Col, Input, InputGroup} from "reactstrap";
import {connect} from "react-redux";
import styled from "styled-components";
import Select  from 'react-select';
import Flatpickr from 'react-flatpickr';
import 'flatpickr/dist/themes/light.css';
import { english } from 'flatpickr/dist/l10n/default.js';
import moment from 'moment';

import {activityLogActions} from "../../../utils/mapCustomActivityLogActionNameToOriginalActionName";

const ActivityLogFiltersWrapper = styled.div`
  background: white;
  width: 100%;
  float: left;
  margin-bottom: 16px;
  padding: 24px;
  display: ${props => props.isHidden ? 'none' : 'block'};
`

const inputFieldStylesheet = {
  height: '56px',
  background: '#F5F5F7',
  fontSize: '14px',
  color: 'black',
  border: 'none',
  boxShadow: 'none',
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
}

const DateIsRequired = styled.p`
  color: gray;
  font-size: 10px;
`

const DateTimeRangeInputWrapper = styled.div`
  width: 100%;
  .row {
    margin: 0;
    padding: 0 16px;
    height: 56px;
    align-items: center;
    background: #F5F5F7;
    display: flex;
  }
  .row > div {
    padding: 0;
    flex-grow: 1;
    flex: 1 1;
    overflow: auto;
    width: auto;
  }

  .calendar, .clock, .arrow {
    max-width: 16px;
  }

  .picker {
    max-width: 50%;
  }

  .date-time-picker {
    background: #F5F5F7;
    border: none;
    padding: 6px;
    width: 100%;
  }
`

export const DropDownInput = ({
   label,
   options,
   onItemSelectedHandler,
   selectedItem,
   extraStyles,
}) => {
  return (
    <div>
      {label && <p className="mb-2">{label}</p>}
      <Select
        value={selectedItem}
        styles = {
          {
            control: (provided) => ({
              ...provided,
              height: '56px',
              border: 'none',
              background: '#F5F5F7',
              ...((extraStyles && extraStyles.control) || {}),
            }),
            singleValue: (provided) => ({
              ...provided,
              color: 'black',
            }),
            dropdownIndicator: (provided, state) => ({
              ...provided,
              transform: state.selectProps.menuIsOpen && 'rotate(180deg)',
              color: '#2B67F5'
            }),
            indicatorSeparator: () => ({
              display:'none'
            }),
            input: () => ({
              ...((extraStyles && extraStyles.input) || {}),
            })
          }
        }
        options={options}
        search
        emptyMessage="Not found"
        placeholder="Please select"
        onChange={onItemSelectedHandler}
      />
    </div>
  )
}

const TextInput = ({placeholder, onChangeHandler, value}) => {
  return (
    <div>
      <InputGroup>
        <p className="mb-2">Business ID</p>
        <Input
          style={inputFieldStylesheet}
          placeholder={placeholder}
          onChange={onChangeHandler}
          value={value}
        />
      </InputGroup>
    </div>
  )
}

const DateRangeInput = ({onStartDatePickedHandler, onEndDatePickedHandler, filters}) => {

  const dateFromFlatPickerRef = useRef(null);
  const dateToFlatPickerRef = useRef(null);

  useEffect(() => {
    if (!Object.keys(filters).length) {
      if (dateFromFlatPickerRef.current) {
        dateFromFlatPickerRef.current.flatpickr.clear();
      }
      if (dateToFlatPickerRef.current) {
        dateToFlatPickerRef.current.flatpickr.clear();
      }
    }
  }, [filters])

  return (
    <div>
      <p className="mb-2">Date</p>
      <DateTimeRangeInputWrapper>
        <Row>
          <Col sm={1} className="calendar mr-2">
            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M1.5 16C1.10228 15.9996 0.720958 15.8415 0.43973 15.5603C0.158502 15.279 0.000353055 14.8977 0 14.5L0 3.5C0.000353055 3.10228 0.158502 2.72096 0.43973 2.43973C0.720958 2.1585 1.10228 2.00035 1.5 2H4V0.5C4 0.367392 4.05268 0.240215 4.14645 0.146447C4.24021 0.0526784 4.36739 0 4.5 0C4.63261 0 4.75979 0.0526784 4.85355 0.146447C4.94732 0.240215 5 0.367392 5 0.5V2H11V0.5C11 0.367392 11.0527 0.240215 11.1464 0.146447C11.2402 0.0526784 11.3674 0 11.5 0C11.6326 0 11.7598 0.0526784 11.8536 0.146447C11.9473 0.240215 12 0.367392 12 0.5V2H14.5C14.8977 2.00035 15.279 2.1585 15.5603 2.43973C15.8415 2.72096 15.9996 3.10228 16 3.5V14.5C15.9996 14.8977 15.8415 15.279 15.5603 15.5603C15.279 15.8415 14.8977 15.9996 14.5 16H1.5ZM1 14.5C1 14.776 1.224 15 1.5 15H14.5C14.6326 15 14.7598 14.9473 14.8536 14.8536C14.9473 14.7598 15 14.6326 15 14.5V7H1V14.5ZM15 6V3.5C15 3.36739 14.9473 3.24021 14.8536 3.14645C14.7598 3.05268 14.6326 3 14.5 3H12V4C12 4.13261 11.9473 4.25979 11.8536 4.35355C11.7598 4.44732 11.6326 4.5 11.5 4.5C11.3674 4.5 11.2402 4.44732 11.1464 4.35355C11.0527 4.25979 11 4.13261 11 4V3H5V4C5 4.13261 4.94732 4.25979 4.85355 4.35355C4.75979 4.44732 4.63261 4.5 4.5 4.5C4.36739 4.5 4.24021 4.44732 4.14645 4.35355C4.05268 4.25979 4 4.13261 4 4V3H1.5C1.36739 3 1.24021 3.05268 1.14645 3.14645C1.05268 3.24021 1 3.36739 1 3.5V6H15Z" fill="#383E66"/>
            </svg>
          </Col>
          <Col sm={4} className="picker">
            <Flatpickr
              ref={dateFromFlatPickerRef}
              className="date-time-picker"
              placeholder="Start date"
              disabled={false}
              onChange={onStartDatePickedHandler}
              options={{
                allowInput: true,
                dateFormat: 'd.m.Y.',
                locale: {
                  ...english,
                  firstDayOfWeek: 1
                }
              }}
            />
          </Col>
          <Col sm={1} className="arrow mx-2">
            <svg width="17" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd" clipRule="evenodd" d="M8.9614 15.3536C8.76614 15.5488 8.44956 15.5488 8.25429 15.3536C8.05903 15.1583 8.05903 14.8417 8.25429 14.6464L14.4008 8.49997L0.607911 8.49997C0.331769 8.49997 0.107911 8.27611 0.107911 7.99997C0.107912 7.72382 0.331769 7.49997 0.607912 7.49997L14.4007 7.49997L8.2543 1.35355C8.05904 1.15829 8.05904 0.84171 8.2543 0.646448C8.44956 0.451186 8.76614 0.451186 8.96141 0.646448L15.9556 7.64069C16.0495 7.73159 16.1079 7.85896 16.1079 7.99997C16.1079 8.08393 16.0872 8.16305 16.0506 8.23253C16.0464 8.24057 16.042 8.24852 16.0373 8.25637C16.0271 8.27332 16.0159 8.28979 16.0036 8.30569C16.001 8.30901 15.9984 8.31231 15.9957 8.31558C15.984 8.32996 15.9715 8.34367 15.9583 8.35665L8.9614 15.3536Z" fill="#383E66"/>
            </svg>
          </Col>
          <Col sm={4} className="picker">
            <Flatpickr
              ref={dateToFlatPickerRef}
              className="date-time-picker"
              placeholder="End date"
              disabled={false}
              onChange={onEndDatePickedHandler}
              options={{
                allowInput: true,
                dateFormat: 'd.m.Y.',
                locale: {
                  ...english,
                  firstDayOfWeek: 1
                }
              }}
            />
          </Col>
        </Row>
      </DateTimeRangeInputWrapper>
    </div>
  )
}

const TimeRangeInput = ({ onStartTimePickedHandler, onEndTimePickedHandler, filters}) => {

  const timeFromFlatPickerRef = useRef(null);
  const timeToFlatPickerRef = useRef(null);

  useEffect(() => {
    if (!Object.keys(filters).length) {
      if (timeFromFlatPickerRef.current) {
        timeFromFlatPickerRef.current.flatpickr.clear();
      }
      if (timeToFlatPickerRef.current) {
        timeToFlatPickerRef.current.flatpickr.clear();
      }
    }
  }, [filters])

  return (
    <div>
      <p className="mb-2">Time</p>
      <DateTimeRangeInputWrapper>
        <Row>
          <Col sm={1} className="clock mr-2">
            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd" clipRule="evenodd" d="M8 3C4.96243 3 2.5 5.46243 2.5 8.5C2.5 11.5376 4.96243 14 8 14C11.0376 14 13.5 11.5376 13.5 8.5C13.5 5.46243 11.0376 3 8 3ZM1.5 8.5C1.5 4.91015 4.41015 2 8 2C11.5899 2 14.5 4.91015 14.5 8.5C14.5 12.0899 11.5899 15 8 15C4.41015 15 1.5 12.0899 1.5 8.5Z" fill="#383E66"/>
              <path fillRule="evenodd" clipRule="evenodd" d="M5.23938 8.5C5.23938 8.22386 5.46324 8 5.73938 8H8.00005C8.27619 8 8.50005 8.22386 8.50005 8.5C8.50005 8.77614 8.27619 9 8.00005 9H5.73938C5.46324 9 5.23938 8.77614 5.23938 8.5Z" fill="#383E66"/>
              <path fillRule="evenodd" clipRule="evenodd" d="M8 4.5C8.27614 4.5 8.5 4.72386 8.5 5V8.5C8.5 8.77614 8.27614 9 8 9C7.72386 9 7.5 8.77614 7.5 8.5V5C7.5 4.72386 7.72386 4.5 8 4.5Z" fill="#383E66"/>
            </svg>
          </Col>
          <Col sm={4} className="picker">
            <Flatpickr
              ref={timeFromFlatPickerRef}
              className="date-time-picker"
              placeholder={`Start time ${filters.dateFrom !== undefined ? '' : '*'}`}
              data-enable-time
              data-no-calendar
              disabled={filters.dateFrom === undefined}
              onChange={onStartTimePickedHandler}
              options={{
                allowInput: true,
                timeFormat: 'H:i'
              }}
            />
          </Col>
          <Col sm={1} className="arrow mx-2">
            <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd" clipRule="evenodd" d="M8.9614 15.3536C8.76614 15.5488 8.44956 15.5488 8.25429 15.3536C8.05903 15.1583 8.05903 14.8417 8.25429 14.6464L14.4008 8.49997L0.607911 8.49997C0.331769 8.49997 0.107911 8.27611 0.107911 7.99997C0.107912 7.72382 0.331769 7.49997 0.607912 7.49997L14.4007 7.49997L8.2543 1.35355C8.05904 1.15829 8.05904 0.84171 8.2543 0.646448C8.44956 0.451186 8.76614 0.451186 8.96141 0.646448L15.9556 7.64069C16.0495 7.73159 16.1079 7.85896 16.1079 7.99997C16.1079 8.08393 16.0872 8.16305 16.0506 8.23253C16.0464 8.24057 16.042 8.24852 16.0373 8.25637C16.0271 8.27332 16.0159 8.28979 16.0036 8.30569C16.001 8.30901 15.9984 8.31231 15.9957 8.31558C15.984 8.32996 15.9715 8.34367 15.9583 8.35665L8.9614 15.3536Z" fill="#383E66"/>
            </svg>
          </Col>
          <Col sm={4} className="picker">
            <Flatpickr
              ref={timeToFlatPickerRef}
              className="date-time-picker"
              placeholder={`End time ${filters.dateTo !== undefined ? '' : '*'}`}
              data-enable-time
              data-no-calendar
              disabled={filters.dateTo === undefined}
              onChange={onEndTimePickedHandler}
              options={{
                allowInput: true,
                timeFormat: 'H:i'
              }}
            />
          </Col>
        </Row>
      </DateTimeRangeInputWrapper>
    </div>
  )
}

const ActivityLogFilters = ({
  isHidden,
  filters,
  onFiltersChanged,
  backOfficeUsers
}) => {
  const [selectedBOUser, setSelectedBOUser] = useState(null)

  useEffect(() => {
    if (!Object.keys(filters).length) {
      setSelectedBOUser(null)
    }
  }, [filters])


  return (
    <ActivityLogFiltersWrapper isHidden={isHidden} id="activity-logs-filters-wrapper">
      <Row className="mx-0">
        <Col sm={12} md={4} className="pl-0 pr-2.5">
          <DropDownInput
            label="User"
            options={backOfficeUsers
              .sort((a, b) => a.mail.localeCompare(b.mail))
              .map(user => {
                return {
                  value: user.id,
                  label: user.mail
                }
              })
            }
            onItemSelectedHandler={item => {
              onFiltersChanged({...filters, userId: item.value})
              setSelectedBOUser(item)
            }}
            selectedItem={selectedBOUser}
          />
        </Col>
        <Col sm={12} md={4} className="px-2.5">
          <DropDownInput
            label="Activity"
            options={Object.keys(activityLogActions).map(action => {
              return {
                value: action,
                label: action
              }
            })}
            onItemSelectedHandler={(item) => onFiltersChanged({...filters, action: item.value})}
            selectedItem={filters.action !== undefined ? {value: filters.action, label: filters.action} : null}
          />
        </Col>
        <Col sm={12} md={4} className="pl-2.5 pr-0">
          <TextInput
            placeholder="Please type"
            onChangeHandler={(e) => onFiltersChanged({...filters, businessId: e.target.value})}
            value={filters.businessId !== undefined ? filters.businessId: ''}
          />
        </Col>
      </Row>
      <Row className="mt-4 mx-0">
        <Col sm={12} md={4} className="pl-0 pr-2.5">
          <DateRangeInput
            onStartDatePickedHandler={dates => {
              if (dates.length) {
                onFiltersChanged({
                  ...filters,
                  dateFrom: moment(dates[0]).format('YYYY-MM-DD')
                })
              }
            }}
            onEndDatePickedHandler={dates => {
              if (dates.length) {
                onFiltersChanged({
                  ...filters,
                  dateTo: moment(dates[0]).format('YYYY-MM-DD')
                })
              }
            }}
            filters={filters}
          />
        </Col>
        <Col sm={12} md={4} className="px-2.5">
          <TimeRangeInput
            onStartTimePickedHandler={times => {
              if (times.length) {
                onFiltersChanged({
                  ...filters,
                  timeFrom: moment(times[0]).format('HH:mm')
                })
              }
            }}
            onEndTimePickedHandler={times => {
              if (times.length) {
                onFiltersChanged({
                  ...filters,
                  timeTo: moment(times[0]).format('HH:mm')
                })
              }
            }}
            filters={filters}
          />
          { (filters.dateFrom === undefined || filters.dateTo === undefined) &&
            <DateIsRequired>&#42; date is required</DateIsRequired>
          }
        </Col>
      </Row>
    </ActivityLogFiltersWrapper>
  )
}

const mapStateToProps = (state) => {
  return {
    backOfficeUsers: state.userroles.users,
    filters: state.activityLogs.filters,
  }
}

export default connect(mapStateToProps)(ActivityLogFilters)
