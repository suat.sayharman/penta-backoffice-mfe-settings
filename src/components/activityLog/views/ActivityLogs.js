import React from 'react';
import {Table} from 'reactstrap';
import styled from 'styled-components'
import {connect} from 'react-redux';
import moment from 'moment';
import InfiniteScroll from 'react-infinite-scroll-component';
import {Link} from "react-router-dom";

import ActivityLogActions from "../../../redux-modules/activity-logs";
import {withPermission} from "../../hoc/withPermission";
import {PERMISSIONS} from "../../../constants/permissions";
import {mapActivityLogActionName} from "../../../utils/mapActivityLogActionName";

const StyledTableHeader = styled.thead`
  height: 48px;
  border-radius: 5px 5px 0px 0px;
  tr {
    th:first-child {
       padding-left: 24px;
    }
    th:last-child {
       text-align: right;
       padding-right: 24px;
    }
  }
`

const StyledTableHeaderCell = styled.th`
  width: 25%;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 140%;
  color: #383E66;
  border-top: none !important;
  border-bottom: 1px solid #F5F5F7   !important;
`

const StyledTableBodyRow = styled.tr`
  height: 54px;
  border: none;
  td {
    border-top: none;
    border-bottom: 1px solid #F5F5F7;
    padding: 16px;
  }
  td:first-child {
    padding-left: 24px;
  }
  td:last-child {
    text-align: right;
    padding-right: 24px;
  }
`

const StyledTableBodyCell = styled.td`
  font-weight: 500;
  font-size: 14px;
  line-height: 160%;
  color: #383E66;
`

const StyledLinkWrapper = styled.div`
  a {
    font-weight: 500;
    font-size: 14px;
    line-height: 160%;
    color: #2B67F5;
  }
`

const BusinessLink = withPermission(Link, [PERMISSIONS.business.read], (props) => <div {...props} />)

const ActivityLogs = ({
  activityLogs,
  getActivityLogs,
  activityLogsPagination,
  filters,
  backOfficeUsers,
}) => {
  const getAgentEmail = activityLog => {
    if (activityLog.user !==  undefined) return activityLog.user.mail
    const backOfficeUser = backOfficeUsers.find(user => user.id === activityLog.user_id.replace(/-/g, ""))
    if (backOfficeUser) return backOfficeUser.mail
    return null
  }
  return (
    <InfiniteScroll
      dataLength={activityLogs.length}
      next={() => {
        getActivityLogs(filters, activityLogsPagination.nextPage, activityLogsPagination.perPage )
      }}
      hasMore={activityLogsPagination.hasMore}
      loader={null}
      endMessage={null}
      style={{
        overflow: 'visible'
      }}
    >
      <div>
        <Table responsive className="bg-white px-4">
          <StyledTableHeader>
            <tr>
              <StyledTableHeaderCell>User</StyledTableHeaderCell>
              <StyledTableHeaderCell>Activity</StyledTableHeaderCell>
              <StyledTableHeaderCell>Business ID</StyledTableHeaderCell>
              <StyledTableHeaderCell>Date & Time</StyledTableHeaderCell>
            </tr>
          </StyledTableHeader>
          <tbody>
          {activityLogs.length
          ? activityLogs.map((activityLog, id) => {
                return (
                  <StyledTableBodyRow key={id}>
                    <StyledTableBodyCell>
                      { getAgentEmail(activityLog)}
                    </StyledTableBodyCell>
                    <StyledTableBodyCell>{mapActivityLogActionName(activityLog)}</StyledTableBodyCell>
                    <td>{}
                      {activityLog.business_id &&
                      <StyledLinkWrapper>
                        <BusinessLink to={`/businesses/${activityLog.business_id}`} className="font-weight-bold">
                          {activityLog.business_id}
                        </BusinessLink>
                      </StyledLinkWrapper>
                      }
                    </td>
                    <StyledTableBodyCell>
                      {moment(activityLog.created_at).format('DD.MM.YYYY HH:mm:ss')}
                    </StyledTableBodyCell>
                  </StyledTableBodyRow>
                )
          })
          : <tr>
              <td colSpan={100} className="text-center">
                <span>No results have been found.</span>
              </td>
            </tr>
          }
          </tbody>
        </Table>
      </div>
    </InfiniteScroll>
  )
}

const mapStateToProps = (state) => {
  return {
    activityLogs: state.activityLogs.activityLogs,
    activityLogsPagination: state.activityLogs.activityLogsPagination,
    filters: state.activityLogs.filters,
    backOfficeUsers: state.userroles.users,
  }
}
const mapDispatchToProps = {
  getActivityLogs: ActivityLogActions.getActivityLogs
}

export default connect(mapStateToProps, mapDispatchToProps)(ActivityLogs)
