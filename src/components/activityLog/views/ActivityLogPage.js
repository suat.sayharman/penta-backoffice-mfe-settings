import React, {useState, useEffect} from 'react'
import styled from "styled-components";

import ActivityLogFilters from "./ActivityLogFilters";
import ActivityLogs from "./ActivityLogs";

export const HeaderWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
`

export const ClearAllFiltersWrapper = styled.div`
  p {
    font-weight: 500;
    font-size: 14px;
    color: #2B67F5;
    border-bottom: 1px dashed #2B67F5;
    cursor: pointer;
  }
`

const ToggleFiltersIcon = styled.div`
  width: 40px;
  height: 40px;
  display: flex
  background: white
  align-items: center;
  justify-content: center;
  float: right;
  cursor: pointer;
  border-radius: 4px;
  margin-bottom: 16px;
  margin-left: 16px;
`

const ActivityLogPage = ({
  filters,
  activityLogsPagination,
  getActivityLogs,
  onFiltersChanged,
  onResetFilters,
  getBackofficeUsers,
}) => {
  useEffect(() => {
    getActivityLogs(filters, activityLogsPagination.nextPage, activityLogsPagination.perPage)
  }, [filters]);

  useEffect(() => {
    getBackofficeUsers()
  }, [])

  const [isFilterSectionHidden, setIsFilterSectionHidden] = useState(false)

  const toggleIsFilterSectionHidden = () => setIsFilterSectionHidden(!isFilterSectionHidden)

  const onFiltersChangedHandler = filters => onFiltersChanged(filters)

  return (
    <div>
      <HeaderWrapper>
        <ClearAllFiltersWrapper>
          <p onClick={onResetFilters}>Clear all filters</p>
        </ClearAllFiltersWrapper>
        <ToggleFiltersIcon onClick={toggleIsFilterSectionHidden}>
          <svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
            <path d="M6.98333 15.766C6.71816 15.7657 6.46395 15.6602 6.27638 15.4728C6.08881 15.2854 5.9832 15.0312 5.98267 14.766V8.58003C2.694 7.72737 0.246667 4.80537 0.00266667 1.40537C0.00133333 1.3807 0 1.3587 0 1.3367C0 1.07003 0.104 0.818701 0.292667 0.629367C0.481333 0.440701 0.732667 0.336034 1 0.336034H14.968C15.1103 0.335587 15.2511 0.36578 15.3808 0.424562C15.5104 0.483343 15.6259 0.569336 15.7193 0.676701C15.8953 0.877367 15.9833 1.1347 15.966 1.40203L15.9653 1.4087C15.7193 4.80803 13.2713 7.72937 9.98267 8.58003V13.0994C9.98333 13.3974 9.85133 13.6774 9.622 13.868L7.62267 15.534C7.44318 15.6836 7.21699 15.7657 6.98333 15.766ZM1.00067 1.33603C1.232 4.45537 3.52533 7.06803 6.58267 7.6927C6.81467 7.74003 6.98267 7.94603 6.98267 8.1827V14.766L8.98267 13.0994V8.1827C8.98267 7.94603 9.15067 7.74003 9.38267 7.6927C12.4467 7.06803 14.7433 4.45337 14.968 1.3347L1.00067 1.33603Z" fill="#383E66"/>
          </svg>
        </ToggleFiltersIcon>
      </HeaderWrapper>
      <ActivityLogFilters isHidden={isFilterSectionHidden} onFiltersChanged={onFiltersChangedHandler}/>
      <ActivityLogs />
    </div>
  )
}

export default ActivityLogPage;
