import React from 'react'
import { useMyPermissions } from '../../hooks/useMyPermissions'

export const withPermission = (WrappedComponent, neededPermissions, FailCaseComponent) => (props) => {
  const myPermissions = useMyPermissions()
  const isAllowed = neededPermissions.every((neededPermission) => myPermissions.includes(neededPermission))

  if (isAllowed) {
    return <WrappedComponent {...props} />
  }

  if (FailCaseComponent) {
    return <FailCaseComponent {...props} />
  }

  return null
}
