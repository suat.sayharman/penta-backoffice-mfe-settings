import React from 'react'
import { Input, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap'

export default ({
  input,
  placeholder,
  type,
  required,
  disabled,
  className,
  groupText,
  size,
  meta: { touched, error }
}) => {
  return groupText ? (
    <InputGroup className={className}>
      <InputGroupAddon addonType="prepend">
        <InputGroupText>{groupText}</InputGroupText>
      </InputGroupAddon>
      <Input
        {...input}
        type={type || 'text'}
        id={input.name}
        className={touched && error ? 'is-invalid' : ''}
        placeholder={placeholder}
        disabled={disabled}
        required={required}
      />
      {showErrorLabel(touched, error)}
    </InputGroup>
  ) : (
    <div>
      <Input
        bsSize={size}
        {...input}
        type={type || 'text'}
        id={input.name}
        className={touched && error ? 'is-invalid' : ''}
        placeholder={placeholder}
        disabled={disabled}
        required={required}
      />
      {showErrorLabel(touched, error)}
    </div>
  )
}

function showErrorLabel(touched, error) {
  if (touched && error) {
    return <div className="invalid-feedback">{error}</div>
  }
  return null
}
