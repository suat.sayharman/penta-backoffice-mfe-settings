import React, { Component } from 'react'
import { ToastContainer, ToastMessageAnimated } from 'react-toastr'

const ToastMessageFactory = React.createFactory(ToastMessageAnimated)

class Notification extends Component {
  componentDidMount() {
    Notification.instance = this
  }

  addSuccess = (msg, title) => {
    this.refs.container.success(title, msg, {
      timeOut: 3000,
      closeButton: false,
      extendedTimeOut: 0,
      showAnimation: 'animated slideInDown',
      hideAnimation: 'animated slideOutUp'
    })
  }

  addFailure = (msg, title) => {
    this.refs.container.error(title, msg, {
      timeOut: 0,
      closeButton: true,
      extendedTimeOut: 0,
      showAnimation: 'animated slideInDown',
      hideAnimation: 'animated slideOutUp'
    })
  }

  clear = () => {
    this.refs.container.clear()
  }

  render() {
    return (
      <ToastContainer
        ref="container"
        toastMessageFactory={ToastMessageFactory}
        className="toast-top-full-width"
      />
    )
  }
}

export default {
  Component: Notification,
  showSucessMsg(msg, title) {
    if (Notification.instance) {
      Notification.instance.addSuccess(msg, title)
    }
  },
  showFailureMsg(msg, title) {
    if (Notification.instance) {
      Notification.instance.addFailure(msg, title)
    }
  },
  clear() {
    if (Notification.instance) {
      Notification.instance.clear()
    }
  }
}
