import React from 'react'

export const getSortIndicator = (columnKey, order, order_direction) => {
  if (order === columnKey) {
    const sortOrder = order_direction.toLowerCase()
    return <i className={`fa fa-sort-${sortOrder}`} />
  }
  return <i className="fa fa-sort" />
}
