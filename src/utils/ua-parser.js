import UAParser from 'ua-parser-js'

export const parseDeviceInfo = userAgentString => {
  const ua = new UAParser(userAgentString)
  const parsedUA = ua.getResult()
  const deviceInfo = {
    os: parsedUA.os.name ? `${parsedUA.os.name} ${parsedUA.os.version || '' }` : '-',
    browser: parsedUA.browser.name ? `${parsedUA.browser.name} ${parsedUA.browser.version || ''}` : '-',
    device: parsedUA.device.vendor && parsedUA.device.model
      ? `${parsedUA.device.vendor} ${parsedUA.device.model} ${parsedUA.device.type || ''}`
      : '-'
  }

  return deviceInfo.os !== '-' || deviceInfo.browser !== '-' || deviceInfo.device !== '-'
    ? deviceInfo
    : { ua: parsedUA.ua }
}
