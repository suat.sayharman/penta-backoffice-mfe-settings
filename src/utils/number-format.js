export function formatNumberByLocale(value, round = 2) {
  const locale = 'en'

  return new Intl.NumberFormat(locale, {
    minimumFractionDigits: round
  }).format(value)
}
