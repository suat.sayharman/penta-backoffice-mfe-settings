import * as Sentry from '@sentry/react'
import { Integrations } from '@sentry/tracing'

const sentryConfig = {
  dsn: process.env.REACT_APP_SENTRY_DSN,
  integrations: [new Integrations.BrowserTracing()],
  tracesSampleRate: 1.0,
  environment: process.env.REACT_APP_HOST_ENV,
  release: process.env.REACT_APP_GIT_SHA,
  initialScope: { tags: { service: 'backoffice-frontend' } }
}

export const initSentry = () => {
  Sentry.init(sentryConfig)
}

export const registerExceptionInSentry = error => {
  Sentry.captureException(error)
}

export const registerErrorMessageInSentry = message => {
  Sentry.captureMessage(message)
}
