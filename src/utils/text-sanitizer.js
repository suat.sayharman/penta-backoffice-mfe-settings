const andReplacementMap = {
  en: 'and',
  de: 'und',
  it: 'e'
}

export const replaceSpecialCharacters = (text) => {
  const language = 'en'
  return text
    .replace(/Ä/g, 'Ae')
    .replace(/ä/g, 'ae')
    .replace(/Ö/g, 'Oe')
    .replace(/ö/g, 'oe')
    .replace(/Ü/g, 'Ue')
    .replace(/ü/g, 'ue')
    .replace(/ß/g, 'ss')
    .replace(/&/g, andReplacementMap[language] || andReplacementMap.en)
}

export const toSEPACharset = (text) => {
  return replaceSpecialCharacters(text)
    .replace(/#/g, 'N.')
    .replace(/_/g, '-')
    .replace(/`/g, "'")
    .replace(/[^a-z0-9/.,+:' -]/gi, '')
}