import React from 'react'
import { Badge } from 'reactstrap'
import _ from 'lodash'
import accounting from 'accounting'
import moment from 'moment'

export const isPerson = (user, persons) => {
  if (user) return !!_.find(persons, x => x.id_person === user.id_person)
  return true
}

export const renderIsBo = (user, row, persons) => {
  if (user && !isPerson(user, persons)) {
    return '-'
  }
  return row.is_beneficial_owner ? 'YES' : 'NO'
}

export const renderIsLr = (user, row, persons) => {
  if (user && !isPerson(user, persons)) {
    return '-'
  }
  return row.is_legal_representative ? 'YES' : 'NO'
}

export const renderShares = (user, row, persons) => {
  if (user && !isPerson(user, persons)) {
    return '-'
  }
  return row.shares
}

export const renderPersonStatus = state => {
  let color
  switch (state) {
    case 'successful':
      color = 'success'
      break
    case 'failed':
      color = 'danger'
      break
    case 'pending':
      color = 'warning'
      break
    default:
      break
  }
  return <Badge color={color}>{state}</Badge>
}

export const renderIsAdmin = user => {
  if (user) {
    return user.is_admin ? 'YES' : 'NO'
  }
  return '-'
}

export const renderIsPasswordSet = user => {
  if (user) {
    let color, text
    if (user.is_password_set) {
      color = 'success'
      text = 'Set'
    } else {
      color = 'danger'
      text = 'Unset'
    }

    return <Badge color={color}>{text}</Badge>
  }
  return '-'
}

export const renderUserActive = user => {
  if (user) {
    let color, text
    if (user.is_active) {
      color = 'success'
      text = 'Active'
    } else {
      color = 'danger'
      text = 'Inactive'
    }

    return <Badge color={color}>{text}</Badge>
  }
  return '-'
}

export const renderBusinessStatus = state => {
  let color
  switch (state) {
    case 'active':
      color = 'success'
      break
    case 'not identified':
      color = 'secondary'
      break
    case 'identified':
      color = 'primary'
      break
    case 'identification failed':
      color = 'danger'
      break
    case 'in identification':
      color = 'warning'
      break
    default:
      break
  }
  return <Badge color={color}>{state}</Badge>
}

export const returnRealm = user => {
  if (user) {
    return user.realm
  }
  return '-'
}
export const returnMail = user => {
  if (user) {
    return user.email
  }
  return '-'
}

export const isAuthorized = (user, authorizedPersons) => {
  if (user) {
    if (
      _.find(authorizedPersons, x => {
        return x.id_person === user.id_person
      })
    )
      return 'YES'
    return 'NO'
  }
  return '-'
}

export const mapPermissionObjectToArray = permissions => {
  let services = []
  if (permissions) {
    services = permissions.services
    if (permissions.is_admin) {
      services.push('is_admin')
    }
  }
  return services
}

export const translateServiceKeyToText = service => {
  switch (service) {
    case 'is_admin':
      return 'Administrator'
    case 'manageCards':
      return 'Manage Cards'
    case 'manageUsers':
      return 'Manage Users'
    case 'manageBusinesses':
      return 'Manage Business Details'
    case 'approveTransaction':
      return 'Send Payments'
    default:
      return 'Wrong key'
  }
}

export const translateExtrasCostsKeyToText = service => {
  switch (service) {
    case 'cards':
      return 'MasterCard'
    case 'transactions':
      return 'Transaction'
    case 'users':
      return 'User'
    case 'withdrawals':
      return 'Cash Withdrawal'
    case 'card_payments':
      return 'Card Payment'
    case 'non_eu_card_payments_basic':
      return 'Non EU Card Payments for Basic Plan'
    case 'non_eu_card_payments_advanced':
      return 'Non EU Card Payments for Advanced Plan'
    case 'non_eu_card_payments_premium':
      return 'Non EU Card Payments for Premium Plan'
    case 'non_eu_card_payments_starter':
      return 'Non EU Card Payments for Starter Plan'
    case 'non_eu_card_payments_comfort':
      return 'Non EU Card Payments for Comfort Plan'
    case 'non_eu_card_payments_enterprise':
      return 'Non EU Card Payments for Enterprise Plan'
    case 'sepa_direct_debit':
      return 'Sepa Direct Debit Collections'
    case 'sub_accounts':
      return 'Sub-Accounts'
    default:
      return 'Wrong key'
  }
}

export const translateCurrencyCloudSwiftItems = itemKey => {
  switch (itemKey) {
    case 'shared':
      return 'International Payments - SWIFT SHARED Processing Fees'
    case 'ours_cheap':
      return 'International Payments - SWIFT OURS (I) Processing Fees'
    case 'ours_expensive':
      return 'International Payments - SWIFT OURS (II) Processing Fees'
    default:
      return 'N/A'
  }
}

export const renderAnnouncContent = announc => {
  const { content_en, content_de } = announc
  let en = content_en.length > 250 ? content_en.slice(0, 250) : content_en
  let de = content_de.length > 250 ? content_de.slice(0, 250) : content_de
  return (
    <React.Fragment>
      <div title={content_en}>{en}</div>
      <div title={content_de}>{de}</div>
    </React.Fragment>
  )
}

export const renderAnnouncPermissionList = data => {
  const { permissions } = data
  const services = mapPermissionObjectToArray(permissions)
  return services.map(service => {
    return <li key={service}>{translateServiceKeyToText(service)}</li>
  })
}

export const buildFileUrl = billItem => {
  let url
  if (billItem.type === 'creditNote') url = `credit-notes/${billItem.id}`
  if (billItem.type === 'invoice') url = `invoices/${billItem.id}`
  return url
}

export const pricingPlanObject = plan => {
  switch (plan) {
    case 'basic':
      return {
        name: 'Basic',
        price: 0
      }
    case 'advanced':
      return {
        name: 'Advanced',
        price: 9
      }
    case 'premium-discounted':
      return {
        name: 'Premium (6 months discount)',
        price: 9
      }
    case 'starter':
      return {
        name: 'Starter',
        price: 9
      }
    case 'comfort':
      return {
        name: 'Comfort',
        price: 19
      }
    case 'enterprise':
      return {
        name: 'Enterprise',
        price: 49
      }
    default:
      return {
        name: 'Premium',
        price: 19
      }
  }
}

export function formatAmount(value) {
  return accounting.formatMoney(value, '', 2, ',', '.')
}

export const transactionTypes = [
  'sepa_credit_transfer',
  'sepa_standing_order',
  'sepa_timed_order',
  'credit_transfer_cancellation_sepa',
  'credit_transfer_cancellation_standing_order',
  'credit_transfer_cancellation_timed_order',
  'sepa_credit_transfer_return_sepa',
  'sepa_credit_transfer_return_standing_order',
  'sepa_credit_transfer_return_timed_order',
  'cancellation_sepa_credit_transfer_return',
  'direct_debit',
  'cancellation_direct_debit',
  'direct_debit_return',
  'cancellation_sepa_direct_debit_return',
  'purchase',
  'cash_atm',
  'cash_manual',
  'credit_presentment',
  'cancellation_card_transaction',
  'international_credit_transfer',
  'cancellation_international_credit_transfer'
]

export const categoryNames = {
  salaries_and_related: 'Salaries',
  sw_internet_services: 'Internet Services',
  travel_expenses: 'Travel expenses',
  food_drinks: 'Food & Drinks',
  office_related: 'Office Related',
  education_training: 'Education Training',
  profit_distribution: 'Profit Distribution',
  loan_repayment: 'Loan Repayment',
  investment_in_fixed_assets: 'Investment in Fixed Assets',
  services: 'Services',
  other_outflows: 'Other Outflows',
  loans_to_other_businesses: 'Loans to Other Businesses',
  office_material: 'Office Material',
  taxes_company_related: 'Taxes',
  office_rent: 'Office Rent',
  marketing_expenses: 'Marketing Expenses',
  atm_cash_withdrawals: 'ATM Cash Withdrawals',
  loans_repaid: 'Loans Repaid',
  sale_of_non_financial_assets: 'Sale of Non Financial Assets',
  interest_income: 'Interests Income',
  dividends: 'Dividends',
  other_inflows: 'Other Inflows',
  sale_of_financial_assets: 'Sale of Financial Assets',
  service_sale1: 'Service Sale 1',
  service_sale2: 'Service Sale 2',
  service_sale3: 'Service Sale 3',
  product_sale1: 'Product Sale 1',
  product_sale2: 'Product Sale 2',
  product_sale3: 'Product Sale 3',
  goods_sale1: 'Goods Sale 1',
  goods_sale2: 'Goods Sale 2',
  goods_sale3: 'Goods Sale 3',
  investment_in_finacial_assets: 'Investment in Financial Assets'
}

export const transactionTypeNames = {
  sepa_credit_transfer: 'SEPA Credit Transfer',
  sepa_timed_order: 'Payment (Timed Order)',
  sepa_standing_order: 'Payment (Standing Order)',
  credit_transfer_cancellation_sepa: 'Cancelled Payment',
  credit_transfer_cancellation_standing_order: 'Cancelled Payment',
  credit_transfer_cancellation_timed_order: 'Cancelled Payment',
  sepa_credit_transfer_return_sepa: 'Returned payment',
  sepa_credit_transfer_return_standing_order: 'Returned payment',
  sepa_credit_transfer_return_timed_order: 'Returned payment',
  cancellation_sepa_credit_transfer_return: 'Returned Payment Cancellation',
  direct_debit: 'Direct Debit',
  cancellation_direct_debit: 'Cancelled Direct Debit',
  direct_debit_return: 'Returned Direct Debit',
  cancellation_sepa_direct_debit_return: 'Returned Direct Debit cancellation',
  purchase: 'Card Payment',
  cash_atm: 'Cash Withdrawal',
  cash_manual: 'Cash Withdrawal',
  credit_presentment: 'Refund',
  cancellation_card_transaction: 'Cancelled Card Payment',
  international_credit_transfer: 'International Credit Transfer',
  intra_customer_transfer: 'Internal Transfer',
  cancellation_international_credit_transfer:
    'International Credit Transfer Cancellation',
  other: 'Other'
}

export const formatDateString = date => {
  return new Date(date).toLocaleDateString('en', {
    day: 'numeric',
    year: 'numeric',
    month: 'long'
  })
}

export const dateToString = (date, format = 'DD/MM/YYYY') => {
  return moment(date).format(format)
}

export const formatIban = iban => {
  if (iban) {
    return iban.match(/.{1,4}/g).join(' ')
  }
  return ''
}

export const TERMS_AND_POLICIES = {
  TERMS_AND_CONDITIONS: 'TERMS_AND_CONDITIONS',
  PRICE_AND_SERVICE_LIST: 'PRICE_AND_SERVICE_LIST',
  SOLARISBANK_DIRECT_DEBIT_COLLECTION_TERMS_AND_CONDITIONS:
    'SOLARISBANK_DIRECT_DEBIT_COLLECTION_TERMS_AND_CONDITIONS',
  SOLARISBANK_DIRECT_DEBIT_AGREEMENT: 'SOLARISBANK_DIRECT_DEBIT_AGREEMENT',
  SOLARISBANK_TERMS_AND_CONDITIONS: 'SOLARISBANK_TERMS_AND_CONDITIONS',
  PRIVACY_POLICY: 'PRIVACY_POLICY'
}

export const mapTermsOrigin = origin => {
  switch (origin) {
    case 'BUSINESS_CREATION':
      return 'BUSINESS CREATION'
    case 'EMAIL_LINK':
      return 'EMAIL LINK'
    case 'BUSINESS_ADMIN_LOGIN':
      return 'BUSINESS ADMIN LOGIN'
    case 'CHANGE_PLAN':
      return 'CHANGE PLAN'
    case 'NEW_FEATURE_ACTIVATION':
      return 'NEW FEATURE ACTIVATION'
    case 'UNKNOWN':
    default:
      return 'UNKNOWN'
  }
}
