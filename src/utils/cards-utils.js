export const getHumanReadableCardType = type => {
  switch (type) {
    case 'VISA_BUSINESS_DEBIT':
      return 'VISA'
    case 'MASTERCARD_BUSINESS_DEBIT':
    case 'MASTERCARD_DEBIT':
      return 'MasterCard'
    default:
      return ''
  }
}

export const getCardStatusColor = status => {
  switch (status) {
    case 'active':
    case 'pending_pin':
      return 'success'
    case 'frozen':
      return 'info'
    case 'requested':
      return 'secondary'
    case 'blocked':
    case 'closed':
    case 'close_pending':
    case 'closed_by_solaris':
      return 'danger'
    case 'pending':
    case 'request pending':
      return 'warning'
    default:
      return ''
  }
}

export const getHumanReadableCardStatus = status => {
  switch (status) {
    case 'active':
      return 'Active'
    case 'pending_pin':
      return 'Pending PIN'
    case 'frozen':
      return 'Frozen'
    case 'requested':
      return 'Inactive'
    case 'blocked':
      return 'Blocked'
    case 'closed':
    case 'close_pending':
    case 'closed_by_solaris':
      return 'Closed'
    case 'request pending':
      return 'Pending'
    default:
      return ''
  }
}

export const formatCardLimit = (value, locale = 'en', round = 2) => {
  return new Intl.NumberFormat(locale, { minimumFractionDigits: round }).format(
    value
  )
}

export const transformCentsToEuros = value => {
  return parseFloat(value / 100)
}

export const isCardClosed = status => {
  return ['closed', 'close_pending', 'closed_by_solaris'].includes(status)
}

export const areLimitsAvailable = status => {
  return ![
    'closed',
    'close_pending',
    'closed_by_solaris',
    'blocked',
    'request pending'
  ].includes(status)
}
