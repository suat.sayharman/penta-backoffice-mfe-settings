export const getApprovalMethod = () => {
  if (
    process.env.REACT_APP_HOST_ENV === 'PROD' ||
    process.env.REACT_APP_HOST_ENV === 'STAGE'
  ) {
    return 'mobile_number'
  } else {
    return 'static'
  }
}

export const getPentaBusinessId = () => {
  return process.env.REACT_APP_PENTA_BUSINESS_ID
}
