export function onKeyDown(event) {
  if (!isValidCharacter(event)) {
    event.preventDefault()
  } else {
    handleKeyInput(event)
  }
}

function isNumericInput(key) {
  return (key >= 48 && key <= 57) || (key >= 96 && key <= 105)
}

function handleKeyInput(event) {
  const key = event.keyCode || event.which
  const currentElement = event.currentTarget

  // prevent adding more than 1 character - can happen when user types very fast;
  // prevent characters other than 0-9 (number input accepts '.' or 'e') and
  // don't allow user to enter more characters when reached the end of TAN (6th digit)
  if (key !== 8) {
    handleStandardInput(key, event, currentElement)
  } else {
    handleBackInput(currentElement)
  }
}

function handleBackInput(currentElement) {
  if (
    currentElement.value.length === 0 &&
    currentElement.previousSibling !== null
  ) {
    // move to previous element when 'Back' is pressed if value is empty
    currentElement.previousSibling.focus()
  }
}

function handleStandardInput(key, event, currentElement) {
  // this is used to check if field already has a value - onFocus() will select current value
  const selectedText = getSelectedText(currentElement)

  if (
    (currentElement.value.length === 1 && !selectedText) ||
    !isNumericInput(key)
  ) {
    event.preventDefault()
    if (currentElement.nextSibling !== null && isNumericInput(key)) {
      currentElement.nextSibling.focus()
    }
  }
}

function getSelectedText(element) {
  return element.value
    ? element.value.substring(element.selectionStart, element.selectionEnd)
    : ''
}

function isValidCharacter(event) {
  const key = event.keyCode || event.which
  return (
    !event.shiftKey &&
    !event.ctrlKey &&
    !event.altKey &&
    (isNumericInput(key) || key === 8)
  )
}

export function onKeyUp(event) {
  const key = event.keyCode || event.which
  const currentElement = event.currentTarget
  if (currentElement.value.length === 1) {
    if (currentElement.nextSibling !== null && isNumericInput(key)) {
      currentElement.nextSibling.focus()
    }
  }
}
