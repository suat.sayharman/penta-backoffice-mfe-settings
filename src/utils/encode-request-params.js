export const convertRequestParamsToQueryString = requestParams => {
  const queryParams = Object.keys(requestParams)
    .filter(key => {
      const value = requestParams[key]
      return value !== null && (Array.isArray(value) ? value.length > 0 : true)
    })
    .map(key => {
      const value = requestParams[key]
      return encodeValue(key, value)
    })

  return queryParams.length > 0 ? `?${queryParams.join('&')}` : ``
}

const encodeValue = (key, value) => {
  return Array.isArray(value)
    ? encodeArrayValue(key, value)
    : encodeFieldValue(key, value)
}

const encodeFieldValue = (key, value) => {
  return `${encodeURIComponent(key)}=${encodeURIComponent(value)}`
}
const encodeArrayValue = (key, value) => {
  return `${encodeURIComponent(key)}=${encodeURIComponent(
    JSON.stringify(value)
  )}`
}
