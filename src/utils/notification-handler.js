import Notification from '../components/common/Notification'

export const showErrorNotification = response => {
  const { response: originalResponse } = response.originalError
  if (originalResponse.data.code) {
    const action = (originalResponse.data.data && originalResponse.data.data.action) || ''
    Notification.showFailureMsg(originalResponse.data.code, action)
  } else {
    Notification.showFailureMsg(
      response.problem,
      `Unhandled Error, status: ${originalResponse.status}`
    )
  }
}

export const showErrorMessage = (action, title) => {
  Notification.showFailureMsg(action, title)
}

export const showSuccessNotification = message => {
  Notification.showSucessMsg(message)
}
