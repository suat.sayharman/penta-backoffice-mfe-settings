const mapBusinessUpdateActionName = activityLog => {
  if (activityLog.body && activityLog.body.address !== undefined) return 'Edited Business Address'
  else if (activityLog.body && activityLog.body.vat_number !== undefined) return 'Edited Business VAT Number'
  else if (activityLog.body && activityLog.body.business_purpose !== undefined) return 'Edited Business Purpose'
  else if (activityLog.body && activityLog.body.crs_company_type !== undefined) return 'Edited Business CRS Company Type'
  else if (activityLog.body && activityLog.body.ticket_status !== undefined) return 'Edited Business Ticket Status'
  return activityLog.action
}

const mapPersonUpdateActionName = activityLog => {
  if (activityLog.body && activityLog.body.email !== undefined) return 'Edited User Email'
  else if (activityLog.body && activityLog.body.address !== undefined) return 'Edited User Address'
  return 'Edited User Details'
}

const mapMaintenancePageActionName = activityLog => {
  if (!activityLog.body) return activityLog.action
  return activityLog.body.status ? 'Turned On Maintenance Page' : 'Turned Off Maintenance Page'
}

const mapIsUserActiveActionName = activityLog => {
  if (!activityLog.body) return activityLog.action
  return activityLog.body.is_active ? 'Activated user' : 'Deactivated user'
}

const mapActionName = (activityLog) => {
  const actions = {
    account_closure_create: 'Created churn reason and comment',
    account_closure_delete: 'Deleted churn reason and comment',
    account_closure_read: 'Viewed churn reason and comment',
    account_closure_update: 'Edited churn reason and comment',
    mandate_download: 'Downloaded Mandate PDF',
    maintenance_read: 'Viewed Maintenance Page',
    accounts_list: 'Viewed list of all accounts',
    activity_log_search: 'Searched the activity log',
    announcement_delete: 'Deleted announcement',
    announcement_read: 'Viewed announcement',
    announcement_write: 'Edited announcement',
    approval_confirm: 'Approved a change request',
    approval_request: 'Requested an approval for a change',
    authorized_users_create: 'Added new authorized person (approvers)',
    backoffice_user_logout: 'Logged out',
    balance_read: 'Viewed balance',
    beneficial_owners_create: 'Edited the person as UBO',
    business_delete: 'Deleted a business',
    business_read: 'Viewed the businesses details',
    business_search: 'Searched businesses',
    business_synchronize: 'Synced businesses details',
    business_credit_report_create: 'Uploaded direct debit credit report',
    business_documents_download: 'Downloaded documents',
    business_permissions_list: 'Viewed business permissions and roles',
    direct_debit_profile_create: 'Created direct debit profile',
    direct_debit_profile_read: 'Viewed direct debit profile',
    cached_change_update: 'Updated approval change request',
    cards_list: 'Viewed cards ',
    cards_read: 'Viewed cards details',
    compliance_question_read: 'Viewed compliance questions',
    compliance_questions_list: 'viewed list of compliance questions',
    costs_breakdown_read: 'Viewed subscription page',
    credits_update: 'Updated credits, viewed subscription page',
    credits_view: 'Viewed subscription page',
    credits_view_remaining: 'Viewed subscription page',
    currency_cloud_conversions_list: 'Currency Cloud Statement Downloaded',
    currency_cloud_costs_read: 'Viewed subscription page',
    direct_debit_profile_collection_limits_read: 'Viewed collection limit for the direct debit profile',
    files_list: 'Viewed files',
    files_upload: 'Uploaded a file',
    free_trial_list: 'Viewed free trial period',
    free_trial_read: 'Viewed free trial period',
    free_trial_update: 'Edited free trial period',
    identification_mark_as_ready: 'Marked an identification as Ready',
    identification_request: 'Requested a new identification',
    identification_process_initialize: 'Initiated the identification process for the Business',
    identification_process_read: 'Viewed the identification process details',
    identifications_list: 'Viewed list of all identification',
    identifications_read: 'Viewed identification details',
    legal_representatives_create: 'Adds person as legal representative',
    login_attempts_read: 'Viewed login attempts page',
    missing_documents_list: 'Listed missing onboarding documents',
    new_user_email_create: 'Created new email',
    onboarding_request_read: 'Viewed onboarding request details',
    onboarding_request_reject: 'Rejected onboarding request',
    password_change_request_create: 'Requested password change',
    person_read: 'Viewed person details',
    person_synchronize: 'Synced person details',
    persons_list: 'Viewed list of all persons',
    pricing_plan_cancel_downgrade: 'Canceled downgrade of the plan',
    Pricing_plan_update: 'Edited the pricing plan',
    redis_task_create: 'Created a redis task',
    reservations_list: 'Viewed list of all reservations',
    scheduled_orders_list: 'Viewed list of all scheduled orders',
    seizures_read: 'Viewed seizures',
    tax_identifications_create: 'Created a tax identification record',
    tax_identifications_list: 'Viewed list all tax identifications for a Business',
    terms_and_policies_list: 'Viewed accepted terms and policies',
    transactions_list: 'Viewed list of transactions',
    transactions_read: 'Viewed transactions',
    trustpilot_review_link_create: 'Created trust pilot link',
    usage_breakdown_read: 'Viewed usage breakdown',
    user_create: 'Created a user',
    user_delete: 'Deleted a user',
    user_permissions_update: 'Edited permissions',
    verified_mobile_number_list: 'Viewed list of mobile numbers',
    business_identification_read: 'Viewed list of business identifications',
    direct_debit_profile_update: 'Edited a Direct Debit profile',
    find_direct_debit_submission_details_read: 'Viewed Direct Debit submission details',
    person_create: 'Created a person',
    user_permissions_list: 'List of all user permissions',
    business_state_update: 'Edited a business state',
    user_email_update: 'Edited a user email',
    business_role_insert: 'Created a business role',
    business_role_get: 'Viewed business role details',
    business_role_delete: 'Delete business role',
    mandate_generate: 'Generated mandate',
    mandate_read: 'Viewed the mandate details',
    mandate_list: 'Viewed list of all mandates',
    document_upload: 'Uploaded document',
    document_download: 'Downloaded document',
    promo_code_list: 'Viewed list of all promo codes',
    promo_code_create: 'Created a promo code',
    promo_code_update: 'Edited a promo code',
    referral_view: 'Viewed referral details',
    referral_list: 'Viewed list of all referrals',
    user_role_list: 'Viewed list of all roles',
    user_role_list_my_roles: 'Viewed list of all my roles',
    user_role_create: 'Created a user role',
    user_role_assign: 'Assigned a user role',
    role_permissions_list: 'Viewed list of all permissions',
    vat_view: 'Viewed a VAT',
    vat_create: 'Created a VAT',
    vat_delete: 'Deleted a VAT',
    business_credit_note_list: 'Viewed a list of all credit notes',
    business_credit_note_download_pdf: 'Downloaded a credit note pdf',
    business_credit_note_list_signed: 'Viewed a list of signed credit notes',
    business_credit_note_create: 'Created a business credit note',
    business_credit_note_canceled: 'Canceled a credit note',
    business_credit_note_update: 'Edited a credit note',
    business_credit_note_remove: 'Deleted a credit note',
    invoice_view: 'Viewed an invoice details',
    signed_invoice_view: 'Viewed a signed invoice details',
    business_invoice_view: 'Viewed a business invoice details',
    invoice_cancel: 'Canceled an invoice',
    invoice_status_create: 'Created an invoice status',
  }
  const actionName = activityLog.action.replace(/:/g, '_')
  return actions[actionName] === undefined ? activityLog.action : actions[actionName]
}

export const mapActivityLogActionName = (activityLog) => {
  switch (activityLog.action) {
    case 'business:update':
      return mapBusinessUpdateActionName(activityLog)
    case 'person:update':
      return mapPersonUpdateActionName(activityLog)
    case 'maintenance:create':
      return mapMaintenancePageActionName(activityLog)
    case 'is_user_active:update':
      return mapIsUserActiveActionName(activityLog)
    default:
      return mapActionName(activityLog)
  }
}
