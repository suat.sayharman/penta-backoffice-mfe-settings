const contentDispositionFileNameRegExp = /filename=(.+);?$/

export const getFileNameFromContentDisposition = header => {
  const matches = header.match(contentDispositionFileNameRegExp)
  if (matches && matches[1]) {
    try {
      // `filename` value might or might not be inside `"`;
      return JSON.parse(matches[1])
    } catch (_) {
      return matches[1]
    }
  }
  return undefined
}
