import moment from 'moment'
import _ from 'lodash'

export const requiredField = (values, errors, name) => {
  if (!values[name]) {
    errors[name] = 'Please enter this field.'
  }
}

export const requiredNumberField = (values, errors, name) => {
  validateNumber(values, errors, name)
  if (values[name] !== 0) {
    requiredField(values, errors, name)
  }
}

export const validateEmail = (values, errors, name) => {
  if (
    values[name] &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,24}$/i.test(values[name])
  ) {
    errors[name] = 'Invalid email address.'
  }
}

export const validatePhone = (values, errors, name) => {
  // Removes space before validation to allow user to enter space.
  var numberWithoutSpace = values[name]
    ? values[name].replace(/\s/g, '')
    : undefined
  // Alows number to begin with 00 or +.
  if (
    numberWithoutSpace &&
    !/^(00|\+)[1-9]\d{7,14}$/i.test(numberWithoutSpace)
  ) {
    errors[name] = 'Invalid phone number!'
  }
}

export const validateDate = (values, errors, name, maxDate, minDate) => {
  if (values[name]) {
    const format = 'DD/MM/YYYY'
    const mDate = moment(values[name], format, true)
    if (!mDate.isValid()) {
      errors[name] = 'Invalid date'
    } else {
      if (maxDate && mDate.isAfter(moment(maxDate, format, true), 'day')) {
        errors[name] = `Date can't be after: ${moment(maxDate, format).format(
          'L'
        )}`
      }
      if (minDate && mDate.isBefore(moment(minDate, format, true), 'day')) {
        errors[name] = `Date can't be before: ${moment(minDate, format).format(
          'L'
        )}`
      }
    }
  }
}

export const validateDateRange = (values, errors, fieldNames) => {
  const [start, end] = fieldNames
  const startDate = values[start]
  const endDate = values[end]
  validateDate(values, errors, start, endDate)
  validateDate(values, errors, end, null, startDate)
}

export const minLengthRequired = (values, errors, name, min) => {
  const value = values[name]
  if (value && value.length < min) {
    errors[name] = `minimum ${min} characters are required`
  }
}

export const maxLengthExceeded = (values, errors, name, max) => {
  const value = values[name]
  if (value && value.length > max) {
    errors[name] = `Max ${max} characters are allowed`
  }
}

export const checkAllowedAddressCharacters = (values, errors, name) => {
  // eslint-disable-next-line
  const regEx = /^[âäàáãåçñ¢.(+|&éêëèíîïìß!*)-/ÂÄÀÁÃÅÇÑ,%_?`øÉÊËÈÍÎÏÌ:#@'="Øabcdefghiýjklmnopqrstuvwxyz¡¿Ý®©´ABCDEFGHIôöòóõJKLMNOPQRûüùúÿ÷STUVWXYZÔÖÒÓÕ0123456789ÛÜÙÚ\s]+$/
  const text = values[name]
  if (text) {
    if (!regEx.test(text)) {
      let invalid = []
      text.split('').forEach(char => {
        if (!regEx.test(char)) {
          if (!invalid.includes(char)) {
            invalid.push(char)
          }
        }
      })
      errors[name] = `Please don't use "${invalid.join('", "')}"`
    }
  }
}

export const trimFormFields = formObj => {
  let formData = _.omit(formObj, [undefined])
  Object.keys(formData).forEach(prop => {
    const item = formData[prop]
    if (item) {
      // trim fields if they're strings
      if (item instanceof String || typeof item === 'string') {
        formData[prop] = item.trim()
      } else if (item === Object(item)) {
        // if detected that field contains object, go recursively through its props and trim
        trimFormFields(item)
      }
    }
  })

  return formData
}

export const validateLink = (values, errors, name) => {
  const externalRegex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/i
  const value = values[name]
  if (value && !externalRegex.test(value) && !value.startsWith('/')) {
    errors[name] = 'Invalid link format.'
  }
}

export const validateNumber = (values, errors, name) => {
  const value = parseInt(values[name], 10)
  const regex = /^\d+(\.,\d+)?$/
  if (isNaN(value) || !regex.test(values[name])) {
    errors[name] = 'Only numbers accepted.'
  }
}

export const requiredOption = (values, errors, name) => {
  if (values[name] && values[name].length === 0) {
    errors[name] = 'Please select one option.'
  }
}

export const validatePromoCode = (values, errors, name) => {
  const value = values[name]
  if (!value) {
    return
  }

  const isPromoCodeValid = /^[A-Za-z0-9]+$/g.test(value)
  if (!isPromoCodeValid) {
    errors[name] = 'Promo code should not contain special characters'
  }

  const hasWhiteSpace = /\s/g.test(value)
  if (hasWhiteSpace) {
    errors[name] = 'Promo code should not contain space(s)!'
  }
}
