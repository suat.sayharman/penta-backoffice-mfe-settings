import { parse } from 'uuid'
import { Base64 } from 'js-base64'

export function encodeBusinessIdToReference(businessId) {
  const p1 = businessId.substr(0, 8)
  const p2 = businessId.substr(8, 4)
  const p3 = businessId.substr(12, 4)
  const p4 = businessId.substr(16, 4)
  const p5 = businessId.substr(20)
  const binary = parse(`${p1}-${p2}-${p3}-${p4}-${p5}`)
  return Base64.fromUint8Array(binary)
}
