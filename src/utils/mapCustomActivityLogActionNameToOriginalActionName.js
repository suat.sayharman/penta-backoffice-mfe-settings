export const activityLogActions = {
  'Edited Business Address': {action: 'business:update', bodyParameter: 'address'},
  'Edited Business VAT Number': {action: 'business:update', bodyParameter: 'vat_number'},
  'Edited Business Purpose': {action: 'business:update', bodyParameter: 'business_purpose'},
  'Edited Business CRS Company Type': {action: 'business:update', bodyParameter: 'crs_company_type'},
  'Edited Business Ticket Status': {action: 'business:update', bodyParameter: 'ticket_status'},
  'Created churn reason and comment': {action: 'account_closure:create'},
  'Viewed churn reason and comment': {action: 'account_closure:read'},
  'Edited churn reason and comment': {action: 'account_closure:update'},
  'Deleted churn reason and comment': {action: 'account_closure:delete'},
  'Edited User Email': {action: 'person:update', bodyParameter: 'email'},
  'Edited User Address': {action: 'person:update', bodyParameter: 'address'},
  'Edited User Details': {action: 'person:update'},
  'Turned On Maintenance Page': {action: 'maintenance:create', bodyParameter: 'status', bodyParameterValue: true},
  'Turned Off Maintenance Page': {action: 'maintenance:create', bodyParameter: 'status', bodyParameterValue: false},
  'Activated User': {action: 'is_user_active:update', bodyParameter: 'is_active', bodyParameterValue: true},
  'Deactivated User': {action: 'is_user_active:update', bodyParameter: 'is_active', bodyParameterValue: false},
  'Downloaded Mandate PDF': {action: 'mandate:download'},
  'Viewed Maintenance Page': {action: 'maintenance:read'},
  'Viewed list of all accounts': {action: 'accounts:list'},
  'Searched the activity log': {action: 'activity_log:search'},
  'Deleted announcement': {action: 'announcement:delete'},
  'Viewed announcement': {action: 'announcement:read'},
  'Edited announcement': {action: 'announcement:write'},
  'Approved a change request': {action: 'approval:confirm'},
  'Requested an approval for a change': {action: 'approval:request'},
  'Added new authorized person (approvers)': {action: 'authorized_users:create'},
  'Logged out': {action: "backoffice_user:logout"},
  'Viewed balance': {action: 'balance:read'},
  'Edited the person as UBO': {action: 'beneficial_owners:create'},
  'Deleted a business': {action: 'business:delete'},
  'Viewed the businesses details': {action: 'business:read'},
  'Searched businesses': {action: 'business:search'},
  'Synced businesses details': {action: 'business:synchronize'},
  'Uploaded direct debit credit report': {action: 'business_credit_report:create'},
  'Downloaded documents': {action: 'business_documents:download'},
  'Viewed business permissions and roles': {action: 'business_permissions:list'},
  'Created direct debit profile': {action: 'direct_debit_profile:create'},
  'Viewed direct debit profile': {action: 'direct_debit_profile:read'},
  'Updated approval change request': {action: 'cached_change:update'},
  'Viewed cards ': {action: 'cards:list'},
  'Viewed cards details': {action: 'cards:read'},
  'Viewed compliance questions': {action: 'compliance_question:read'},
  'viewed list of compliance questions': {action: 'compliance_questions:list'},
  'Viewed subscription page': {action: 'costs_breakdown:read'},
  'Updated credits, viewed subscription page': {action: 'credits:update'},
  'Viewed collection limit for the direct debit profile': {action: 'direct_debit_profile_collection_limits:read'},
  'Viewed files': {action: 'files:list'},
  'Uploaded a file': {action: 'files:upload'},
  'Viewed free trial period': {action: 'free_trial:list'},
  'Edited free trial period': {action: 'free_trial:update'},
  'Marked an identification as Ready': {action: 'identification:mark_as_ready'},
  'Requested a new identification': {action: 'identification:request'},
  'Initiated the identification process for the Business': {action: 'identification_process:initialize'},
  'Viewed the identification process details': {action: 'identification_process:read'},
  'Viewed list of all identification': {action: 'identifications:list'},
  'Viewed identification details': {action: 'identifications:read'},
  'Adds person as legal representative': {action: 'legal_representatives:create'},
  'Viewed login attempts page': {action: 'login_attempts:read'},
  'Listed missing onboarding documents': {action: 'missing_documents:list'},
  'Created new email': {action: 'new_user_email:create'},
  'Viewed onboarding request details': {action: 'onboarding_request:read'},
  'Rejected onboarding request': {action: 'onboarding_request:reject'},
  'Requested password change': {action: 'password_change_request:create'},
  'Viewed person details': {action: 'person:read'},
  'Synced person details': {action: 'person:synchronize'},
  'Viewed list of all persons': {action: 'persons:list'},
  'canceled downgrade of the plan': {action: 'pricing_plan:cancel_downgrade'},
  'Edited the pricing plan': {action: 'Pricing_plan:update'},
  'Created a redis task': {action: 'redis_task:create'},
  'Viewed list of all reservations': {action: 'reservations:list'},
  'Viewed list of all scheduled orders': {action: 'scheduled_orders:list'},
  'Viewed seizures': {action: 'seizures:read'},
  'Created a tax identification record': {action: 'tax_identifications:create'},
  'Viewed list all tax identifications for a Business': {action: 'tax_identifications:list'},
  'Viewed accepted terms and policies': {action: 'terms_and_policies:list'},
  'Viewed list of transactions': {action: 'transactions:list'},
  'Viewed transactions': {action: 'transactions:read'},
  'Created trust pilot link': {action: 'trustpilot_review_link:create'},
  'Viewed usage breakdown': {action: 'usage_breakdown:read'},
  'Created a user': {action: 'user:create'},
  'Deleted a user': {action: 'user:delete'},
  'Edited permissions': {action: 'user_permissions:update'},
  'Viewed list of mobile numbers': {action: 'verified_mobile_number:list'},
  'Viewed list of business identifications': {action: 'business_identification:read'},
  'Edited a Direct Debit profile': {action: 'direct_debit_profile:update'},
  'Viewed Direct Debit submission details': {action: 'find_direct_debit_submission_details:read'},
  'Created a person': {action: 'person:create'},
  'List of all user permissions': {action: 'user_permissions:list'},
  'Edited a business state': {action: 'business_state:update'},
  'Edited a user email': {action: 'user_email:update'},
  'Created a business role': {action: 'business_role:insert'},
  'Viewed business role details': {action: 'business_role:get'},
  'Delete business role': {action: 'business_role:delete'},
  'Generated mandate': {action: 'mandate:generate'},
  'Viewed the mandate details': {action: 'mandate:read'},
  'Viewed list of all mandates': {action: 'mandate:list'},
  'Downloaded mandate': {action: 'mandate:download'},
  'Uploaded document': {action: 'document:upload'},
  'Downloaded document': {action: 'document_download'},
  'Viewed list of all promo codes': {action: 'promo_code:list'},
  'Created a promo code': {action: 'promo_code:create'},
  'Edited a promo code': {action: 'promo_code:update'},
  'Viewed referral details': {action: 'referral:view'},
  'Viewed list of all referrals': {action: 'referral:list'},
  'Viewed list of all roles': {action: 'user_role:list'},
  'Viewed list of all my roles': {action: 'user_role:list_my_roles'},
  'Created a user role': {action: 'user_role:create'},
  'Assigned a user role': {action: 'user_role:assign'},
  'Viewed list of all permissions': {action: 'role_permissions:list'},
  'Viewed a VAT': {action: 'vat:view'},
  'Created a VAT': {action: 'vat:create'},
  'Deleted a VAT': {action: 'vat:delete'},
  'Viewed a list of all credit notes': {action: 'business_credit_note:list'},
  'Downloaded a credit note pdf': {action: 'business_credit_note:download_pdf'},
  'Viewed a list of signed credit notes': {action: 'business_credit_note:list_signed'},
  'Created a business credit note': {action: 'business_credit_note:create'},
  'Canceled a credit note': {action: 'business_credit_note:canceled'},
  'Edited a credit note': {action: 'business_credit_note:update'},
  'Deleted a credit note': {action: 'business_credit_note:remove'},
  'Viewed an invoice details': {action: 'invoice:view'},
  'Viewed a signed invoice details': {action: 'signed_invoice:view'},
  'Viewed a business invoice details': {action: 'business_invoice:view'},
  'Canceled an invoice': {action: 'invoice:cancel'},
  'Created an invoice status': {action: 'invoice_status:create'},
}

export const mapCustomActivityLogActionNameToOriginalActionName = (actionName) => {
  return activityLogActions[actionName] === undefined
   ? {action: actionName}
   : activityLogActions[actionName]
}
