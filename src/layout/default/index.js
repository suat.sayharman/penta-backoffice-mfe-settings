import { connect } from 'react-redux'
import DefaultLayout from './views/DefaultLayout'
import AuthActions from '../../redux-modules/auth'

const mapStateToProps = (state) => ({
  userRole: state.auth.role
})

export default connect(mapStateToProps, AuthActions)(DefaultLayout)

