import React, { useCallback } from 'react'
import { Card, CardBody } from 'reactstrap'
import { Redirect, Route, Switch } from 'react-router-dom'
import { withLDConsumer } from 'launchdarkly-react-client-sdk'

// routes config
import routes from '../../../routes'
import { LaunchDarklyRoute } from '../../../components/feature-flag/launchdarkly-route'
import { useSelector } from 'react-redux'

function DefaultLayout(props) {
  const myPermissions = useSelector(state => state.permissions.myPermissions)

  const myPermissionsFetched = useSelector(
    state => state.permissions.myPermissionsFetched
  )

  const filterNavigation = useCallback(
    navigationItem =>
      navigationItem.requiredPermissions.every(permission =>
        myPermissions.includes(permission)
      ),
    [myPermissions]
  )

  const filteredRoutes = routes.filter(filterNavigation)

  return (
    <Switch>
      {filteredRoutes.map((route, idx) => {
        if (myPermissionsFetched && myPermissions.length === 0) {
          return (
            <Card className="mt-4">
              <CardBody>
                You don’t have any permissions assigned at the moment, please
                reach out to the Admin
              </CardBody>
            </Card>
          )
        }
        if (!route.component) {
          return null
        }
        if (route.launchDarklyKey) {
          return (
            <LaunchDarklyRoute
              key={idx}
              redirectToHome={true}
              requiredFlag={route.launchDarklyKey}
              path={route.path}
              render={routeProps => <route.component {...routeProps} />}
              exact={true}
            />
          )
        }
        return (
          <Route
            key={idx}
            path={route.path}
            exact={route.exact}
            name={route.name}
            render={routeProps => <route.component {...routeProps} />}
          />
        )
      })}
      {myPermissionsFetched && (
        <Redirect from="/" to={filteredRoutes[0].path} />
      )}
    </Switch>
  )
}

export default withLDConsumer()(DefaultLayout)
