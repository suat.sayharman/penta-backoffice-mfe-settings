import React, { Component } from 'react'
import { DropdownItem, DropdownMenu, DropdownToggle, Nav } from 'reactstrap'
import PropTypes from 'prop-types'

import {
  AppHeaderDropdown,
  AppNavbarBrand,
  AppSidebarToggler
} from '@coreui/react'
import logo from '../../../assets/img/brand/logo.svg'
import pentaIcon from '../../../assets/img/brand/penta-icon.png'
import { NotificationHeader } from '../../../components/notifications/notification-header/NotificationHeader'
const propTypes = {
  children: PropTypes.node
}

const defaultProps = {}

class DefaultHeader extends Component {
  render() {
    // eslint-disable-next-line
    const userEmail = sessionStorage.getItem('email') || 'User'
    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: 'Penta Logo' }}
          minimized={{
            src: pentaIcon,
            width: 30,
            height: 30,
            alt: 'Penta Logo'
          }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="ml-auto" navbar>
          <NotificationHeader></NotificationHeader>
          <AppHeaderDropdown direction="down" className="ml-4 mr-4">
            <DropdownToggle nav>
              <img
                src={'/assets/img/user.png'}
                className="img-avatar"
                alt={userEmail}
                title={userEmail}
              />
            </DropdownToggle>
            <DropdownMenu right style={{ right: 'auto' }}>
              <DropdownItem onClick={this.props.logout}>
                <i className="icon-logout" /> Logout
              </DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
      </React.Fragment>
    )
  }
}

DefaultHeader.propTypes = propTypes
DefaultHeader.defaultProps = defaultProps

export default DefaultHeader
