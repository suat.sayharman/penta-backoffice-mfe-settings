import React, { Component } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
// Styles
// CoreUI Icons Set
import '@coreui/icons/css/coreui-icons.min.css'
// Import Flag Icons Set
import 'flag-icon-css/css/flag-icon.min.css'
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css'
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css'
// Import Main styles for this application
import './scss/style.css'
// Containers
import DefaultLayout from './layout/default'
// Pages
import Login from './components/login'
import Notification from './components/common/Notification'
import { Helmet } from 'react-helmet'
import $ from 'jquery'
import { LaunchDarkly } from './LaunchDarkly'
import { withLDProvider } from 'launchdarkly-react-client-sdk'
import { connect } from 'react-redux'
import AuthActions from './redux-modules/auth'

const ldClientId = process.env.REACT_APP_LD_CLIENT_ID
const lDAnonymous = { key: 'anonymous', anonymous: true }

const authenticate = Page => {
  return function(props) {
    const authToken = sessionStorage.getItem('auth_token')
    if (authToken) {
      return <Page {...props} />
    }
    return <Redirect to="/login" />
  }
}

class App extends Component {
  componentDidMount() {
    $('body').click(() => {
      const toast = $('.toast-error')
      if (toast && toast.is(':visible')) {
        setTimeout(() => {
          toast.hide()
        }, 1000)
      }
    })
    this.props.checkAuthState()
  }

  render() {
    return (
      <div>
        <Helmet titleTemplate="%s | Penta Backoffice" />
        <Notification.Component />
        <Switch>
          <Route exact path="/login" name="Login Page" component={Login} />
          <Route path="/" name="Home" component={authenticate(DefaultLayout)} />
        </Switch>
        <LaunchDarkly />
      </div>
    )
  }
}

const mapDispatchToProps = {
  checkAuthState: AuthActions.checkAuthState
}

export default withLDProvider({
  clientSideID: ldClientId,
  user: lDAnonymous
})(connect(null, mapDispatchToProps)(App))
