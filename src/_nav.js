import { PERMISSIONS } from './constants/permissions'

export default {
  items: [
    {
      name: 'Home',
      url: '/',
      icon: 'icon-home',
      requiredPermissions: []
    },
    // {
    //   title: true,
    //   name: 'Links',
    //   wrapper: {
    //     // optional wrapper object
    //     element: '', // required valid HTML5 element tag
    //     attributes: {} // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
    //   },
    //   class: '', // optional class names space delimited list for title item ex: "text-center"
    //   requiredPermissions: []
    // },
    // {
    //   name: 'Businesses',
    //   url: '/businesses',
    //   icon: 'icon-people',
    //   requiredPermissions: [PERMISSIONS.business.read]
    // },
    // {
    //   name: 'Announcement',
    //   url: '/announcement',
    //   icon: 'icon-bell',
    //   requiredPermissions: [PERMISSIONS.announcements.read]
    // },
    // {
    //   name: 'Maintenance',
    //   url: '/maintenance',
    //   icon: 'icon-wrench',
    //   requiredPermissions: [PERMISSIONS.maintenance.read]
    // },
    // {
    //   name: 'Login Attempts',
    //   url: '/login_attempts',
    //   icon: 'icon-user',
    //   requiredPermissions: [PERMISSIONS.loginattempts.read]
    // },
    {
      name: 'Settings',
      url: '/settings',
      icon: 'icon-settings',
      launchDarklyKey: 'backofficeRbamEnabled',
      requiredPermissions: [PERMISSIONS.settings.read]
    }
    // {
    //   name: 'Finance',
    //   url: '/finance',
    //   icon: 'icon-pencil',
    //   requiredPermissions: [PERMISSIONS.finance.read]
    // },
    // {
    //   name: 'Promo Codes',
    //   url: '/promocodes',
    //   icon: 'icon-present',
    //   requiredPermissions: [PERMISSIONS.promocode.read]
    // },
    // {
    //   name: 'Activity log',
    //   url: '/activity_log',
    //   icon: 'icon-book-open',
    //   launchDarklyKey: 'activityLogsEnabled',
    //   requiredPermissions: [PERMISSIONS.activityLogs.read]
    // },
    // {
    //   name: 'Onboarding Funnel',
    //   url: '/funnel',
    //   icon: 'icon-present',
    //   requiredPermissions: [PERMISSIONS.onboardingfunnel.read]
    // },
    // {
    //   name: 'Notifications',
    //   url: '/Notifications',
    //   icon: 'icon-present',
    //   requiredPermissions: [PERMISSIONS.notifications.read]
    // }
  ]
}
