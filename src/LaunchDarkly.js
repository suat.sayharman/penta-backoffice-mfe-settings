import withLDConsumer from 'launchdarkly-react-client-sdk/lib/withLDConsumer'
import { PureComponent } from 'react'
import { connect } from 'react-redux'
import LDActions, { mkUser } from './redux-modules/launchdarkly'

class LaunchDarkly extends PureComponent {
  componentDidUpdate(prevProps) {
    const { setLaunchDarklyClient, isLoggedIn } = this.props
    const isLaunchDarklyReady = !prevProps.ldClient && setLaunchDarklyClient
    if (isLaunchDarklyReady || isLoggedIn !== prevProps.isLoggedIn) {
      if(this.props.ldClient){
        this.props.ldClient
        .waitForInitialization()
        .then(() => {
          setLaunchDarklyClient(this.props.ldClient)
          const userId = sessionStorage.getItem('user_id')
          if(userId){
            this.props.identifyLaunchdarklyUser(mkUser(userId))
          }
        })
      }
    }
  }

  render() {
    return null
  }
}

const mapDispatchToProps = {
  setLaunchDarklyClient: LDActions.setLaunchdarklyClient,
  identifyLaunchdarklyUser: LDActions.identifyLaunchdarklyUser
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.auth.isLoggedIn
  }
}

const enhancedComponent = withLDConsumer()(
  connect(mapStateToProps, mapDispatchToProps)(LaunchDarkly)
)

export { enhancedComponent as LaunchDarkly }
