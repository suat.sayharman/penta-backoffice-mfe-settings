import apisauce from 'apisauce'
import { push } from 'react-router-redux'
import qs from 'qs'
import moment from 'moment'
import { convertRequestParamsToQueryString } from '../utils/encode-request-params'
import { getApprovalMethod } from '../utils/environment-settings'
import { mapCustomActivityLogActionNameToOriginalActionName } from '../utils/mapCustomActivityLogActionNameToOriginalActionName'

let store = {}
export function setStore(st) {
  store = st
}

const create = () => {
  const api = createApi('/api/')
  const getUserId = () => {
    return sessionStorage.getItem('user_id')
  }

  const getPerson = (businessId, personId) => {
    return api.get(`businesses/${businessId}/persons/${personId}`)
  }

  function syncBusiness(businessId) {
    return api.post(`businesses/${businessId}/sync`)
  }

  const syncPersons = businessId =>
    api.post(`businesses/${businessId}/sync_persons`)

  const login = (email, password) => {
    return api.post('users/login', { email, password })
  }

  const getAnnouncements = () => {
    return api.get(
      'notification_defs?types=["announcement","announcement_app"]'
    )
  }

  const createAnnouncement = data => {
    return api.post('notification_defs', data)
  }

  const editAnnouncement = (announcementId, data) => {
    return api.patch(`notification_defs/${announcementId}`, data)
  }

  const deleteAnnouncement = (announcementId, data) => {
    return api.patch(`notification_defs/${announcementId}/delete`, data)
  }

  const logout = () => {
    return api.post(`users/${getUserId()}/logout`, {})
  }

  const getLoginAttemptsForUser = (userId, limit, offset) => {
    const queryString = qs.stringify({ id_user: userId, limit, offset })
    return api.get(`login_attempts?${queryString}`)
  }

  const defaultSearchObj = {
    userId: undefined,
    email: undefined,
    ip: undefined
  }
  const searchLoginAttempts = (
    limit,
    offset,
    { userId, email, ip } = defaultSearchObj
  ) => {
    const queryString = qs.stringify({
      limit,
      offset,
      id_user: userId,
      email,
      ip
    })
    return api.get(`login_attempts?${queryString}`)
  }

  const getUsersForBusiness = businessId => {
    return api.get(`businesses/${businessId}/users`)
  }

  const searchBusinesses = params => {
    const queryParams = convertRequestParamsToQueryString(params)
    const apiUrl = `backoffice/businesses/search${queryParams}`
    return api.get(apiUrl)
  }

  const getNotifications = params => {
    const queryParams = convertRequestParamsToQueryString(params)
    const apiUrl = `notifications/${queryParams}`
    return api.get(apiUrl)
  }

  const getCachedNotifications = () => {
    const apiUrl = `notifications/cached`
    return api.get(apiUrl)
  }

  const deleteNotification = id => {
    const apiUrl = `notifications/${id}/deleted`
    return api.post(apiUrl)
  }

  const markNotificationAsSeen = id => {
    const apiUrl = `notifications/${id}/seen`
    return api.post(apiUrl)
  }

  const deleteAllNotifications = () => {
    const apiUrl = `notifications/deleteall`
    return api.post(apiUrl)
  }

  const markAllNotificationsAsSeen = () => {
    const apiUrl = `notifications/readall`
    return api.post(apiUrl)
  }

  const searchUsers = (query, category, page) => {
    const params = []
    if (query) {
      params.push(`query=${encodeURIComponent(query)}`)
    }
    if (category) {
      params.push(`category=${encodeURIComponent(category)}`)
    }
    if (page) {
      params.push(`page=${encodeURIComponent(page)}`)
    }
    const queryParams = params.length > 0 ? `?${params.join('&')}` : ``
    const apiUrl = `backoffice/search${queryParams}&pageSize=20`
    return api.get(apiUrl)
  }

  const getBusiness = businessId => {
    return api.get(`businesses/${businessId}?details=true`)
  }

  const saveBusinessClosureReason = (businessId, data) => {
    return api.post(`account_closure/business/${businessId}`, data)
  }

  const getBusinessClosureReason = businessId => {
    return api.get(`account_closure/business/${businessId}`)
  }

  const updateBusinessClosureReason = (businessId, data) => {
    return api.patch(`account_closure/business/${businessId}`, data)
  }

  const deleteBusinessClosureReason = (businessId, data) => {
    return api.delete(`account_closure/business/${businessId}`)
  }

  const getIdentification = businessId => {
    return api.get(`businesses/${businessId}/identification`)
  }

  const getBusinessIdentificationsByBusinessId = businessId => {
    return api.get(
      `businesses/${businessId}/identifications?recordsPerPage=100&pageNumber=0`
    )
  }

  const getBusinessIdentificationById = (businessId, identificationId) => {
    return api.get(
      `businesses/${businessId}/identifications/${identificationId}`
    )
  }

  const getPersonIdentifications = personId => {
    return api.get(`persons/${personId}/identifications`)
  }

  const getPersonIdentificationAttempts = identificationId => {
    return api.get(`person-identifications/${identificationId}/attempts`)
  }

  const createPersonIdentification = (businessId, personId) => {
    return api.post(
      `businesses/${businessId}/persons/${personId}/identification`
    )
  }

  const setAsLr = (businessId, id_person) => {
    return api.post(`businesses/${businessId}/legal_representatives`, {
      id_person
    })
  }

  const setAsUbo = (businessId, id_person, voting_share) => {
    return api.post(`businesses/${businessId}/beneficial_owners`, {
      id_person,
      voting_share
    })
  }

  const getPersons = businessId => {
    return api.get(`businesses/${businessId}/persons`)
  }

  const getCards = businessId => {
    return api.get(`businesses/${businessId}/cards`)
  }

  const getCardLimit = (businessId, accountId, cardId) => {
    return api.get(
      `businesses/${businessId}/accounts/${accountId}/cards/${cardId}/limits`
    )
  }

  const updateBusiness = (businessId, data) =>
    api.patch(`businesses/${businessId}`, data)

  const updateChange = (changeId, data) =>
    api.patch(`changes/${changeId}`, data)

  const newApprovalChangeRequest = (changeId, personId, businessId) =>
    api.post(`changes/${changeId}/approval`, {
      approval_method: getApprovalMethod(),
      id_person: personId,
      id_business: businessId
    })

  const confirmApprovalChangeRequest = (changeId, tan, personId, businessId) =>
    api.post(`changes/${changeId}/confirmation`, {
      tan,
      id_person: personId,
      id_business: businessId
    })

  const getAccountsForBusiness = businessId =>
    api.get(`businesses/${businessId}/accounts`)

  const updateUserPermissions = (businessId, userId, data) =>
    api.patch(`businesses/${businessId}/users/${userId}/permissions`, data)

  const getUserPermissions = (businessId, userId) =>
    api.get(`businesses/${businessId}/users/${userId}/permissions`)

  const getAuthorizedPersons = (idBusiness, idAccount) =>
    api.get(`businesses/${idBusiness}/accounts/${idAccount}/authorized_users`)

  const authorizeUser = (idBusiness, idAccount, idUser) =>
    api.post(
      `businesses/${idBusiness}/accounts/${idAccount}/authorized_users`,
      { id_user: idUser }
    )

  const invitePerson = (businessId, personId, permissions) =>
    api.post(`businesses/${businessId}/persons/${personId}/user`, permissions)

  const getAccountData = (idBusiness, idAccount) =>
    api.get(`businesses/${idBusiness}/accounts/${idAccount}`)

  const businessReadyForSolaris = businessId =>
    api.post(`businesses/${businessId}/identification/mark_as_ready`, {})

  const getBusinessFiles = businessId =>
    api.get(`businesses/${businessId}/files`)

  const getTaxInfo = businessId =>
    api.get(`businesses/${businessId}/tax_identifications`)

  const createTaxRecord = (businessId, data) =>
    api.post(`businesses/${businessId}/tax_identifications`, data)

  const uploadBusinessDoc = (businessId, docType, fileData) => {
    let formData = new FormData()
    formData.append('file', fileData)
    return api.post(
      `documents/businesses/${businessId}/upload_file/${docType}`,
      formData,
      {
        headers: { 'content-type': 'multipart/form-data' }
      }
    )
  }

  const deleteBusinessData = businessId => {
    return api.delete(`businesses/${businessId}`)
  }

  const restartBusinessIdentification = businessId => {
    return api.post(`businesses/${businessId}/identification`, { flow: 'b' })
  }

  const savePerson = (businessId, data) =>
    api.post(`businesses/${businessId}/persons`, data)

  const patchPerson = (businessId, personId, data) =>
    api.patch(`businesses/${businessId}/persons/${personId}`, data)

  const getVerifiedNumber = (businessId, personId) =>
    api.get(
      `businesses/${businessId}/persons/${personId}/verified_mobile_number`
    )

  const syncPersonMobileNumber = personId =>
    api.post(`persons/${personId}/mobile_number/sync`)

  const getMaintenanceStatus = () => {
    return api.get('maintenance')
  }

  const toggleMaintenanceStatus = newStatus => {
    return api.patch('maintenance', { status: newStatus })
  }

  const changePricingPlan = (businessId, data) =>
    api.post(`backoffice/${businessId}/pricing_plan`, data)

  const getCostsBreakdown = (businessId, month = null, year = null) => {
    let request = `businesses/${businessId}/costs_breakdown`

    if (month && year) {
      request = request + `?month=${month}&year=${year}`
    }

    return api.get(request)
  }

  const getCurrencyCloudCostsBreakdown = (
    businessId,
    month = null,
    year = null
  ) => {
    let request = `currency-cloud/costs/${businessId}`

    if (month && year) {
      request = request + `?month=${month}&year=${year}`
    }

    return api.get(request)
  }

  const getCreditNotes = businessId =>
    api.get(`credit-notes/business/${businessId}`)

  const getUsageBreakdown = (businessId, month = null, year = null) => {
    let request = `businesses/${businessId}/usage_breakdown`

    if (month && year) {
      request = request + `?month=${month}&year=${year}`
    }

    return api.get(request)
  }

  const cancelDowngrade = businessId =>
    api.post(`businesses/${businessId}/pricing_plan/cancel_downgrade`)

  const topUpCredit = (businessId, credit_to_add, reason) =>
    api.post(`credits/business/${businessId}/topup_credit`, {
      credit_to_add,
      reason
    })

  const deleteUser = userId => api.delete(`users/${userId}`)

  const updateUserActiveState = (businessId, userId, isActive) =>
    api.patch(`businesses/${businessId}/users/${userId}/isactive`, {
      is_active: isActive
    })

  const sendActivationEmail = (businessId, userId) =>
    api.post(`businesses/${businessId}/users/${userId}/new_user_email`)

  const getTransactions = (
    businessId,
    accountId,
    filter,
    pageNum = 1,
    size = 50
  ) =>
    api.get(
      `businesses/${businessId}/accounts/${accountId}/transactions?page[number]=${pageNum}&page[size]=${size}`,
      filter
    )

  const getTransaction = (businessId, accountId, transactionId) =>
    api.get(
      `businesses/${businessId}/accounts/${accountId}/transactions/${transactionId}`
    )

  const getScheduledOrders = (businessId, accountId, params) =>
    api.get(
      `businesses/${businessId}/accounts/${accountId}/scheduled_orders`,
      params
    )

  const getReservations = (businessId, accountId, params) =>
    api.get(
      `businesses/${businessId}/accounts/${accountId}/reservations`,
      params
    )

  const getBalanceData = (businessId, accountId) =>
    api.get(`businesses/${businessId}/accounts/${accountId}/balance`)

  const getCategories = businessId =>
    api.get(`businesses/${businessId}/categories`)

  const getSeizuresData = businessId =>
    api.get(`businesses/${businessId}/seizures`)

  const getInvoicesList = (businessId, withMissingPdf) =>
    api.get(
      `invoices/business/${businessId}${
        withMissingPdf ? '?with_missing_pdf=true' : ''
      }`
    )

  const setAccountClosureDate = (businessId, accountId, closureDate) =>
    api.post(`businesses/${businessId}/accounts/${accountId}/account_closure`, {
      closure_date: closureDate
    })

  const getMandateData = businessId =>
    api.get(`invoices/mandates/${businessId}`)

  const getComplianceQuestions = businessId =>
    api.get(`businesses/${businessId}/compliance-questions`)

  const getMissingDocuments = (businessId, status) =>
    api.get(`backoffice/${businessId}/missing-documents`, { status })

  const downloadDocument = (businessId, documentId) =>
    api.get(
      `backoffice/${businessId}/documents/${documentId}/download`,
      undefined,
      { responseType: 'blob' }
    )

  const patchFreeTrial = (businessId, endDate) =>
    api.patch(`free_trials/business/${businessId}`, { end_date: endDate })

  const getTermsAndPolicies = businessId =>
    api.get(`businesses/${businessId}/terms_and_policies`)

  const getVat = businessId => api.get(`vat/business/${businessId}`)

  const createVat = (businessId, vatData) =>
    api.post(`vat/business/${businessId}`, { ...vatData })

  const deleteVatServicePeriod = id => api.delete(`vat/${id}`)

  const cancelInvoice = (id, reason) =>
    api.post(`invoices/${id}/cancel`, { reason })

  const cancelCreditNote = (id, reason) =>
    api.post(`credit-notes/${id}/cancel`, { reason })

  const getRemainingCredits = businessId =>
    api.get(`credits/business/${businessId}/remaining_credits`)

  const generateTrustpilotLink = userId =>
    api.get(`trustpilot/review/${userId}`)

  const getDirectDebitProfile = businessId =>
    api.get(`direct_debits/businesses/${businessId}/direct-debit-profile`)

  const getCollectionLimit = businessId =>
    api.get(
      `direct_debits/businesses/${businessId}/direct-debit-profile/limits`
    )

  const getDirectDebitOnboardingRequest = businessId =>
    api.get(
      `direct_debits/businesses/${businessId}/direct-debit-onboarding-requests`
    )

  const rejectDirectDebitOnboardingRequest = businessId =>
    api.delete(
      `direct_debits/businesses/${businessId}/direct-debit-onboarding-requests`
    )

  const getDirectDebitsCollection = (businessId, pageNum = 1, size = 20) =>
    api.get(
      `direct_debits/businesses/${businessId}/direct-debits?page[number]=${pageNum}&page[size]=${size}`
    )

  const updateCreditorIdentifer = (businessId, creditorIdentifier) =>
    api.patch(`direct_debits/businesses/${businessId}/direct-debit-profile`, {
      creditor_identifier: creditorIdentifier
    })

  const uploadDirectDebitCreditReport = (businessId, creditReportData) => {
    let formData = new FormData()
    formData.append('file', creditReportData.file)
    formData.append('report', JSON.stringify(creditReportData.report))
    return api.post(
      `direct_debits/businesses/${businessId}/credit-reports`,
      formData,
      {
        headers: { 'content-type': 'multipart/form-data' }
      }
    )
  }

  const createDirectDebitProfile = (businessId, { creditReportId, tcDate }) =>
    api.post(`direct_debits/businesses/${businessId}/direct-debit-profile`, {
      credit_report_id: creditReportId,
      terms_conditions_signed_at: tcDate
    })

  const getPentaPayments = (
    filter,
    direction = 'income',
    page = 1,
    size = 20
  ) => {
    return api.get(
      `/internal/transactions?events=["${direction}"]&page[number]=${page}&page[size]=${size}&free_text=${filter}`
    )
  }

  const resetUserPassword = body => api.post('users/update_pass_request', body)

  const updateInvoice = payload => api.patch(`invoices/${payload.id}`, payload)

  const createCreditNote = (businessId, payload) =>
    api.post(`credit-notes/business/${businessId}`, payload)

  const updateCreditNote = (creditNoteId, payload) =>
    api.patch(`credit-notes/${creditNoteId}`, payload)

  const deleteCreditNote = id => api.delete(`credit-notes/${id}`)

  const downloadBillDocument = url =>
    api.get(url, undefined, { responseType: 'blob' })

  const downloadMandate = id =>
    api.get(`/invoices/mandates/${id}/file`, undefined, {
      responseType: 'blob'
    })

  const downloadConversionsStatement = id => {
    return api.get(`currency-cloud/past-conversions/${id}`, undefined, {
      responseType: 'blob'
    })
  }

  const getBackofficeUsers = () => api.get('users/backoffice')

  const getMyRole = () => api.get('internal/userroles/my')

  const getAllRoles = () => api.get('internal/userroles')

  const createRole = (role, permissions) =>
    api.post('internal/userroles', {
      role,
      permissions
    })

  const assignUserToRole = (userId, role) =>
    api.put('internal/userroles/assign_role', {
      userId,
      role
    })

  const updateRoleName = (roleId, newName) =>
    api.patch(`internal/userroles/${roleId}`, {
      name: newName
    })

  const deleteRole = roleId => api.delete(`internal/userroles/${roleId}`)

  const getRolePermissions = role =>
    api.get(`internal/permissions/userrole?role=${encodeURIComponent(role)}`)

  const setRolePermissions = (role, add, remove) =>
    api.patch(`internal/permissions/${role.id}`, {
      role: role.role_name,
      add,
      remove
    })

  const getMyPermissions = () => api.get('internal/permissions')

  const updateBusinessState = (businessId, state) => {
    return api.put(`internal/businesses/${businessId}/state/`, { state })
  }

  const updateUserEmail = (userId, email) => {
    return api.put(`/internal/users/${userId}/email/`, { email })
  }

  const updateUserRealm = (userId, realm) => {
    return api.put(`/internal/users/${userId}/realm/`, { realm })
  }

  const updateUserIsAdmin = (userId, is_admin) => {
    return api.put(`/internal/users/${userId}/admin/`, { is_admin })
  }

  const updateUserIsActive = (userId, is_active) => {
    return api.put(`/internal/users/${userId}/active/`, { is_active })
  }

  const getBusinessRoles = businessId => {
    return api.get(`/internal/businesses/${businessId}/businessrole/`)
  }

  const deleteBusinessRole = (businessId, personId) => {
    return api.delete(
      `/internal/businesses/${businessId}/${personId}/businessrole/`
    )
  }

  const addBusinessRoles = (
    businessId,
    personId,
    legalRepresentativeSolaris,
    beneficialOwnerSolaris,
    personSolaris
  ) => {
    return api.post(`/internal/businesses/${businessId}/businessrole/`, {
      id_person: personId,
      id_legal_representative_solaris: legalRepresentativeSolaris || null,
      id_beneficial_owner_solaris: beneficialOwnerSolaris || null,
      id_person_solaris: personSolaris || null
    })
  }

  const getActivityLogs = (filters, page = 1, perPage = 20) => {
    let url = `activity_logs?page=${page}&perPage=${perPage}`
    Object.keys(filters).forEach(key => {
      switch (key) {
        case 'userId':
          url += `&user_id=${filters[key]}`
          break
        case 'businessId':
          url += `&business_id=${filters[key]}`
          break
        case 'action':
          const actionData = mapCustomActivityLogActionNameToOriginalActionName(
            filters.action
          )
          url += `&action=${actionData.action}`
          if (actionData.bodyParameter !== undefined) {
            url += `&body_parameter=${actionData.bodyParameter}`
          }
          if (actionData.bodyParameterValue !== undefined) {
            url += `&body_parameter_value=${actionData.bodyParameterValue}`
          }
          break
        case 'dateFrom':
          let from
          if (filters['timeFrom'] !== undefined) {
            from = moment(`${filters[key]} ${filters['timeFrom']}`)
          } else {
            from = moment(filters[key]).startOf('day')
          }
          url += `&from=${from.utc().format()}`
          break
        case 'dateTo':
          let to
          if (filters['timeTo'] !== undefined) {
            to = moment(`${filters[key]} ${filters['timeTo']}`)
          } else {
            to = moment(filters[key]).endOf('day')
          }
          url += `&to=${to.utc().format()}`
          break
        default:
          break
      }
    })
    return api.get(url)
  }

  const getBusinessAssignmentStatus = businessId => {
    return api.get(`/backoffice/businesses/${businessId}/bkyc`)
  }

  const getBusinessAssignmentsByUserId = userId => {
    return api.get(`/backoffice/users/${userId}/bkyc`)
  }

  const getBusinessAssignmentsByStatus = status => {
    return api.get(`/backoffice/businesses/bkyc/${status}`)
  }

  const updateBusinessAssignmentStatus = (businessId, status) => {
    return api.post(`/backoffice/businesses/${businessId}/bkyc/status`, {
      status
    })
  }

  const updateBusinessAssignment = (businessId, userId, status) => {
    return api.post(
      `/backoffice/businesses/${businessId}/bkyc/${userId}/assignment`,
      {
        status
      }
    )
  }

  return {
    updateBusinessState,
    updateUserEmail,
    updateUserRealm,
    updateUserIsAdmin,
    updateUserIsActive,
    getBusinessRoles,
    deleteBusinessRole,
    addBusinessRoles,
    getMaintenanceStatus,
    toggleMaintenanceStatus,
    getAnnouncements,
    createAnnouncement,
    editAnnouncement,
    deleteAnnouncement,
    login,
    logout,
    getLoginAttemptsForUser,
    searchLoginAttempts,
    getCards,
    getCardLimit,
    getPersons,
    getIdentification,
    getPersonIdentifications,
    getPersonIdentificationAttempts,
    createPersonIdentification,
    getBusiness,
    saveBusinessClosureReason,
    getBusinessClosureReason,
    updateBusinessClosureReason,
    deleteBusinessClosureReason,
    searchUsers,
    getPerson,
    syncBusiness,
    syncPersons,
    getUsersForBusiness,
    newApprovalChangeRequest,
    confirmApprovalChangeRequest,
    getAccountsForBusiness,
    updateUserPermissions,
    getUserPermissions,
    getAuthorizedPersons,
    authorizeUser,
    invitePerson,
    getAccountData,
    updateBusiness,
    updateChange,
    businessReadyForSolaris,
    getBusinessFiles,
    getTaxInfo,
    createTaxRecord,
    setAsLr,
    setAsUbo,
    uploadBusinessDoc,
    savePerson,
    patchPerson,
    getVerifiedNumber,
    syncPersonMobileNumber,
    deleteBusinessData,
    restartBusinessIdentification,
    changePricingPlan,
    getCostsBreakdown,
    getCurrencyCloudCostsBreakdown,
    getCreditNotes,
    getUsageBreakdown,
    cancelDowngrade,
    topUpCredit,
    deleteUser,
    updateUserActiveState,
    sendActivationEmail,
    getTransactions,
    getTransaction,
    getScheduledOrders,
    getReservations,
    getBalanceData,
    getCategories,
    getSeizuresData,
    getInvoicesList,
    getMandateData,
    setAccountClosureDate,
    getComplianceQuestions,
    getMissingDocuments,
    downloadDocument,
    patchFreeTrial,
    getTermsAndPolicies,
    getVat,
    createVat,
    deleteVatServicePeriod,
    cancelInvoice,
    cancelCreditNote,
    getRemainingCredits,
    generateTrustpilotLink,
    resetUserPassword,
    getDirectDebitProfile,
    getCollectionLimit,
    updateCreditorIdentifer,
    uploadDirectDebitCreditReport,
    createDirectDebitProfile,
    getDirectDebitOnboardingRequest,
    rejectDirectDebitOnboardingRequest,
    getDirectDebitsCollection,
    getPentaPayments,
    axiosInstance: api.axiosInstance,
    apisauceInstance: api,
    updateInvoice,
    createCreditNote,
    updateCreditNote,
    deleteCreditNote,
    downloadBillDocument,
    downloadMandate,
    downloadConversionsStatement,
    getBusinessIdentificationsByBusinessId,
    getBusinessIdentificationById,
    getBackofficeUsers,
    getMyRole,
    getAllRoles,
    createRole,
    assignUserToRole,
    updateRoleName,
    deleteRole,
    getRolePermissions,
    getMyPermissions,
    setRolePermissions,
    getActivityLogs,
    searchBusinesses,
    getNotifications,
    deleteNotification,
    deleteAllNotifications,
    markAllNotificationsAsSeen,
    markNotificationAsSeen,
    getCachedNotifications,
    getBusinessAssignmentStatus,
    updateBusinessAssignmentStatus,
    updateBusinessAssignment,
    getBusinessAssignmentsByUserId,
    getBusinessAssignmentsByStatus
  }
}

export default { create }

function createApi(baseUrl) {
  const api = apisauce.create({
    baseURL: baseUrl,
    timeout: 300000
  })

  api.addRequestTransform(request => {
    request.headers['penta-token'] = sessionStorage.getItem('auth_token')
    request.headers['Cache-Control'] = 'no-cache'
  })

  api.addResponseTransform(response => {
    if (response.status === 401) {
      sessionStorage.removeItem('auth_token')
      sessionStorage.removeItem('user_id')
      sessionStorage.removeItem('email')
      store.dispatch(push('/login'))
    }
  })

  return api
}
