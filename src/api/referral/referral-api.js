import { create } from 'apisauce'
import { getQuery } from '../../components/promo-codes/helpers'

export const sortBy = {
  ASC: 'ASC',
  DESC: 'DESC'
}
export const promoCodeStatus = {
  rewarded: 'rewarded',
  registered: 'registered'
}
class ReferralApi {
  constructor() {
    this.api = create({
      baseURL: '/api/referral',
      timeout: 300000
    })

    this.api.addRequestTransform(request => {
      request.headers['penta-token'] = sessionStorage.getItem('auth_token')
      request.headers['Cache-Control'] = 'no-cache'
    })
  }

  getPromoCodes = () => {
    return this.api.get(`/promocode`)
  }

  createPromoCode = promoCode => {
    return this.api.post('/promocode/add', promoCode)
  }

  updatePromoCode = (id, promoCode) => {
    return this.api.put(`/promocode/${id}`, promoCode)
  }

  getPromoCodeBusinessList = queryData => {
    const query = getQuery(queryData)
    return this.api.get(`/promocodebusiness${query}`)
  }

  getReferralSummary = () => {
    return this.api.get('/referralreports/summary/')
  }

  getReferralReports = queryData => {
    const query = getQuery(queryData)
    return this.api.get(`/referralreports/all${query}`)
  }
}

export default new ReferralApi()
