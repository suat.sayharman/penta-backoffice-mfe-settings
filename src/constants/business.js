export const QUERY_KEY = 'query_sf'
export const CATEGORY_KEY = 'category_sf'

export const CATEGORIES = [
  {
    value: 'EMAIL',
    label: 'Email'
  },
  {
    value: 'BUSINESS_NAME',
    label: 'Company name'
  },
  {
    value: 'BUSINESS_ID',
    label: 'Business Id'
  },
  {
    value: 'PERSON_ID',
    label: 'Person Id'
  },
  {
    value: 'IBAN',
    label: 'IBAN'
  },
  {
    value: 'FIRST_NAME',
    label: 'Name'
  },
  {
    value: 'LAST_NAME',
    label: 'Lastname'
  },
  {
    value: 'FULL_NAME',
    label: 'Name & Lastname'
  }
]
