export const PERMISSIONS = {
  business: {
    read: 'business:read',
    write: 'business:write',
    delete: 'business:delete'
  },
  announcements: {
    read: 'announcement:read',
    write: 'announcement:write',
    delete: 'announcement:delete'
  },
  maintenance: {
    read: 'maintenance:read',
    write: 'maintenance:write'
  },
  loginattempts: {
    read: 'loginattempts:read'
  },
  settings: {
    read: 'settings:read',
    write: 'settings:write',
    delete: 'settings:delete'
  },
  finance: {
    read: 'finance:read',
    write: 'finance:write',
    delete: 'finance:delete'
  },
  promocode: {
    read: 'promocode:read',
    write: 'promocode:write'
  },
  developer: {
    write: 'developer:write'
  },
  notifications: {
    read: 'notifications:read',
    write: 'notifications:write',
    delete: 'notifications:delete'
  },
  activityLogs: {
    read: 'activitylogs:read'
  },
  onboardingfunnel: {
    read: 'onboardingfunnel:read',
    write: 'onboardingfunnel:write',
    delete: 'onboardingfunnel:delete'
  }
}
