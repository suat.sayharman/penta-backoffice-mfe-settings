import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { reducer as announcement } from '../redux-modules/announcement'
import { reducer as promoCodes } from '../redux-modules/promo-codes'
import { reducer as referralReports } from '../redux-modules/referral-reports'
import { reducer as auth } from '../redux-modules/auth'
import { reducer as businesses } from '../redux-modules/businesses'
import { reducer as person } from '../redux-modules/person'
import { reducer as maintenance } from '../redux-modules/maintenance'
import { reducer as confirmationDialog } from '../redux-modules/confirmation-dialog'
import { reducer as pricing } from '../redux-modules/pricing'
import { reducer as transactions } from '../redux-modules/transactions'
import { reducer as vat } from '../redux-modules/vat'
import { routerReducer } from 'react-router-redux'
import { launchDarklyReducer } from '../redux-modules/launchdarkly'
import { reducer as loginAttempts } from '../redux-modules/loginAttempts'
import { reducer as directDebit } from '../redux-modules/direct-debits'
import { reducer as cards } from '../redux-modules/cards'
import { reducer as persons } from '../redux-modules/persons'
import { reducer as users } from '../redux-modules/users'
import { reducer as businessIdentifications } from '../redux-modules/business-identifications'
import { reducer as permissions } from '../redux-modules/permissions'
import { reducer as userroles } from '../redux-modules/userroles'
import { reducer as activityLogs } from '../redux-modules/activity-logs'

const appReducer = combineReducers({
  announcement,
  promoCodes,
  referralReports,
  auth,
  businesses,
  person,
  maintenance,
  confirmationDialog,
  pricing,
  transactions,
  vat,
  form: formReducer,
  router: routerReducer,
  launchdarkly: launchDarklyReducer,
  loginAttempts,
  directDebit,
  cards,
  persons,
  users,
  businessIdentifications,
  permissions,
  userroles,
  activityLogs
})

export default appReducer
