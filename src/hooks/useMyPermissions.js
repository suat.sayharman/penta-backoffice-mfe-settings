import { useSelector } from 'react-redux'

export function useMyPermissions() {
  return useSelector(state => state.permissions.myPermissions)
}
