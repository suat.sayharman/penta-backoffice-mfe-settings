import { call, put, select } from 'redux-saga/effects'

import LD, { isReady, mkUser } from '../redux-modules/launchdarkly'

export const ldClientSelector = state => state.launchdarkly

export function* setLaunchDarklyClientSaga() {
  const isLoggedIn = sessionStorage.getItem('auth_token')
  if (isLoggedIn) {
    const userId = sessionStorage.getItem('user_id')
    yield put(LD.identifyLaunchdarklyUser(mkUser(userId)))
  }
}

export function* identifyLaunchDarklyUserSaga(action) {
  const ldState = yield select(ldClientSelector)
  yield isReady(ldState, ldClient => call(ldClient.identify, action.payload))
}
