import { all, call, put } from 'redux-saga/effects'
import PersonsActions from '../redux-modules/persons'
import UsersActions from '../redux-modules/users'
import {
  showErrorNotification,
  showSuccessNotification
} from '../utils/notification-handler'
import BusinessActions from '../redux-modules/businesses'
import { onError } from './businesses'
import { showErrorMessage } from '../utils/notification-handler'

export function* getBusinessPersons(api, { businessId }) {
  const responses = yield all([
    call(api.getIdentification, businessId),
    call(api.getPersons, businessId)
  ])

  const identificationResponse = responses[0]
  const personsResponse = responses[1]

  const persons = personsResponse.data

  if (personsResponse.ok) {
    if (identificationResponse.ok && identificationResponse.data) {
      persons.forEach(person => {
        const personFromIdentification = identificationResponse.data.persons.find(
          item => item.id === person.id_person
        )
        if (personFromIdentification) {
          person.status = personFromIdentification.status
        }
      })
    }

    yield put(PersonsActions.onPersonsReceived(persons))
  }
}

export function* invitePersonSaga(api, { businessId, personId, permissions }) {
  const response = yield call(
    api.invitePerson,
    businessId,
    personId,
    permissions
  )
  if (response.ok) {
    showSuccessNotification('User invitation successfully sent')
    yield put(PersonsActions.invitePersonSuccess())
    yield put(PersonsActions.getPersons(businessId))
    yield put(UsersActions.getBusinessUsers(businessId))
  } else {
    showErrorNotification(response)
    yield put(PersonsActions.invitePersonFailure())
  }
}

export function* requestApprovalForPerson(
  api,
  { idChange, person, businessId }
) {
  const newChangeRes = yield call(
    api.newApprovalChangeRequest,
    idChange,
    person.id_person,
    businessId
  )
  if (newChangeRes.ok) {
    const updateChangeRes = yield call(api.updateChange, idChange, {
      data: {
        authorizedBy: person.id_person
      }
    })

    if (updateChangeRes.ok) {
      yield put(BusinessActions.onApprovalRequested(person))
    } else {
      yield call(onError, updateChangeRes)
    }
  } else {
    if (newChangeRes.data.code === 'USER_MOBILE_NUMBER_NOT_AUTHORIZED') {
      showErrorMessage(
        'It looks like there is a problem with this person’s mobile phone number.',
        'Ask them to add or change their mobile number.'
      )
      yield put(BusinessActions.onError())
    } else {
      yield call(onError, newChangeRes)
    }
  }
}
