import { call, put, select } from 'redux-saga/effects'
import PermissionActions from '../redux-modules/permissions'
import {
  showErrorNotification,
  showSuccessNotification
} from '../utils/notification-handler'

export function* getRolePermissions(api, userRole) {
  const response = yield call(api.getRolePermissions, userRole.role)

  if (response.ok) {
    yield put(PermissionActions.getRolePermissionsSuccess(response.data))
  }
}

export function* updateRolePermissions(api, { role, addToPermissions, removeFromPermissions }) {
  const response = yield call(api.setRolePermissions, role, addToPermissions, removeFromPermissions)

  if (response.ok) {
    yield put(PermissionActions.updateRolePermissionsSuccess(role, addToPermissions, removeFromPermissions))
    const myRole = yield select(state => state.auth.role)
    if (myRole === role) {
      const newPermissions = yield select(state => state.permissions.rolePermissions)
      yield put(PermissionActions.getMyPermissionsSuccess([...newPermissions]))
    }
    showSuccessNotification('The role has been updated')
  } else {
    showErrorNotification(response)
  }
}

export function* getMyPermissions(api) {
  const response = yield call(api.getMyPermissions)

  if (response.ok) {
    yield put(PermissionActions.getMyPermissionsSuccess(response.data))
  }
}
