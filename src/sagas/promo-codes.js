import { call, put, all } from 'redux-saga/effects'
import PromoCodesActions from '../redux-modules/promo-codes'
import { push } from 'react-router-redux'
import Notification from '../components/common/Notification'
import { promoCodeStatus } from '../api/referral/referral-api'

function getPromoCodeUsedCounts(api) {
  return function*(promoCodeData) {
    let promoCode = { ...promoCodeData, promoCodeUsed: 0 }
    const res = yield call(api.getPromoCodeBusinessList, { promo_code_id: promoCode.Id })
    if(res.ok) {
      const rewardedPromoCode = res.data.filter((code) => {
        return code.Status === promoCodeStatus.rewarded
      })
      const promoCodeUsed = rewardedPromoCode.length
      promoCode.promoCodeUsed = promoCodeUsed
    }
    return promoCode
  }
}

export function* getPromoCodes(api, { query }) {
  const response = yield call(api.getPromoCodes, query)
  if(response.ok){
    const { data } = response
    const results = yield all(data.map(getPromoCodeUsedCounts(api)))
    yield put(PromoCodesActions.onPromoCodesReceived(results))
  } else {
    Notification.showFailureMsg(response.data.message)
  }
}

export function* createPromoCode(api, { promoCode }) {
  const response = yield call(api.createPromoCode, promoCode)
  if(response.ok){
    yield put(push(`/promocodes`))
  } else {
    Notification.showFailureMsg(response.data.message)
  }
}

export function* updatePromoCode(api, { id, promoCode }) {
  const response = yield call(api.updatePromoCode, id, promoCode)
  if(response.ok){
    yield put(push(`/promocodes`))
  } else {
    Notification.showFailureMsg(response.data.message)
  }
}
