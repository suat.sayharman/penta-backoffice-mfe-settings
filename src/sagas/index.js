import { all, takeLatest } from 'redux-saga/effects'
import API from '../api'
/* ------------- Types ------------- */
import { AuthTypes } from '../redux-modules/auth'
import { AnnouncementTypes } from '../redux-modules/announcement'
import { PromoCodesTypes } from '../redux-modules/promo-codes'
import { ReferralReportsTypes } from '../redux-modules/referral-reports'
import { BusinessTypes } from '../redux-modules/businesses'
import { PersonTypes } from '../redux-modules/person'
import { MaintenanceTypes } from '../redux-modules/maintenance'
import { PricingTypes } from '../redux-modules/pricing'
import { TransactionsTypes } from '../redux-modules/transactions'
import { VatTypes } from '../redux-modules/vat'
import { LDTypes } from '../redux-modules/launchdarkly'
import { LoginAttemptsTypes } from '../redux-modules/loginAttempts'
import { DirectDebitTypes } from '../redux-modules/direct-debits'
import { CardsTypes } from '../redux-modules/cards'
import { PersonsTypes } from '../redux-modules/persons'
import { UsersTypes } from '../redux-modules/users'
import { PermissionsTypes } from '../redux-modules/permissions'
import { UserRoleTypes } from '../redux-modules/userroles'
import { ActivityLogTypes } from '../redux-modules/activity-logs'

/* ------------- Sagas ------------- */
import {
  createAnnouncement,
  editAnnouncement,
  getAnnouncements,
  removeAnnouncement
} from './announcement'
import { checkAuthState, login, logout } from './auth'
import {
  businessReadyForSolaris,
  cancelDowngrade,
  changePricingPlan,
  confirmBusinessUpdate,
  createTaxRecord,
  deleteBusinessData,
  fetchBusinesses,
  getBusinessData,
  getBusinessFiles,
  restartBusinessIdentification,
  topUpCredit,
  updateBusiness,
  uploadDocument,
  getUsageAndCostsByDate,
  setAccountClosureDate,
  getComplianceQuestions,
  getMissingDocuments,
  downloadDocument,
  updateFreeTrial,
  getBusinessClosureReason,
  saveBusinessClosureReason,
  updateBusinessClosureReason,
  deleteBusinessClosureReason,
  syncBusinessSaga
} from './businesses'
import { getBusinessUsers, authorizeUser, deleteUser } from './users'
import {
  getBusinessIdentificationsByBusinessId,
  getBusinessIdentificationById
} from './business-identifications'
import {
  createPersonIdentification,
  getPerson,
  getPersonIdentifications,
  getPersonIdentificationAttempts,
  savePerson,
  patchPerson,
  setAsLr,
  setAsUbo,
  syncPersons,
  updateUserActiveState,
  syncPersonMobileNumber,
  updateUserPermissions,
  getUserPermissions,
  getUserLoginAttempts,
  resetUserPassword,
  confirmPatchPersonUpdate,
  requestApprovalForPatchPerson,
  sendActivationEmail,
  generateTrustpilotLink
} from './person'
import { getMaintenanceStatus, toggleMaintenanceStatus } from './maintenance'
import {
  getBusinessPersons,
  invitePersonSaga,
  requestApprovalForPerson
} from './persons'
import {
  fetchInitialTransactions,
  selectTransaction,
  getTransactions
} from './transactions'
import {
  setLaunchDarklyClientSaga,
  identifyLaunchDarklyUserSaga
} from './launchdarkly'
import { getVat, createVat, deleteVatServicePeriod } from './vat'
import {
  cancelBill,
  getRemainingCredits,
  getPentaPayments,
  updateInvoice,
  createCreditNote,
  updateCreditNote,
  deleteCreditNote,
  downloadBillDocument,
  previewBillDocument,
  downloadMandate,
  downloadConversionsStatement
} from './pricing'
import {
  getDirectDebitProfile,
  getCollectionLimit,
  createDirectDebitProfile,
  getDirectDebitOnboardingRequest,
  rejectDirectDebitOnboardingRequest,
  requestDirectDebitCollection,
  updateCreditorIdentifier
} from './direct-debits'
import { getPromoCodes, createPromoCode, updatePromoCode } from './promo-codes'
import { getReferralReports, getReferralSummary } from './referral-reports'
import referralApi from '../api/referral/referral-api'
import { getLoginAttempts, resetLoginAttemptsState } from './loginAttempts'
import { getBusinessCards } from './cards'
import { BusinessIdentificationsTypes } from '../redux-modules/business-identifications'
import {
  getRolePermissions,
  updateRolePermissions,
  getMyPermissions
} from './permissions'
import {
  createUserRole,
  getUserRoles,
  getBackofficeUsers,
  assignUserToRole,
  updateRoleName,
  deleteRole
} from './userroles'

import { getActivityLogs } from './activity-logs'

export const api = API.create()
/* ------------- Connect Types To Sagas ------------- */
export default function* root() {
  yield all([
    takeLatest(
      MaintenanceTypes.TOGGLE_MAINTENANCE_STATUS,
      toggleMaintenanceStatus,
      api
    ),
    takeLatest(
      MaintenanceTypes.GET_MAINTENANCE_STATUS,
      getMaintenanceStatus,
      api
    ),
    takeLatest(AnnouncementTypes.GET_ANNOUNCEMENTS, getAnnouncements, api),
    takeLatest(AnnouncementTypes.CREATE_ANNOUNCEMENT, createAnnouncement, api),
    takeLatest(AnnouncementTypes.EDIT_ANNOUNCEMENT, editAnnouncement, api),
    takeLatest(AnnouncementTypes.REMOVE_ANNOUNCEMENT, removeAnnouncement, api),

    takeLatest(PromoCodesTypes.GET_PROMO_CODES, getPromoCodes, referralApi),
    takeLatest(PromoCodesTypes.CREATE_PROMO_CODE, createPromoCode, referralApi),
    takeLatest(PromoCodesTypes.UPDATE_PROMO_CODE, updatePromoCode, referralApi),

    takeLatest(
      ReferralReportsTypes.GET_REFERRAL_REPORTS,
      getReferralReports,
      referralApi
    ),
    takeLatest(
      ReferralReportsTypes.GET_REFERRAL_SUMMARY,
      getReferralSummary,
      referralApi
    ),

    takeLatest(AuthTypes.LOGIN_REQUEST, login, api),
    takeLatest(AuthTypes.LOGOUT_REQUEST, logout, api),
    takeLatest(BusinessTypes.FETCH_BUSINESSES, fetchBusinesses, api),
    takeLatest(BusinessTypes.GET_BUSINESS_DATA, getBusinessData, api),
    takeLatest(
      BusinessTypes.CONFIRM_BUSINESS_UPDATE,
      confirmBusinessUpdate,
      api
    ),
    takeLatest(BusinessTypes.UPDATE_BUSINESS, updateBusiness, api),
    takeLatest(BusinessTypes.GET_FILES, getBusinessFiles, api),
    takeLatest(PersonsTypes.GET_PERSONS, getBusinessPersons, api),
    takeLatest(UsersTypes.GET_BUSINESS_USERS, getBusinessUsers, api),
    takeLatest(BusinessTypes.UPDATE_TAX_INFO, createTaxRecord, api),
    takeLatest(BusinessTypes.UPLOAD_DOCUMENT, uploadDocument, api),
    takeLatest(UsersTypes.DELETE_USER, deleteUser, api),
    takeLatest(UsersTypes.AUTHORIZE_USER, authorizeUser, api),
    takeLatest(
      PersonsTypes.REQUEST_APPROVAL_FOR_PERSON,
      requestApprovalForPerson,
      api
    ),
    takeLatest(
      BusinessTypes.BUSINESS_READY_FOR_SOLARIS,
      businessReadyForSolaris,
      api
    ),
    takeLatest(
      BusinessTypes.SET_ACCOUNT_CLOSURE_DATE,
      setAccountClosureDate,
      api
    ),
    takeLatest(
      BusinessTypes.GET_COMPLIANCE_QUESTIONS_REQUEST,
      getComplianceQuestions,
      api
    ),
    takeLatest(
      BusinessTypes.GET_MISSING_DOCUMENTS_REQUEST,
      getMissingDocuments,
      api
    ),
    takeLatest(BusinessTypes.DOWNLOAD_DOCUMENT, downloadDocument, api),
    takeLatest(
      BusinessTypes.UPDATE_BUSINESS_CLOSURE_REASON,
      updateBusinessClosureReason,
      api
    ),
    takeLatest(
      BusinessTypes.SAVE_BUSINESS_CLOSURE_REASON,
      saveBusinessClosureReason,
      api
    ),
    takeLatest(
      BusinessTypes.GET_BUSINESS_CLOSURE_REASON,
      getBusinessClosureReason,
      api
    ),
    takeLatest(
      BusinessTypes.DELETE_BUSINESS_CLOSURE_REASON,
      deleteBusinessClosureReason,
      api
    ),
    takeLatest(BusinessTypes.SYNC_BUSINESS_REQUEST, syncBusinessSaga, api),
    takeLatest(
      BusinessIdentificationsTypes.GET_BUSINESS_IDENTIFICATIONS_BY_BUSINESS_ID,
      getBusinessIdentificationsByBusinessId,
      api
    ),
    takeLatest(
      BusinessIdentificationsTypes.GET_BUSINESS_IDENTIFICATION_BY_ID,
      getBusinessIdentificationById,
      api
    ),
    takeLatest(PersonTypes.SYNC_PERSONS, syncPersons, api),
    takeLatest(PersonTypes.GET_PERSON, getPerson, api),
    takeLatest(
      PersonTypes.GET_PERSON_IDENTIFICATIONS,
      getPersonIdentifications,
      api
    ),
    takeLatest(
      PersonTypes.GET_PERSON_IDENTIFICATION_ATTEMPTS,
      getPersonIdentificationAttempts,
      api
    ),
    takeLatest(
      PersonTypes.CREATE_PERSON_IDENTIFICATION,
      createPersonIdentification,
      api
    ),
    takeLatest(PersonTypes.SET_AS_LR, setAsLr, api),
    takeLatest(PersonTypes.UPDATE_USER_PERMISSIONS, updateUserPermissions, api),
    takeLatest(PersonTypes.GET_USER_PERMISSIONS, getUserPermissions, api),
    takeLatest(PersonTypes.GET_USER_LOGIN_ATTEMPTS, getUserLoginAttempts, api),
    takeLatest(PersonTypes.RESET_USER_PASSWORD, resetUserPassword, api),
    takeLatest(PersonTypes.SET_AS_UBO, setAsUbo, api),
    takeLatest(PersonTypes.SAVE_PERSON, savePerson, api),
    takeLatest(PersonTypes.PATCH_PERSON, patchPerson, api),
    takeLatest(
      PersonTypes.GENERATE_TRUSTPILOT_LINK,
      generateTrustpilotLink,
      api
    ),
    takeLatest(
      PersonTypes.REQUEST_APPROVAL_FOR_PATCH_PERSON,
      requestApprovalForPatchPerson,
      api
    ),
    takeLatest(
      PersonTypes.CONFIRM_PATCH_PERSON_UPDATE,
      confirmPatchPersonUpdate,
      api
    ),
    takeLatest(
      PersonTypes.UPDATE_USER_ACTIVE_STATE,
      updateUserActiveState,
      api
    ),
    takeLatest(
      PersonTypes.SYNC_PERSON_MOBILE_NUMBER,
      syncPersonMobileNumber,
      api
    ),
    takeLatest(PersonTypes.SEND_ACTIVATION_EMAIL, sendActivationEmail, api),
    takeLatest(CardsTypes.GET_CARDS, getBusinessCards, api),
    takeLatest(BusinessTypes.DELETE_BUSINESS_DATA, deleteBusinessData, api),
    takeLatest(
      BusinessTypes.RESTART_BUSINESS_IDENTIFICATION,
      restartBusinessIdentification,
      api
    ),
    takeLatest(PersonsTypes.INVITE_PERSON_REQUEST, invitePersonSaga, api),
    takeLatest(PricingTypes.CANCEL_DOWNGRADE, cancelDowngrade, api),
    takeLatest(PricingTypes.CHANGE_PLAN, changePricingPlan, api),
    takeLatest(PricingTypes.TOP_UP_CREDIT, topUpCredit, api),
    takeLatest(
      TransactionsTypes.FETCH_INITIAL_TRANSACTIONS,
      fetchInitialTransactions,
      api
    ),
    takeLatest(TransactionsTypes.REQUEST_TRANSACTIONS, getTransactions, api),
    takeLatest(TransactionsTypes.SELECT_TRANSACTION, selectTransaction, api),
    takeLatest(
      PricingTypes.GET_USAGE_AND_COSTS_BY_DATE,
      getUsageAndCostsByDate,
      api
    ),
    takeLatest(PricingTypes.UPDATE_FREE_TRIAL, updateFreeTrial, api),
    takeLatest(PricingTypes.CANCEL_BILL, cancelBill, api),
    takeLatest(PricingTypes.GET_REMAINING_CREDITS, getRemainingCredits, api),
    takeLatest(PricingTypes.GET_PENTA_PAYMENTS, getPentaPayments, api),
    takeLatest(VatTypes.GET_VAT, getVat, api),
    takeLatest(VatTypes.CREATE_VAT, createVat, api),
    takeLatest(VatTypes.DELETE_VAT_SERVICE_PERIOD, deleteVatServicePeriod, api),
    takeLatest(LDTypes.SET_LAUNCHDARKLY_CLIENT, setLaunchDarklyClientSaga),
    takeLatest(
      LDTypes.IDENTIFY_LAUNCHDARKLY_USER,
      identifyLaunchDarklyUserSaga
    ),
    takeLatest(
      LoginAttemptsTypes.RESET_LOGIN_ATTEMPTS_STATE,
      resetLoginAttemptsState
    ),
    takeLatest(
      DirectDebitTypes.GET_DIRECT_DEBIT_PROFILE,
      getDirectDebitProfile,
      api
    ),
    takeLatest(DirectDebitTypes.GET_COLLECTION_LIMIT, getCollectionLimit, api),
    takeLatest(
      DirectDebitTypes.CREATE_DIRECT_DEBIT_PROFILE,
      createDirectDebitProfile,
      api
    ),
    takeLatest(
      DirectDebitTypes.GET_DIRECT_DEBIT_ONBOARDING_REQUEST,
      getDirectDebitOnboardingRequest,
      api
    ),
    takeLatest(
      DirectDebitTypes.REJECT_DIRECT_DEBIT_ONBOARDING_REQUEST,
      rejectDirectDebitOnboardingRequest,
      api
    ),
    takeLatest(
      DirectDebitTypes.REQUEST_COLLECTION_LIST,
      requestDirectDebitCollection,
      api
    ),
    takeLatest(
      DirectDebitTypes.UPDATE_CREDITOR_IDENTIFIER_REQUEST,
      updateCreditorIdentifier,
      api
    ),
    takeLatest(LoginAttemptsTypes.GET_LOGIN_ATTEMPTS, getLoginAttempts, api),
    takeLatest(
      LoginAttemptsTypes.RESET_LOGIN_ATTEMPTS_STATE,
      resetLoginAttemptsState
    ),
    takeLatest(PricingTypes.UPDATE_INVOICE, updateInvoice, api),
    takeLatest(PricingTypes.CREATE_CREDIT_NOTE, createCreditNote, api),
    takeLatest(PricingTypes.UPDATE_CREDIT_NOTE, updateCreditNote, api),
    takeLatest(PricingTypes.DELETE_CREDIT_NOTE, deleteCreditNote, api),
    takeLatest(PricingTypes.DOWNLOAD_BILL_DOCUMENT, downloadBillDocument, api),
    takeLatest(PricingTypes.PREVIEW_BILL_DOCUMENT, previewBillDocument, api),
    takeLatest(PricingTypes.DOWNLOAD_MANDATE, downloadMandate, api),
    takeLatest(
      PricingTypes.DOWNLOAD_CONVERSIONS_STATEMENT,
      downloadConversionsStatement,
      api
    ),
    takeLatest(AuthTypes.CHECK_AUTH_STATE, checkAuthState, api),
    takeLatest(PermissionsTypes.GET_MY_PERMISSIONS, getMyPermissions, api),
    takeLatest(PermissionsTypes.GET_ROLE_PERMISSIONS, getRolePermissions, api),
    takeLatest(
      PermissionsTypes.UPDATE_ROLE_PERMISSIONS,
      updateRolePermissions,
      api
    ),
    takeLatest(UserRoleTypes.GET_ROLES, getUserRoles, api),
    takeLatest(UserRoleTypes.CREATE_ROLE, createUserRole, api),
    takeLatest(UserRoleTypes.GET_BACKOFFICE_USERS, getBackofficeUsers, api),
    takeLatest(UserRoleTypes.ASSIGN_USER_TO_ROLE, assignUserToRole, api),
    takeLatest(UserRoleTypes.UPDATE_ROLE_NAME, updateRoleName, api),
    takeLatest(UserRoleTypes.DELETE_ROLE, deleteRole, api),
    takeLatest(ActivityLogTypes.GET_ACTIVITY_LOGS, getActivityLogs, api)
  ])
}
