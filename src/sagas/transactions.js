import { all, call, put } from 'redux-saga/effects'
import _ from 'lodash'
import TransactionsActions from '../redux-modules/transactions'
import { showErrorNotification } from '../utils/notification-handler'

const defaults = {
  states: '["booked","accepted"]',
  currency: 'EUR',
  unit: 'cents',
  mode: 'list'
}

export function* fetchInitialTransactions(
  api,
  { accountId, businessId, filter }
) {
  const responses = yield all([
    call(api.getTransactions, businessId, accountId, {
      ...defaults,
      ...filter
    }),
    call(api.getScheduledOrders, businessId, accountId),
    call(api.getReservations, businessId, accountId),
    call(api.getBalanceData, businessId, accountId),
    call(api.getCategories, businessId)
  ])

  const errorResp = _.filter(responses, x => !x.ok)
  if (errorResp && errorResp.length) {
    errorResp.forEach(x => showErrorNotification(x))
  }

  yield put(
    TransactionsActions.onInitialTransactionsDataLoaded({
      transactions: getResponseDataOrDefault(responses[0]),
      scheduledOrders: getResponseDataOrDefault(responses[1]),
      reservations: getResponseDataOrDefault(responses[2]),
      balance: getResponseDataOrDefault(responses[3], {}),
      categories: getResponseDataOrDefault(responses[4])
    })
  )
}

export function* getTransactions(
  api,
  { businessId, accountId, filter, number, size }
) {
  const response = yield call(
    api.getTransactions,
    businessId,
    accountId,
    { ...defaults, ...filter },
    number || 1,
    size || 50
  )
  if (response.ok) {
    const {
      headers: { links, pagenumber, total, count }
    } = response
    const linksObj = JSON.parse(links)
    const hasMore = !!linksObj.next

    yield put(
      TransactionsActions.onTransactionsDataLoaded(
        response.data,
        total,
        hasMore,
        pagenumber,
        count
      )
    )
  } else {
    showErrorNotification(response.data)
    yield put(TransactionsActions.onError())
  }
}

function getResponseDataOrDefault(response, defaultValue = null) {
  return response.ok ? response.data : defaultValue
}

export function* selectTransaction(api, { accountId, businessId, id }) {
  const response = yield call(api.getTransaction, businessId, accountId, id)
  if (response.ok) {
    yield put(TransactionsActions.onTransactionLoaded(response.data))
  }
}
