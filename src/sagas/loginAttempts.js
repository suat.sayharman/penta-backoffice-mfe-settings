import { call, put } from 'redux-saga/effects'
import LoginAttemptsActions from '../redux-modules/loginAttempts'
import { showErrorNotification } from '../utils/notification-handler'

export function* getLoginAttempts(api, { limit, offset, search }) {
  const response = yield call(api.searchLoginAttempts, limit, offset, search)
  if (response.ok) {
    yield put(LoginAttemptsActions.setLoginAttempts(response.data))
  } else {
    showErrorNotification(response)
  }
}

export function* resetLoginAttemptsState(){
  yield put(LoginAttemptsActions.setLoginAttempts([]))
}
