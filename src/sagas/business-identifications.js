import { call, put } from 'redux-saga/effects'
import { showErrorNotification } from '../utils/notification-handler'
import BusinessIdentificationsActions from '../redux-modules/business-identifications'

export function* getBusinessIdentificationsByBusinessId(api, { businessId }) {
  const response = yield call(api.getBusinessIdentificationsByBusinessId, businessId)
  if (response.ok) {
    yield put(BusinessIdentificationsActions.onBusinessIdentificationsReceived(response.data))
  } else {
    showErrorNotification(response)
    yield put(BusinessIdentificationsActions.onError())
  }
}

export function* getBusinessIdentificationById(api, { businessId, identificationId }) {
  const response = yield call(api.getBusinessIdentificationById, businessId, identificationId)
  if (response.ok) {
    yield put(BusinessIdentificationsActions.onBusinessIdentificationReceived(response.data))
  } else {
    showErrorNotification(response)
    yield put(BusinessIdentificationsActions.onError())
  }
}