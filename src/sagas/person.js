import { all, call, put, select } from 'redux-saga/effects'
import { startAsyncValidation, reset } from 'redux-form'

import PersonActions from '../redux-modules/person'
import UsersActions from '../redux-modules/users'
import PersonsActions from '../redux-modules/persons'
import {
  showErrorNotification,
  showSuccessNotification
} from '../utils/notification-handler'
import { getBusinessPersons } from './persons'

export function* syncPersons(api, { businessId }) {
  const response = yield call(api.syncPersons, businessId)
  if (response.ok) {
    yield put(PersonActions.onSyncComplete()) // move this to PersonsActions instead of PersonActions
    yield put(PersonsActions.getPersons(businessId))
    yield put(UsersActions.getBusinessUsers(businessId))
  } else {
    showErrorNotification(response)
    yield put(PersonActions.onSyncError())
  }
}

export function* updateUserPermissions(api, { businessId, userId, data }) {
  const response = yield call(
    api.updateUserPermissions,
    businessId,
    userId,
    data
  )
  if (response.ok) {
    yield put(
      PersonActions.updateUserPermissionsSuccessful({
        // If a user becomes admin his permission services get deleted
        is_admin: data.is_admin,
        services: data.is_admin ? [] : data.services
      })
    )
  } else {
    yield put(PersonActions.updateUserPermissionsFailed())
    showErrorNotification(response)
  }
}

export function* getUserPermissions(api, { businessId, userId }) {
  const response = yield call(api.getUserPermissions, businessId, userId)
  if (response.ok) {
    yield put(PersonActions.setUserPermissions(response.data))
  } else {
    yield put(PersonActions.getUserPermissionsFailed())
    showErrorNotification(response)
  }
}

export function* getUserLoginAttempts(api, { userId, limit, offset }) {
  const response = yield call(
    api.getLoginAttemptsForUser,
    userId,
    limit,
    offset
  )
  if (response.ok) {
    yield put(PersonActions.setUserLoginAttempts(response.data))
  } else {
    showErrorNotification(response)
  }
}

export function* resetUserPassword(api, body) {
  const response = yield call(api.resetUserPassword, { email: body.email })
  if (response.ok) {
    showSuccessNotification(
      'A request for reseting the password was sent to the user'
    )
  } else {
    showErrorNotification(response)
  }
}

export function* getPerson(api, { businessId, personId }) {
  const responses = yield all([
    call(api.getPerson, businessId, personId),
    call(api.getVerifiedNumber, businessId, personId)
  ])

  const personResponse = responses[0]
  if (personResponse.ok) {
    const { data } = personResponse
    data.businessId = businessId
    const phoneResponse = responses[1]
    if (phoneResponse.ok && phoneResponse.data) {
      data.verified_phone = phoneResponse.data
    } else {
      data.verified_phone = null
    }
    yield put(PersonActions.onPersonLoaded(data))
  } else {
    showErrorNotification(personResponse)
  }
}

export function* getPersonIdentifications(api, { personId }) {
  const response = yield call(api.getPersonIdentifications, personId)
  if (response.ok) {
    yield put(PersonActions.onPersonIdentificationsFetched(response.data))
  } else {
    showErrorNotification(response)
  }
}

export function* getPersonIdentificationAttempts(api, { identificationId }) {
  const response = yield call(
    api.getPersonIdentificationAttempts,
    identificationId
  )
  if (response.ok) {
    yield put(
      PersonActions.onPersonIdentificationAttemptsFetched(response.data)
    )
  } else {
    showErrorNotification(response)
  }
}

export function* createPersonIdentification(api, { businessId, personId }) {
  const response = yield call(
    api.createPersonIdentification,
    businessId,
    personId
  )
  if (response.ok) {
    yield call(getPersonIdentifications, api, { personId })
  } else {
    showErrorNotification(response)
  }
}

export function* setAsLr(api, { businessId, personId }) {
  const response = yield call(api.setAsLr, businessId, personId)
  if (response.ok) {
    yield put(PersonActions.onLegalRepSet())
    yield call(getPerson, api, { businessId, personId })
    yield put(PersonsActions.getPersons(businessId))
  } else {
    showErrorNotification(response)
  }
}

export function* setAsUbo(api, { businessId, personId, voting_share }) {
  const response = yield call(api.setAsUbo, businessId, personId, voting_share)
  if (response.ok) {
    yield call(getPerson, api, { businessId, personId })
    yield put(PersonsActions.getPersons(businessId))
  } else {
    showErrorNotification(response)
  }
}

export function* savePerson(api, { businessId, data }) {
  let response = yield call(api.savePerson, businessId, data)
  if (response.ok) {
    yield put(PersonsActions.getPersons(businessId))
    yield put(PersonActions.onSyncComplete())
  } else {
    showErrorNotification(response)
  }
}

export function* patchPerson(api, { businessId, personId, data }) {
  let response = yield call(api.patchPerson, businessId, personId, data)
  if (response.ok) {
    const { status, data: responseData } = response
    if (status === 202) {
      // load persons (and users) if not yet available in state - needed to check if authorized person is LR
      const persons = yield select(state => state.persons.persons)
      if (!persons) {
        yield call(getBusinessPersons, api, { businessId })
      }
      yield put(
        PersonActions.onPatchPersonChangeRequestReceived(responseData.id_change)
      )
    } else {
      yield call(onUpdateSuccess, businessId, personId)
    }
  } else {
    showErrorNotification(response)
  }
}

export function* confirmPatchPersonUpdate(
  api,
  { idChange, businessId, tan, personId }
) {
  yield put(startAsyncValidation('confirmUpdateForm'))
  const response = yield call(
    api.confirmApprovalChangeRequest,
    idChange,
    tan,
    personId,
    businessId
  )
  if (response.ok) {
    yield call(onUpdateSuccess, businessId, personId)
  } else {
    const patchPersonData = yield select(state => state.person.patchPersonData)
    yield put(PersonActions.patchPerson(businessId, personId, patchPersonData))
    showErrorNotification(response)
  }
}

export function* updateUserActiveState(api, { businessId, userId, isActive }) {
  const response = yield call(
    api.updateUserActiveState,
    businessId,
    userId,
    isActive
  )

  if (response.ok) {
    yield put(PersonActions.updateUserActiveStateSuccess(isActive))
  } else {
    showErrorNotification(response)
    yield put(PersonActions.onSyncComplete())
  }
}

export function* syncPersonMobileNumber(api, { personId, businessId }) {
  const syncResponse = yield call(api.syncPersonMobileNumber, personId)
  if (syncResponse.status === 201) {
    const response = yield call(api.getVerifiedNumber, businessId, personId)
    if (response.ok) {
      yield put(PersonActions.getPersonMobileNumberSuccess(response.data))
    }
  }
  yield put(PersonActions.onSyncComplete())
}

export function* requestApprovalForPatchPerson(
  api,
  { idChange, person, businessId }
) {
  const newChangeRes = yield call(
    api.newApprovalChangeRequest,
    idChange,
    person.id_person,
    businessId
  )
  if (newChangeRes.ok) {
    const updateChangeRes = yield call(api.updateChange, idChange, {
      data: {
        authorizedBy: person.id_person
      }
    })

    if (updateChangeRes.ok) {
      yield put(PersonActions.onApprovalRequested(person))
    } else {
      showErrorNotification(updateChangeRes)
    }
  } else {
    showErrorNotification(newChangeRes)
  }
}

export function* sendActivationEmail(api, { businessId, userId }) {
  const response = yield call(api.sendActivationEmail, businessId, userId)
  if (response.ok) {
    yield put(PersonsActions.getPersons(businessId))
    yield put(PersonActions.onSendActivationEmailSuccess())
  } else {
    showErrorNotification(response)
  }
}

export function* generateTrustpilotLink(api, { userId }) {
  const response = yield call(api.generateTrustpilotLink, userId)
  if (response.ok) {
    yield put(PersonActions.onTrustpilotLinkReceived(response.data.url))
  } else {
    showErrorNotification(response)
  }
}

function* onUpdateSuccess(businessId, personId) {
  yield put(PersonActions.onPatchPersonUpdated())
  yield put(reset('confirmUpdateForm'))
  yield put(PersonActions.getPerson(businessId, personId))
  yield put(PersonsActions.getPersons(businessId))
  yield put(UsersActions.getBusinessUsers(businessId))
}
