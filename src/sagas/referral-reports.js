import { call, put } from 'redux-saga/effects'
import ReferralReportsActions from '../redux-modules/referral-reports'
import Notification from '../components/common/Notification'

export function* getReferralReports(api, { query }) {
  const response = yield call(api.getReferralReports, query)
  if(response.ok){
    const { data } = response
    yield put(ReferralReportsActions.onReportsReceived(data))
  } else {
    Notification.showFailureMsg(response.data.message)
  }
}

export function* getReferralSummary(api, { query }) {
  const response = yield call(api.getReferralSummary, query)
  if(response.ok){
    const { data } = response
    yield put(ReferralReportsActions.onSummaryReceived(data))
  } else {
    Notification.showFailureMsg(response.data.message)
  }
}