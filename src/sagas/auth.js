import { call, put } from 'redux-saga/effects'
import AuthActions from '../redux-modules/auth'
import { push } from 'react-router-redux'
import { showErrorNotification } from '../utils/notification-handler'
import LDActions, { mkUser } from '../redux-modules/launchdarkly'
import PermissionActions from '../redux-modules/permissions'

export function* login(api, { email, password }) {
  yield put(AuthActions.logoutSuccess())
  const response = yield call(api.login, email, password)
  if (response.ok) {
    const { data } = response
    if (data.realm === 'backoffice') {
      yield put(AuthActions.loginSuccess(data))
      sessionStorage.setItem('email', email)
      yield put(LDActions.identifyLaunchdarklyUser(mkUser(data.id_user)))
      yield put(push('/home'))
    } else {
      showErrorNotification(response)
      yield put(AuthActions.onAuthError('Not authorized for backoffice.'))
    }

    yield put(PermissionActions.getMyPermissions())
  } else {
    showErrorNotification(response)
    yield put(AuthActions.onAuthError(response.data.action))
  }
}

export function* checkAuthState(api) {
  const token = sessionStorage.getItem('auth_token')

  if (token) {
    const response = yield call(api.getMyRole)
    yield put(PermissionActions.getMyPermissions())

    if (response.ok) {
      yield put(AuthActions.checkAuthStateComplete(response.data.role))
    }
  }
}

export function* logout(api) {
  const response = yield call(api.logout)
  if (response.ok) {
    yield put(AuthActions.logoutSuccess())
    yield put(push('/login'))
    yield put(LDActions.identifyLaunchdarklyUser({ key: 'anonymous', anonymous: true }))
  }
}
