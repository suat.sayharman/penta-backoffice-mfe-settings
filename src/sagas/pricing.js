import { call, put, select, delay } from 'redux-saga/effects'
import FileSaver from 'file-saver'
import {
  showErrorNotification,
  showSuccessNotification
} from '../utils/notification-handler'
import PricingActions from '../redux-modules/pricing'
import {getFileNameFromContentDisposition} from "../utils/getFileNameFromContentDisposition";
import {buildFileUrl} from "../utils/transform-data";

export function* cancelBill(api, { bill, reason }) {
  const response = bill.type === 'invoice'
    ? yield call(api.cancelInvoice, bill.id, reason)
    : yield call(api.cancelCreditNote, bill.id, reason)
  if (response.ok) {
    const successMsg = `The ${bill.type === 'creditNote' ? 'Credit note' : "Invoice"} has been cancelled successfully`
    showSuccessNotification(successMsg)
    const businessId = yield select(
      state => state.businesses.businessDetails.id
    )
    yield put(PricingActions.getRemainingCredits(businessId))

    // wait a bit for generate PDF task to be completed before reloading the invoices
    yield delay(2000)

    const invoicesResponse = yield call(api.getInvoicesList, businessId, true)
    const creditNotesResponse = yield call(api.getCreditNotes, businessId, true)
    if (!invoicesResponse.ok) {
      showErrorNotification(invoicesResponse)
    } else if (!creditNotesResponse.ok) {
      showErrorNotification(creditNotesResponse)
    } else {
      yield put(PricingActions.onInvoicesRecieved(invoicesResponse.data))
      yield put(PricingActions.onCreditNotesReceived(creditNotesResponse.data))
    }
  } else {
    showErrorNotification(response)
  }
}

export function* getRemainingCredits(api, { businessId }) {
  const response = yield call(api.getRemainingCredits, businessId)
  if (response.ok) {
    yield put(PricingActions.onRemainingCreditsReceieved(response.data))
  } else {
    showErrorNotification(response)
  }
}

export function* getPentaPayments(api, { filter, direction, page=1, size=20 }) {
  const response = yield call(api.getPentaPayments, filter, direction, page, size)
  if (response.ok) {
    const {
      headers: { links, total }
    } = response
    const linksObj = JSON.parse(links)
    const hasMore = !!linksObj.next
    yield put(
      PricingActions.onPentaPaymentsRecieved(
        response.data,
        total,
        hasMore,
        page,
        size
      )
    )
  } else {
    showErrorNotification(response)
  }
}

export function* updateInvoice(api, { businessId, payload }) {
  const response = yield call(api.updateInvoice, payload)
  if (response.ok) {
    const invoicesResponse = yield call(api.getInvoicesList, businessId)
    if (invoicesResponse.ok) {
      yield put(PricingActions.onInvoicesRecieved(invoicesResponse.data))
      yield put(PricingActions.onInvoiceUpdated())
      showSuccessNotification('Invoice status has been updated successfully!')
    } else {
      showErrorNotification(invoicesResponse)
    }
  } else {
    showErrorNotification(response)
  }
}

export function* createCreditNote(api, { businessId, payload } ) {
  const response = yield call(api.createCreditNote, businessId, payload)
  if (response.ok) {
    yield put(PricingActions.onCreditNoteSaved(response.data))
    const successMessage = payload.draft
      ? 'Draft has been saved successfully!'
      : 'Credit note has been saved successfully!'
    showSuccessNotification(successMessage)
    if (!payload.draft) {
      yield delay(3000)
    }
    const creditNotesResponse = yield call(api.getCreditNotes, businessId)
    if (creditNotesResponse.ok) {
      yield put(
        PricingActions.onCreditNotesReceived(creditNotesResponse.data )
      )
    } else {
      showErrorNotification(creditNotesResponse)
    }
  } else {
    showErrorNotification(response)
  }
}

export function* updateCreditNote(
  api,
  {
    businessId,
    creditNoteId,
    payload,
    pauseFetchingCreditNotes = true
  }
) {
  if (payload && Object.keys(payload).includes('status')){
    payload = { status_updates:payload }
  }
  const response = yield call(api.updateCreditNote, creditNoteId, payload)
  if (response.ok) {
    yield put(PricingActions.onCreditNoteSaved(response.data))
    const successMessage = payload.draft
      ? 'Draft has been saved successfully!'
      : 'Credit note has been saved successfully!'
    showSuccessNotification(successMessage)
    if (!payload.draft && pauseFetchingCreditNotes) {
      yield delay(3000)
    }
    const creditNotesResponse = yield call(api.getCreditNotes, businessId)
    if (creditNotesResponse.ok) {
      yield put(
        PricingActions.onCreditNotesReceived(creditNotesResponse.data )
      )
    } else {
      showErrorNotification(creditNotesResponse)
    }
  } else {
    showErrorNotification(response)
  }
}

export function* deleteCreditNote(api, { businessId, id }) {
  const response = yield call(api.deleteCreditNote, id)
  if (response.ok) {
    yield put(PricingActions.onCreditNoteDeleted())
    showSuccessNotification("Draft has been deleted successfully")
    const creditNotesResponse = yield call(api.getCreditNotes, businessId)
    if (creditNotesResponse.ok) {
      yield put(
        PricingActions.onCreditNotesReceived(creditNotesResponse.data )
      )
    } else {
      showErrorNotification(creditNotesResponse)
    }
  } else {
    showErrorNotification(response)
  }
}

export function* downloadBillDocument(api, {bill}) {
  const url = buildFileUrl(bill)
  const response = yield call(api.downloadBillDocument, url)

  if (response.ok && response.data) {
    const filename = getFileNameFromContentDisposition(response.headers['content-disposition'])
      .replace(/[;]/gi, '');
    FileSaver.saveAs(response.data, filename)
  } else {
    showErrorNotification(response)
  }
}

export function* previewBillDocument(api, {bill}) {
  const url = buildFileUrl(bill)
  const response = yield call(api.downloadBillDocument, url)

  if (response.ok && response.data) {
    const newWindow = window.open('/')
    newWindow.onload = () => {
      newWindow.location = URL.createObjectURL(response.data);
    };
  } else {
    showErrorNotification(response)
  }
}

export function* downloadMandate(api, {id}) {
  const response = yield call(api.downloadMandate, id)

  if (response.ok && response.data) {
    const filename = getFileNameFromContentDisposition(response.headers['content-disposition'])
      .replace(/[;]/gi, '');
    FileSaver.saveAs(response.data, filename)
  } else {
    showErrorNotification(response)
  }
}
export function* downloadConversionsStatement(api) {
  const businessId = yield select(
    state => state.businesses.businessDetails.id
  )
  const response = yield call(api.downloadConversionsStatement, businessId)
  if (response.ok && response.data) {
    const filename = getFileNameFromContentDisposition(response.headers['content-disposition'])
      .replace(/[;]/gi, '');
    FileSaver.saveAs(response.data, filename)
  } else {
    showErrorNotification(response)
  }
}

