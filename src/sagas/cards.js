import { call, put, all } from 'redux-saga/effects'
import CardsActions from '../redux-modules/cards'
import { areLimitsAvailable } from '../utils/cards-utils'
import { showErrorNotification } from '../utils/notification-handler'

export function* getBusinessCards(api, { businessId }) {
  const response = yield call(api.getCards, businessId)
  if (response.ok) {
    const validCards = response.data.filter(card =>
      areLimitsAvailable(card.status)
    )

    const limits = yield all(
      validCards.map(card =>
        call(api.getCardLimit, card.id_business, card.id_account, card.id)
      )
    )

    const cardsWithLimits = response.data.map(card => {
      const cardLimitResponse = limits.find(
        limit => limit.ok && limit.data.card_id === card.id
      )
      const cardLimit =
        cardLimitResponse && cardLimitResponse.data
          ? { ...cardLimitResponse.data }
          : null

      return { ...card, limits: cardLimit }
    })

    const cards = [...cardsWithLimits]
    yield put(CardsActions.onCardsReceived(cards))
  } else {
    showErrorNotification(response)
  }
}
