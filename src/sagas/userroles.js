import { call, put, select } from 'redux-saga/effects'
import UserRoleActions from '../redux-modules/userroles'
import {
  showErrorMessage,
  showErrorNotification,
  showSuccessNotification
} from '../utils/notification-handler'

export function* getUserRoles(api) {
  const response = yield call(api.getAllRoles)

  if (response.ok) {
    yield put(UserRoleActions.getRolesSuccess(response.data))
  }
}

export function* createUserRole(api, { role }) {
  const roles = yield select(state => state.userroles.roles);
  const hasSameName = roles.some(({ role_name }) => role_name === role);

  if (hasSameName) {
    return showErrorMessage(`Role with name ${role} is already exist. Please select another name`, 'Cannot create role')
  }

  const response = yield call(api.createRole, role)

  if (response.ok) {
    yield put(UserRoleActions.createRoleSuccess())
    yield call(getUserRoles, api)
    showSuccessNotification('The role has been updated')
  } else {
    showErrorNotification(response)
  }
}

export function* getBackofficeUsers (api) {
  const response = yield call(api.getBackofficeUsers)

  if (response.ok) {
    yield put (UserRoleActions.getBackofficeUsersSuccess(response.data))
  }
}

export function* assignUserToRole(api, { userId, role }) {
  const response = yield call(api.assignUserToRole, userId, role)

  if (response.ok) {
    yield put(UserRoleActions.assignUserToRoleSuccess(userId, role))
  }
}

export function* updateRoleName(api, { roleId, newName }) {
  const roles = yield select(state => state.userroles.roles);
  const hasSameName = roles.some(({ role_name }) => role_name === newName);

  if (hasSameName) {
    return showErrorMessage(`Role with name ${newName} is already exist. Please select another name`, 'Cannot create role')
  }

  const response = yield call(api.updateRoleName, roleId, newName)

  if (response.ok) {
    yield put(UserRoleActions.getRoles())
    yield put(UserRoleActions.updateRoleNameSuccess())
  }
}

export function* deleteRole(api, { role }) {
  const users = yield select(state => state.userroles.users);
  const hasUser = users.some((user) => user.role === role.role_name);

  if (hasUser) {
    return showErrorMessage(`Please unassign users from ${role.role_name} role first`, 'User role cannot be deleted')
  }

  const response = yield call(api.deleteRole, role.id)

  if (response.ok) {
    yield put(UserRoleActions.getRoles())
  }
}
