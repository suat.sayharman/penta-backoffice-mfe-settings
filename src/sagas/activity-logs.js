import {call, put} from 'redux-saga/effects'
import {showErrorNotification} from '../utils/notification-handler'
import ActivityLogActions from '../redux-modules/activity-logs'

export function* getActivityLogs(api, {filters, page, perPage}) {
  const response = yield call(api.getActivityLogs, filters, page, perPage)
  if (response.ok) {
    yield put(ActivityLogActions.onActivityLogsReceived(response.data))
  } else {
    showErrorNotification(response)
  }
}
