import { call, put, select } from 'redux-saga/effects'
import DirectDebitActions from '../redux-modules/direct-debits'
import { showErrorNotification } from '../utils/notification-handler'

export function* getDirectDebitProfile(api, { businessId }) {
  const response = yield call(api.getDirectDebitProfile, businessId)
  if (response.status === 404) {
    yield put(DirectDebitActions.onGetDirectDebitProfileSuccess(null))
  } else if (!response.ok) {
    showErrorNotification(response)
    yield put(DirectDebitActions.onGetDirectDebitProfileError())
  } else {
    yield put(DirectDebitActions.onGetDirectDebitProfileSuccess(response.data))
  }
}

export function* getCollectionLimit(api, { businessId }) {
  const response = yield call(api.getCollectionLimit, businessId)
  if (response.status === 404) {
    yield put(DirectDebitActions.onGetCollectionLimitSuccess(null))
  } else if (!response.ok) {
    showErrorNotification(response)
    yield put(DirectDebitActions.onGetCollectionLimitError())
  } else {
    yield put(DirectDebitActions.onGetCollectionLimitSuccess(response.data))
  }
}

export function* getDirectDebitOnboardingRequest(api, { businessId }) {
  const response = yield call(api.getDirectDebitOnboardingRequest, businessId)

  if (response.status === 404) {
    yield put(DirectDebitActions.onGetDirectDebitOnboardingRequestSuccess(null))
  } else if (!response.ok) {
    showErrorNotification(response)
    yield put(DirectDebitActions.onGetDirectDebitOnboardingRequestError())
  } else {
    yield put(
      DirectDebitActions.onGetDirectDebitOnboardingRequestSuccess(response.data)
    )
  }
}

export function* rejectDirectDebitOnboardingRequest(
  api,
  { businessId, requestId }
) {
  const response = yield call(
    api.rejectDirectDebitOnboardingRequest,
    businessId,
    requestId
  )

  yield put(DirectDebitActions.onRejectDirectDebitOnboardingRequestComplete())

  if (!response.ok) {
    showErrorNotification(response)
  } else {
    yield put(DirectDebitActions.getDirectDebitOnboardingRequest(businessId))
  }
}

export function* createDirectDebitProfile(api, { report, file, tcDate }) {
  const businessId = yield select(state => state.businesses.businessDetails.id)
  const reportResponse = yield call(
    api.uploadDirectDebitCreditReport,
    businessId,
    { report, file }
  )

  if (!reportResponse.ok) {
    showErrorNotification(reportResponse)
    yield put(DirectDebitActions.onCreateDirectDebitProfileComplete())
    return
  }

  const profileResponse = yield call(api.createDirectDebitProfile, businessId, {
    creditReportId: reportResponse.data.id,
    tcDate
  })
  if (!profileResponse.ok) {
    showErrorNotification(profileResponse)
    yield put(DirectDebitActions.onCreateDirectDebitProfileComplete())
  } else {
    yield put(
      DirectDebitActions.onGetDirectDebitProfileSuccess(profileResponse.data)
    )
    yield put(DirectDebitActions.toggleCreateProfileModal(false))
  }
}

export function* requestDirectDebitCollection(
  api,
  { businessId, filter, pageNumber, pageSize }
) {
  try {
    const response = yield call(
      api.getDirectDebitsCollection,
      businessId,
      pageNumber,
      pageSize
    )
    if (response.ok) {
      const {
        content,
        page: { size },
        total_elements,
        has_next
      } = response.data

      const count =
        pageNumber * size < total_elements ? pageNumber * size : total_elements

      yield put(
        DirectDebitActions.onCollectionsListDataSuccess(
          content,
          total_elements,
          has_next,
          pageNumber,
          count
        )
      )
    }
  } catch (err) {
    showErrorNotification(err)
    yield put(DirectDebitActions.onCollectionsListDataFailure())
  }
}

export function* updateCreditorIdentifier(
  api,
  { businessId, creditorIdentifier }
) {
  const response = yield call(
    api.updateCreditorIdentifer,
    businessId,
    creditorIdentifier
  )
  if (response.ok) {
    yield put(DirectDebitActions.updateCreditorIdentifierSuccess())
  } else {
    showErrorNotification(response)
    yield put(DirectDebitActions.updateCreditorIdentifierFailure())
  }
}
