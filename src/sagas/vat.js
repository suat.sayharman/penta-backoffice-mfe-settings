import { call, put } from 'redux-saga/effects'
import VatActions from '../redux-modules/vat'
import {
  showErrorNotification,
  showSuccessNotification
} from '../utils/notification-handler'

export function* getVat(api, { businessId }) {
  const response = yield call(api.getVat, businessId)
  if (response.ok) {
    yield put(VatActions.onVatDataReceived(response.data))
  } else {
    showErrorNotification(response)
  }
}

export function* createVat(api, { businessId, vatData }) {
  const response = yield call(api.createVat, businessId, vatData)
  if (response.ok) {
    const sucessMessage = vatData.service_start_date
      ? 'New temporary VAT rate for specific service period has been successfully created.'
      : 'New default VAT rate has been successfully created.'
    showSuccessNotification(sucessMessage)
    yield put(VatActions.getVat(businessId))
  } else {
    showErrorNotification(response)
  }
}

export function* deleteVatServicePeriod(api, { businessId, id }) {
  const response = yield call(api.deleteVatServicePeriod, id)
  if (response.ok) {
    showSuccessNotification(
      'Temporary VAT rate for service period has been successfully deleted.'
    )
    yield put(VatActions.getVat(businessId))
  } else {
    showErrorNotification(response)
  }
}
