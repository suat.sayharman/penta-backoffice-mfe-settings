import { call, put } from 'redux-saga/effects'
import AnnouncementActions from '../redux-modules/announcement'
import { showErrorNotification } from '../utils/notification-handler'
import { push } from 'react-router-redux'
import _ from 'lodash'

export function* getAnnouncements(api) {
  let response = yield call(api.getAnnouncements)
  const types = ['announcement', 'announcement_app']
  if (response.ok) {
    const { data } = response
    const activeOnly = _.filter(
      data,
      x => types.includes(x.type) && x.is_active
    )
    const inactiveOnly = _.filter(
      data,
      x => types.includes(x.type) && !x.is_active
    )
    const sorted = _.orderBy(inactiveOnly, ['created_at'], ['desc'])
    yield put(AnnouncementActions.onAnnouncementsReceived(activeOnly, sorted))
  } else {
    showErrorNotification(response)
  }
}

function createAnnouncementPayload(data, mode = 'create') {
  const payload = {
    is_active: data.is_active,
    condition: {
      permissions: {
        ...data.permissions
      }
    },
    metadata: {
      title: data.title,
      header_de: data.header_de,
      header_en: data.header_en,
      content_de: data.content_de,
      content_en: data.content_en,
      link_de: data.link_de,
      link_en: data.link_en
    }
  }
  if (data.type === 'announcement_app') {
    payload.metadata.priority = data.priority
    payload.metadata.appearance = data.appearance
    if (data.header_it && data.header_it.length) {
      payload.metadata.header_it = data.header_it
    }
    if (data.content_it && data.content_it.length) {
      payload.metadata.content_it = data.content_it
    }
    if (data.link_it && data.link_it.length) {
      payload.metadata.link_it = data.link_it
    }
  }
  if (mode === 'create') {
    payload.channel = ['frontend']
    payload.type = data.type
  }
  return payload
}

export function* createAnnouncement(api, { data }) {
  const payload = createAnnouncementPayload(data)
  let response = yield call(api.createAnnouncement, payload)
  if (response.ok) {
    yield put(push(`/announcement`))
  } else {
    showErrorNotification(response)
  }
}

export function* editAnnouncement(api, { id, data }) {
  const payload = createAnnouncementPayload(data, 'edit')
  let response = yield call(api.editAnnouncement, id, payload)
  if (response.ok) {
    yield put(AnnouncementActions.onAnnouncementEdited())
    yield put(push(`/announcement`))
    yield put(AnnouncementActions.getAnnouncements())
  } else {
    showErrorNotification(response)
  }
}

export function* removeAnnouncement(api, { id }) {
  let response = yield call(api.deleteAnnouncement, id, { is_active: false })
  if (response.ok) {
    yield put(AnnouncementActions.onAnnouncementEdited())
    yield put(push(`/announcement`))
    yield put(AnnouncementActions.getAnnouncements())
  } else {
    showErrorNotification(response)
  }
}
