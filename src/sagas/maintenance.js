import { call, put } from 'redux-saga/effects'
import MaintenanceActions from '../redux-modules/maintenance'
import { showErrorNotification } from '../utils/notification-handler'

export function* toggleMaintenanceStatus(api, data) {
  const newStatus = !data.maintenanceStatus
  let response = yield call(api.toggleMaintenanceStatus, newStatus)
  if (response.ok) {
    yield put(MaintenanceActions.onMaintenanceStatusChanged(newStatus))
  } else {
    showErrorNotification(response)
    yield put(MaintenanceActions.onError())
  }
}

export function* getMaintenanceStatus(api) {
  const response = yield call(api.getMaintenanceStatus)
  if (response.ok) {
    const status = response.data.status
    yield put(MaintenanceActions.onMaintenanceStatusChanged(status))
  } else {
    showErrorNotification(response)
    yield put(MaintenanceActions.onError())
  }
}
