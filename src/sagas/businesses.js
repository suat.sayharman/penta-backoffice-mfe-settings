/* eslint-disable complexity */
import FileSaver from 'file-saver'
import { all, call, put, select } from 'redux-saga/effects'
import BusinessActions from '../redux-modules/businesses'
import PricingActions from '../redux-modules/pricing'
import {
  reset,
  startAsyncValidation,
  stopAsyncValidation,
  touch
} from 'redux-form'
import { push } from 'react-router-redux'
import _ from 'lodash'
import {
  showErrorNotification,
  showSuccessNotification
} from '../utils/notification-handler'
import { getFileNameFromContentDisposition } from '../utils/getFileNameFromContentDisposition'
import { TERMS_AND_POLICIES } from '../utils/transform-data'
import { getBusinessPersons } from './persons'

export function* fetchBusinesses(api, { term, category, page }) {
  const response = yield call(api.searchUsers, term, category, page)
  if (response.ok) {
    let { data } = response
    const bizObj = yield select(state => state.businesses)
    if (bizObj.sortColumn && bizObj.sortOrder) {
      data = _.orderBy(data, bizObj.sortColumn, bizObj.sortOrder)
    }
    data.isInfiniteScrollSearch = page !== 1
    yield put(BusinessActions.onBusinessesReceived(data))
  }
}
export function* getBusinessData(api, { id }) {
  const data = {
    businessDetails: {},
    account: {},
    taxInfo: {},
    seizures: [],
    mandate: {}
  }

  const businessDetailsResponse = yield call(api.getBusiness, id)
  if (businessDetailsResponse.ok && businessDetailsResponse.data) {
      data.businessDetails = businessDetailsResponse.data
    yield put(
      PricingActions.onPlanRecieved(businessDetailsResponse.data.pricing_plan)
    )
  }
  // If the details for the business are not present, accountsForBusiness, taxInfo and seizuresData APIs would fail
  // Therefore we won't call them
  if (data.businessDetails.details) {
    const accountsResponse = yield call(api.getAccountsForBusiness, id)
    if (accountsResponse.ok && accountsResponse.data.length > 0) {
      data.accounts = accountsResponse.data
      if (data.accounts.length > 0) {
        data.account = data.accounts[0]

        const businessUsers = yield call(api.getUsersForBusiness, id)

        const approversResponse = yield call(
          api.getAuthorizedPersons,
          id,
          data.account.id
        )

        businessUsers.data.forEach(user => {
          approversResponse.data.forEach(approver => {
            if (user.id === approver.id) {
              approver['is_active'] = user.is_active
            }
          })
        })

        if (approversResponse.ok) {
          yield put(BusinessActions.onApproversReceived(approversResponse.data))
        } else {
          yield call(onError, approversResponse)
        }
      }
    }

    const taxesResponse = yield call(api.getTaxInfo, id)
    if (taxesResponse.ok && taxesResponse.data.length) {
      let primaryTax = _.find(taxesResponse.data, x => x.primary)
      if (primaryTax) {
        data.taxInfo = primaryTax
      }
    }

    const seizuresResponse = yield call(api.getSeizuresData, id)
    if (seizuresResponse.ok) {
      data.seizures = seizuresResponse.data
    }
  }

  const [
    invoicesResponse,
    mandateResponse,
    termsAndPoliciesResponse,
    identificationRes,
    creditNotesResponse
  ] = yield all([
    call(api.getInvoicesList, id),
    call(api.getMandateData, id),
    call(api.getTermsAndPolicies, id),
    call(api.getIdentification, id),
    call(api.getCreditNotes, id)
  ])
  if (invoicesResponse.ok) {
    yield put(PricingActions.onInvoicesRecieved(invoicesResponse.data))
  }
  if (mandateResponse.ok) {
    data.mandate = mandateResponse.data
  }

  if (termsAndPoliciesResponse.ok) {
    const { data: responseData } = termsAndPoliciesResponse
    data.termsAndConditions = getActiveTermsItem(
      responseData,
      TERMS_AND_POLICIES.TERMS_AND_CONDITIONS
    )
    data.priceAndService = getActiveTermsItem(
      responseData,
      TERMS_AND_POLICIES.PRICE_AND_SERVICE_LIST
    )
  }

  if (identificationRes.ok) {
    data.identification = identificationRes.data
  }

  if (creditNotesResponse.ok) {
    yield put(PricingActions.onCreditNotesReceived(creditNotesResponse.data))
  } else {
    yield call(onPricingError, creditNotesResponse)
  }

  // Map the data to state
  yield put(BusinessActions.onBusinessDataReceived(data))
}

export function* getBusinessFiles(api, { businessId }) {
  const response = yield call(api.getBusinessFiles, businessId)
  if (response.ok) {
    yield put(BusinessActions.onFilesReceived(response.data))
  } else {
    showErrorNotification(response)
  }
}

export function* uploadDocument(api, { businessId, docType, file }) {
  let response = yield call(api.uploadBusinessDoc, businessId, docType, file)
  if (response.ok) {
    yield call(getBusinessFiles, api, { businessId })
  } else {
    showErrorNotification(response)
  }
}

export function* getBusinessCards(api, { businessId }) {
  const response = yield call(api.getCards, businessId)
  if (response.ok) {
    yield put(BusinessActions.onCardsRecieved(response.data))
  } else {
    showErrorNotification(response)
  }
}

export function* createTaxRecord(api, { businessId, data }) {
  const payload = {
    ...data,
    primary: true,
    country: 'DE'
  }
  let response = yield call(api.createTaxRecord, businessId, payload)
  if (response.ok) {
    response = yield call(api.getTaxInfo, businessId)
    if (response.ok && response.data.length) {
      yield put(BusinessActions.onTaxInfoUpdated(response.data[0]))
    }
  } else {
    yield call(onError, response)
  }
}

export function* updateBusiness(api, { businessId, data }) {
  let response = yield call(api.updateBusiness, businessId, data)
  if (response.ok) {
    const { status, data: responseData } = response
    if (status === 202) {
      // load persons (and users) if not yet available in state - needed to check if authorized person is LR
      const persons = yield select(state => state.persons.persons)
      if (!persons) {
        yield call(getBusinessPersons, api, { businessId })
      }
      yield put(BusinessActions.onChangeRequestReceived(responseData.id_change))
    } else {
      yield call(onUpdateSuccess, businessId)
    }
  } else {
    yield call(onError, response)
  }
}

export function* confirmBusinessUpdate(
  api,
  { idChange, businessId, tan, personId }
) {
  yield put(startAsyncValidation('confirmUpdateForm'))
  const response = yield call(
    api.confirmApprovalChangeRequest,
    idChange,
    tan,
    personId,
    businessId
  )
  if (response.ok) {
    yield call(onUpdateSuccess, businessId)
  } else {
    const patchBusinessData = yield select(
      state => state.businesses.patchBusinessData
    )
    yield put(BusinessActions.updateBusiness(businessId, patchBusinessData))
    yield call(onError, response)
  }
}

function* onUpdateSuccess(businessId) {
  yield put(BusinessActions.onBusinessUpdated())
  yield put(reset('confirmUpdateForm'))
  yield put(BusinessActions.getBusinessData(businessId))
}

export function* onError(response) {
  const { code, action } = response.data
  if (code === 'INVALID_OR_EXPIRED_TAN') {
    yield put(BusinessActions.onInvalidTan())
    yield put(
      stopAsyncValidation('confirmUpdateForm', {
        tan0: true,
        tan1: true,
        tan2: true,
        tan3: true,
        tan4: true,
        tan5: true
      })
    )
    yield put(touch('confirmUpdateForm'))
  } else {
    yield put(BusinessActions.onError(action))
  }
  showErrorNotification(response)
}

export function* businessReadyForSolaris(api, { businessId }) {
  const response = yield call(api.businessReadyForSolaris, businessId)
  if (response.ok) {
    yield put(BusinessActions.businessReadyForSolarisSuccess())
  } else {
    yield call(onError, response)
  }
}

export function* deleteBusinessData(api, { businessId }) {
  const response = yield call(api.deleteBusinessData, businessId)
  yield put(BusinessActions.onRequestFinished())
  if (response.ok) {
    yield put(push('/businesses'))
    showSuccessNotification('Business deleted!')
  } else {
    showErrorNotification(response)
  }
}

export function* restartBusinessIdentification(api, { businessId }) {
  const response = yield call(api.restartBusinessIdentification, businessId)
  yield put(BusinessActions.onRequestFinished())
  if (response.ok) {
    yield call(onUpdateSuccess, businessId)
    showSuccessNotification('Identification restarted!')
  } else {
    showErrorNotification(response)
  }
}

export function* cancelDowngrade(api, { businessId }) {
  const response = yield call(api.cancelDowngrade, businessId)
  if (response.ok) {
    yield put(BusinessActions.getBusinessData(businessId))
  }
}

export function* changePricingPlan(api, { businessId, data }) {
  const response = yield call(api.changePricingPlan, businessId, data)
  if (response.ok) {
    yield put(PricingActions.hideModal())
    yield put(BusinessActions.getBusinessData(businessId))
  }
}

export function* topUpCredit(api, payload) {
  const { businessId, data } = payload
  const credit = Math.round(parseFloat(data.credit) * 100)
  const creditToAdd = {
    value: credit,
    currency: 'EUR',
    unit: 'cents'
  }
  const { reason } = data
  const response = yield call(api.topUpCredit, businessId, creditToAdd, reason)
  if (response.ok) {
    showSuccessNotification('Your changes have been saved successfully')
    yield put(PricingActions.getRemainingCredits(businessId))
  } else {
    showErrorNotification(response)
  }
}

export function* getUsageAndCostsByDate(api, { businessId, month, year }) {
  const response = yield all([
    yield call(api.getUsageBreakdown, businessId, month, year),
    yield call(api.getCostsBreakdown, businessId, month, year),
    yield call(api.getCurrencyCloudCostsBreakdown, businessId, month, year)
  ])

  const usageResponse = response[0]
  const costsResponse = response[1]
  const currencyCloudCosts = response[2]

  if (usageResponse.ok) {
    yield put(PricingActions.onUsageRecieved(usageResponse.data.components))
  } else {
    yield call(onPricingError, usageResponse)
  }

  if (costsResponse.ok) {
    yield put(PricingActions.onCostsRecieved(costsResponse.data))
  } else {
    yield call(onPricingError, costsResponse)
  }

  if (currencyCloudCosts.ok) {
    yield put(
      PricingActions.onCurrencyCloudCostsReceived(currencyCloudCosts.data)
    )
  } else {
    yield call(onPricingError, costsResponse)
  }
}

export function* setAccountClosureDate(
  api,
  { businessId, accountId, closureDate }
) {
  const response = yield call(
    api.setAccountClosureDate,
    businessId,
    accountId,
    closureDate
  )
  if (response.ok) {
    yield put(BusinessActions.onAccountClosureDateSet(closureDate))
  } else {
    showErrorNotification(response)
  }
}

export function* getComplianceQuestions(api, { businessId }) {
  const response = yield call(api.getComplianceQuestions, businessId)
  if (response.ok) {
    yield put(BusinessActions.getComplianceQuestionsSuccess(response.data))
  } else {
    showErrorNotification(response)
  }
}

export function* getMissingDocuments(api, { businessId }) {
  const [pendingRes, uploadedRes] = yield all([
    call(api.getMissingDocuments, businessId, 'PENDING'),
    call(api.getMissingDocuments, businessId, 'UPLOADED')
  ])
  if (pendingRes.ok && uploadedRes.ok) {
    yield put(
      BusinessActions.getMissingDocumentsSuccess(
        pendingRes.data.concat(uploadedRes.data)
      )
    )
  } else {
    showErrorNotification(!pendingRes.ok ? pendingRes : uploadedRes)
  }
}

export function* downloadDocument(api, { businessId, documentId }) {
  const response = yield call(api.downloadDocument, businessId, documentId)
  if (response.ok) {
    FileSaver.saveAs(
      response.data,
      getFileNameFromContentDisposition(response.headers['content-disposition'])
    )
  } else {
    showErrorNotification(response)
  }
}

export function* updateFreeTrial(api, { endDate }) {
  const businessId = yield select(state => state.businesses.businessDetails.id)
  const response = yield call(api.patchFreeTrial, businessId, endDate)
  if (response.ok) {
    showSuccessNotification('Your changes have been saved successfully')
    yield call(getUsageAndCostsByDate, api, { businessId })
  } else {
    yield call(onPricingError, response)
  }
}

function* onPricingError(response) {
  showErrorNotification(response)
  yield put(PricingActions.onError())
}

function getActiveTermsItem(data, type) {
  const filtered = _.filter(data, item => item.type === type)
  return _.orderBy(filtered, ['accepted_at'], ['desc'])[0]
}

export function* getBusinessClosureReason(api, { businessId }) {
  const response = yield call(api.getBusinessClosureReason, businessId)
  if (response.ok) {
    if (response.data) {
      const churnReason = response.data.reason_details
      const churnComment = response.data.reason_comments
      yield put(
        BusinessActions.onBusinessClosureReasonReceived(
          churnReason,
          churnComment
        )
      )
    }
  } else {
    showErrorNotification(response)
  }
}

function* onBusinessClosureReasonResponse(response) {
  if (response.ok) {
    const churnReason = (response.data && response.data.reason_details) || ''
    const churnComment = (response.data && response.data.reason_comments) || ''
    yield put(
      BusinessActions.onBusinessClosureReasonReceived(churnReason, churnComment)
    )
  } else {
    showErrorNotification(response)
  }
}

function* onBusinessClosureSavedUpdated(response, churnReason, churnComment) {
  if (response.ok) {
    if (response.data) {
      yield call(onBusinessClosureReasonResponse, response)
    } else {
      yield put(
        BusinessActions.onBusinessClosureReasonReceived(
          churnReason,
          churnComment
        )
      )
    }
  } else {
    showErrorNotification(response)
  }
}

export function* saveBusinessClosureReason(
  api,
  { businessId, churnReason, churnComment }
) {
  const response = yield call(api.saveBusinessClosureReason, businessId, {
    reason_details: churnReason,
    reason_comments: churnComment
  })
  yield call(onBusinessClosureSavedUpdated, response, churnReason, churnComment)
}

export function* updateBusinessClosureReason(
  api,
  { businessId, churnReason, churnComment }
) {
  const response = yield call(api.updateBusinessClosureReason, businessId, {
    reason_details: churnReason,
    reason_comments: churnComment
  })
  yield call(onBusinessClosureSavedUpdated, response, churnReason, churnComment)
}

export function* deleteBusinessClosureReason(api, { businessId }) {
  const response = yield call(api.deleteBusinessClosureReason, businessId)

  yield call(onBusinessClosureSavedUpdated, response, null, null)
}

export function* syncBusinessSaga(api, { businessId }) {
  const response = yield call(api.syncBusiness, businessId)
  if (response.ok) {
    yield put(BusinessActions.syncBusinessSuccess())
    yield put(BusinessActions.getBusinessData(businessId))
  } else {
    showErrorNotification(response)
    yield put(BusinessActions.syncBusinessFailure())
  }
}
