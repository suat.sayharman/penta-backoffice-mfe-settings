import { call, put } from 'redux-saga/effects'
import BusinessActions from '../redux-modules/businesses'
import UsersActions from '../redux-modules/users'
import {
  showErrorMessage,
  showErrorNotification,
  showSuccessNotification
} from '../utils/notification-handler'
import { fetchBusinesses } from './businesses'
import Notification from '../components/common/Notification'

import { QUERY_KEY, CATEGORY_KEY } from '../constants/business'

export function* getBusinessUsers(api, { businessId }) {
  const response = yield call(api.getUsersForBusiness, businessId)
  if (response.ok) {
    yield put(UsersActions.onUsersReceived(response.data))
  } else {
    showErrorNotification(response)
  }
}

export function* deleteUser(api, { id }) {
  const response = yield call(api.deleteUser, id)
  if (response.ok) {
    showSuccessNotification('User successfully deleted.')
    yield call(fetchBusinesses, api, {
      term: sessionStorage.getItem(QUERY_KEY),
      category: sessionStorage.getItem(CATEGORY_KEY),
      page: 1
    })
  } else {
    showErrorNotification(response)
  }
}

export function* authorizeUser(api, { businessId, accountId, userId }) {
  const response = yield call(api.authorizeUser, businessId, accountId, userId)
  if (response.ok) {
    yield put(UsersActions.authorizeUserSuccess())
    if (response.data && response.data.status) {
      yield put(
        BusinessActions.onChangeRequestReceived(response.data.id_change)
      )
    } else {
      yield put(BusinessActions.getBusinessData(businessId))
      showSuccessNotification('User Authorized')
    }
  } else {
    if (response.data.code === 'USER_ALREADY_AUTHORIZED') {
      showErrorMessage('User is already authorized')
    } else if (response.data.code === 'FAILED_AUTHORIZATION_MISSING_FIELDS') {
      showFailedAuthorizationMissingFieldsError(response.data)
    } else {
      showErrorNotification(response)
    }
    yield put(UsersActions.authorizeUserFailure())
  }
}

function showFailedAuthorizationMissingFieldsError(error) {
  const fieldMapping = {
    address_line_1: 'Address',
    country: 'Country',
    city: 'City',
    birth_city: 'Birth City',
    nationality: 'Nationality'
  }

  const fields = error.data
  const fieldList = fields
    .map(field => {
      if (fieldMapping.hasOwnProperty(field)) {
        return `${fieldMapping[field]} (${field})`
      }
      return field
    })
    .join(', ')

  Notification.showFailureMsg(
    `We are unable to authorise user since the following identification information is missing: ${fieldList}`
  )
}
