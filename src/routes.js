import Loadable from 'react-loadable'

import DefaultLayout from './layout/default'
import Loader from './components/common/Loader'
import { PERMISSIONS } from './constants/permissions'

const Settings = Loadable({
  loader: () => import('./components/settings'),
  loading: Loader
})

const routes = [
  {
    path: '/settings',
    exact: true,
    name: 'Settings',
    component: Settings,
    requiredPermissions: [PERMISSIONS.settings.read]
  },

  {
    path: '/',
    exact: true,
    name: 'Home',
    component: DefaultLayout,
    requiredPermissions: []
  }
]

export default routes
